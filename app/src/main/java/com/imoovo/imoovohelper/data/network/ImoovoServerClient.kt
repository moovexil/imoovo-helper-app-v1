package com.imoovo.imoovohelper.data.network

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import com.google.gson.Gson
import com.imoovo.imoovohelper.data.cloud.CloudAuthDataSource
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ImoovoServerClient @Inject constructor(
    private val appContext: Context,
    private val gsonConverter: Gson,
    private val cloudAuthDataSource: CloudAuthDataSource
) {
    /**
     * Creates the network client for Immovo server requests
     *
     * @return the network client
     */
     fun provideApi(): RemoteDataSource {
        return provideRetrofit().create(RemoteDataSource::class.java)
    }

    @SuppressLint("HardwareIds")
    private fun provideDeviceId(): String = Settings.Secure
        .getString(appContext.contentResolver, Settings.Secure.ANDROID_ID)


    private fun provideTimestamp(): String = Calendar.getInstance().time.toString()


    private fun provideTimeZoneId(): String = TimeZone.getDefault().id

    private fun provideUserToken (): String = cloudAuthDataSource.getUserAuthToken()

    @SuppressLint("HardwareIds")
    private fun provideOkHttpClient(
        deviceID: String, timestamp: String, timezoneId: String, userToken: String
    ): OkHttpClient {
        return OkHttpClient().newBuilder()
            .addInterceptor { chain: Interceptor.Chain ->
                val originalRequest = chain.request()
                val builder = originalRequest.newBuilder()
                    .header("Authorization", Credentials.basic("misha", "misha"))
                    .header("device-id", deviceID)
                    .header("user-token", userToken)
                    .header("app-name", "imoovo-helper")
                    .header("app-id", "imoovo-helper")
                    .header("timestamp", timestamp)
                    .header("time-zone-id", timezoneId)

                val newRequest = builder.build()
                chain.proceed(newRequest)
            }.build()
    }

    private fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://moovex-server-2.herokuapp.com")
            .client(
                provideOkHttpClient(
                    deviceID = provideDeviceId(),
                    timestamp = provideTimestamp(),
                    timezoneId = provideTimeZoneId(),
                    userToken = provideUserToken()
                )
            )
            .addConverterFactory(GsonConverterFactory.create(gsonConverter))
            .build()
    }

}