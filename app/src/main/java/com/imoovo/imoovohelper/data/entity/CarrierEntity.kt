package com.imoovo.imoovohelper.data.entity

import com.imoovo.imoovohelper.domain.model.Metadata
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType
import java.util.*

data class CarrierEntity(

    val id: UUID?,
    val adminComment: String?,
    // this is not nullable mandatory data
    val userType: UserType,
    val userStatus: UserStatus,
    val contactName: String,
    val phoneNumber: String,
    //false by default
    val cbtAvailable: Boolean?, //cbt -> cross border transportation
    val gitAvailable: Boolean?,

    val companyName: String?,
    val email: String?,
    val country: String?,
    val region: String?,
    val city: String?,
    val trucks: Set<TruckEntity>?,

    val metaData: Metadata?
)

