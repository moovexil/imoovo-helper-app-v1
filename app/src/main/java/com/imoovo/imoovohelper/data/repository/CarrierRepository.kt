package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.data.entity.CarrierEntity
import com.imoovo.imoovohelper.domain.model.filters.CarrierFilter
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType
import java.util.*

interface CarrierRepository {

    suspend fun createCarrierUser(user: CarrierEntity)

    suspend fun updateCarrierUser(user: CarrierEntity)

    suspend fun getCarriersByFilters(filter: CarrierFilter): Set<CarrierEntity>

    suspend fun getCarriersByUserStatus(userStatus: UserStatus): Set<CarrierEntity>

    suspend fun getCarrierUserById(id: UUID): CarrierEntity

    suspend fun changeUserRoleToCarrier(
        carrierUser: CarrierEntity,
        oldUserId: UUID,
        oldUserType: UserType
    )
}