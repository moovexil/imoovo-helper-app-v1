package com.imoovo.imoovohelper.data.entity

import com.imoovo.imoovohelper.domain.model.truck.TruckType
import java.util.*

data class TruckEntity(
    val id: UUID?,
    val type: TruckType?,
    val tonnage: Double?,
    val trucksNumber: Int?
)