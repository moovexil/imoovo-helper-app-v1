package com.imoovo.imoovohelper.data.preference

import com.imoovo.imoovohelper.domain.model.user.User
import kotlinx.coroutines.flow.Flow

interface PreferenceDataSource {

    var isLocationTrackingObservable: Flow<Boolean>
    var isLocationTracking: Boolean
    var user: User?

}