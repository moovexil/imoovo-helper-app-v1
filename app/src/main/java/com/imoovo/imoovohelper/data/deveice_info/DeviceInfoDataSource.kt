package com.imoovo.imoovohelper.data.deveice_info

interface DeviceInfoDataSource {

   suspend fun getDeviceId () : String
}