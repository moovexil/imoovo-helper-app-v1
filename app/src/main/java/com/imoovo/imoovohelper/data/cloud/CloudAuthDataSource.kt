package com.imoovo.imoovohelper.data.cloud

import com.imoovo.imoovohelper.domain.model.PhoneAuthState
import com.imoovo.imoovohelper.domain.model.Result
import kotlinx.coroutines.flow.Flow


interface CloudAuthDataSource {

    suspend fun isUserSignedIn(): Boolean

    fun observeAuthenticationFlow(): Flow<Result<PhoneAuthState>>

    suspend fun sighInWithPhoneNumber(phoneNumber: String)

    suspend fun setSmsVerificationCode(code: String)

    suspend fun signOut()

    fun getUserAuthToken(): String
}