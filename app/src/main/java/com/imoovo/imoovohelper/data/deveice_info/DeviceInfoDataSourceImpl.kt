package com.imoovo.imoovohelper.data.deveice_info

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import com.imoovo.imoovohelper.di.qualifier.DefaultDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DeviceInfoDataSourceImpl @Inject constructor(
    private val appContext: Context,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) : DeviceInfoDataSource {


    @SuppressLint("HardwareIds")
    override suspend fun getDeviceId(): String =
        withContext(defaultDispatcher){
            Settings.Secure.getString(appContext.contentResolver, Settings.Secure.ANDROID_ID)
        }


}