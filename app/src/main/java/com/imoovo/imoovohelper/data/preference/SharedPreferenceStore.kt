package com.imoovo.imoovohelper.data.preference

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.annotation.WorkerThread
import androidx.core.content.edit
import com.google.gson.Gson
import com.imoovo.imoovohelper.common.extensions.deserializeJson
import com.imoovo.imoovohelper.common.extensions.serializeJson
import com.imoovo.imoovohelper.domain.model.user.User
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import javax.inject.Inject
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

@ExperimentalCoroutinesApi
@FlowPreview
class SharedPreferenceStore @Inject constructor(
    private val context: Context,
    private val serializer: Gson
) : PreferenceDataSource {

    companion object {
        const val TAG = "SharedPreferenceStore"
        const val PREFS_NAME = "driver_safe"
        const val PREF_IS_LOCATION_TRACKING = "is_location_tracking"
        const val PREF_USER = "user"
    }

    private val prefs: Lazy<SharedPreferences> = lazy {
        // Lazy to prevent IO access to main thread.
        context.applicationContext.getSharedPreferences(
            PREFS_NAME, Context.MODE_PRIVATE
        ).apply {
            registerOnSharedPreferenceChangeListener(changeListener)
        }
    }

    private val changeListener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
        when (key) {
            PREF_IS_LOCATION_TRACKING -> {
                locationTrackingChannel.offer(isLocationTracking)
                Log.d(TAG, "isTracking was updated  $isLocationTracking")
            }
        }
    }


    private val locationTrackingChannel: ConflatedBroadcastChannel<Boolean> by lazy {
        ConflatedBroadcastChannel<Boolean>().also { channel ->
            channel.offer(isLocationTracking)
        }
    }

    override var isLocationTrackingObservable: Flow<Boolean>
        get() = locationTrackingChannel.asFlow()
        set(_) = throw  IllegalArgumentException("This property can't be changed")


    override var isLocationTracking by BooleanPreference(
        prefs,
        PREF_IS_LOCATION_TRACKING,
        defaultValue = false
    )

    override var user: User? by SerializedUserPreference(
        prefs,
        PREF_USER,
        serializer
    )

}

class BooleanPreference(
    private val preferences: Lazy<SharedPreferences>,
    private val name: String,
    private val defaultValue: Boolean
) : ReadWriteProperty<Any, Boolean> {

    @WorkerThread
    override fun getValue(thisRef: Any, property: KProperty<*>): Boolean {
        return preferences.value.getBoolean(name, defaultValue)
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: Boolean) {
        preferences.value.edit { putBoolean(name, value) }
    }
}

class StringPreference(
    private val preferences: Lazy<SharedPreferences>,
    private val name: String,
    private val defaultValue: String?
) : ReadWriteProperty<Any, String?> {

    @WorkerThread
    override fun getValue(thisRef: Any, property: KProperty<*>): String? {
        return preferences.value.getString(name, defaultValue)
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: String?) {
        preferences.value.edit { putString(name, value) }
    }
}

class SerializedUserPreference (
    private val preferences: Lazy<SharedPreferences>,
    private val name: String,
    private val serializer : Gson,
    private val defaultValue: User? = null
): ReadWriteProperty<Any, User?> {

    override fun getValue(thisRef: Any, property: KProperty<*>): User? {
        val json = preferences.value.getString(name,null)
        val user = json?.deserializeJson<User>(serializer)
        return user ?: defaultValue
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: User?) {
        if (value != null){
            val data = value.serializeJson(serializer)
            preferences.value.edit { putString(name,data) }
        }
    }
}