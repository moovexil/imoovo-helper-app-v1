package com.imoovo.imoovohelper.data.cloud

import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.model.PhoneAuthState
import com.imoovo.imoovohelper.domain.model.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit
import javax.inject.Inject


@FlowPreview
@ExperimentalCoroutinesApi
class CloudAuthDataSourceImpl @Inject constructor(
    private val firebaseAuth: FirebaseAuth,
    private val executor: Executor,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : CloudAuthDataSource {

    private val authChannel by lazy { ConflatedBroadcastChannel<Result<PhoneAuthState>>() }
    private lateinit var storedVerificationId: String
    private var isAutoSignInEnabled = false

    override fun observeAuthenticationFlow(): Flow<Result<PhoneAuthState>> =
        authChannel.asFlow()

    override suspend fun isUserSignedIn(): Boolean =
        firebaseAuth.currentUser != null

    override suspend fun sighInWithPhoneNumber(phoneNumber: String) {

        val callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.

                //notifies that we can authenticate user without sms

                isAutoSignInEnabled = true
                signInWithPhoneAuthCredential(credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                authChannel.offer(Result.Error(e))
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.

                // Save verification ID and resending token so we can use them later
                storedVerificationId = verificationId

                // init delay to wait callback from the auto login
                Thread.sleep(1000)

                if (!isAutoSignInEnabled) {
                    authChannel.offer(Result.Success(PhoneAuthState.ON_CODE_SENT))
                }
            }
        }

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            phoneNumber,
            60,
            TimeUnit.SECONDS,
            executor,
            callbacks
        )
    }

    override suspend fun setSmsVerificationCode(code: String) {
        val credentials = PhoneAuthProvider.getCredential(storedVerificationId, code)
        signInWithPhoneAuthCredential(credentials)
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        firebaseAuth.signInWithCredential(credential)

            .addOnCompleteListener(executor, OnCompleteListener { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    authChannel.offer(Result.Success(PhoneAuthState.SUCCESS))
                } else {
                    // Sign in failed, display a message and update the UI
                    authChannel.offer(Result.Error(task.exception!!))
                }
            })

            .addOnFailureListener(executor, OnFailureListener {
                authChannel.offer(Result.Error(it))
            })

    }

    override suspend fun signOut() = firebaseAuth.signOut()

    override fun getUserAuthToken(): String = "unknown"


}