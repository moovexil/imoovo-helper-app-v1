package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.domain.model.PhoneAuthState
import com.imoovo.imoovohelper.domain.model.Result
import kotlinx.coroutines.flow.Flow

interface AuthRepository {

    suspend fun isUserSignedIn (): Boolean

    fun observeAuthenticationFlow(): Flow<Result<PhoneAuthState>>

    suspend fun sighInWithPhoneNumber(phoneNumber: String)

    suspend fun setSmsVerificationCode (code: String)

    suspend fun signOut()
}