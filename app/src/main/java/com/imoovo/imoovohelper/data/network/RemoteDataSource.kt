package com.imoovo.imoovohelper.data.network

import com.imoovo.imoovohelper.data.entity.CarrierEntity
import com.imoovo.imoovohelper.data.entity.user.UserEntity
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.dashboard.DashboardStatistics
import com.imoovo.imoovohelper.domain.model.filters.CarrierFilter
import com.imoovo.imoovohelper.domain.model.filters.UserFilter
import com.imoovo.imoovohelper.domain.model.user.UserType
import retrofit2.http.*
import java.util.*

interface RemoteDataSource {

    @POST("v1/createNewUser")
    suspend fun createNewUser (@Body user: UserEntity): UserEntity

    @GET("v1/getUserByPhoneNumber")
    suspend fun getUserByPhoneNumber(@Query("phoneNumber") phoneNumber: String): UserEntity

    //--------------- Agent user --------------------------------

    @POST("v1/getAgentsByFilters")
    suspend fun getAgentsByFilters (@Body userFilter: UserFilter) : Set<UserEntity>



    //---------------- Broker user ------------------------------

    @POST("v1/getBrokersByFilters")
    suspend fun getBrokersByFilters (@Body userFilter: UserFilter) : Set<UserEntity>

    @PUT("v1/updateBrokerUser")
    suspend fun updateBrokerUser (@Body brokerUser: UserEntity)

    @PUT("v1/changeUserRoleToBroker")
    suspend fun changeUserRoleToBroker (
        @Body brokerUser: UserEntity,
        @Query("oldUserId") oldUserId: UUID,
        @Query ("oldUserType") oldUserType: UserType
    )


    //--------------- Carrier user ------------------------------

    @POST("v1/createCarrierUser")
    suspend fun createCarrierUser(@Body carrier: CarrierEntity)

    @PUT("v1/updateCarrierUser")
    suspend fun updateCarrierUser(@Body carrier: CarrierEntity)

    @POST("v1/getCarriersByFilters")
    suspend fun getCarriersByFilters(@Body filter: CarrierFilter): Set<CarrierEntity>

    @GET("v1/getCarriersByUserStatus")
    suspend fun getCarriersByUserStatus (@Query("userStatus") userStatus: UserStatus) : Set<CarrierEntity>

    @GET ("v1/getCarrierUserById")
    suspend fun getCarrierUserById (@Query("id") id: UUID): CarrierEntity

    @PUT("v1/changeUserRoleToCarrier")
    suspend fun changeUserRoleToCarrier (
        @Body carrierUser: CarrierEntity,
        @Query("oldUserId") oldUserId: UUID,
        @Query ("oldUserType") oldUserType: UserType
    )

    //---------------- Dashboard -------------------------------

    @GET("v2/getDashboardStatistics")
    suspend fun getDashboardStatistics(
        @Query("userType") userType: UserType
    ): DashboardStatistics

}