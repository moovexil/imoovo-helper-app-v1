package com.imoovo.imoovohelper.data.entity.filters

import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType

data class CarrierFiltersEntity(
    val userType: UserType? = null,
    val userStatus: UserStatus? = null,
    val phoneNumber: String? = null,
    val city: String? = null,
    val region: String? = null,
    val country: String? = null,
    val hasGitDoc: Boolean = false,
    val cbtAvailable: Boolean = false,
    val truckFilters: TruckFiltersEntity? = null
)
