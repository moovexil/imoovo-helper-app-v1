package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.data.cloud.CloudAuthDataSource
import com.imoovo.imoovohelper.domain.model.PhoneAuthState
import com.imoovo.imoovohelper.domain.model.Result
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val cloudAuthDataSource: CloudAuthDataSource
) : AuthRepository {

    override suspend fun isUserSignedIn(): Boolean =
        cloudAuthDataSource.isUserSignedIn()

    override fun observeAuthenticationFlow(): Flow<Result<PhoneAuthState>> =
        cloudAuthDataSource.observeAuthenticationFlow()

    override suspend fun sighInWithPhoneNumber(phoneNumber: String) =
        cloudAuthDataSource.sighInWithPhoneNumber(phoneNumber)

    override suspend fun setSmsVerificationCode(code: String) =
        cloudAuthDataSource.setSmsVerificationCode(code)

    override suspend fun signOut() = cloudAuthDataSource.signOut()
}