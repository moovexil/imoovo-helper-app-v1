package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.data.entity.user.UserEntity
import com.imoovo.imoovohelper.data.network.RemoteDataSource
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.filters.UserFilter
import com.imoovo.imoovohelper.domain.model.user.UserType
import java.util.*
import javax.inject.Inject

class BrokerRepositoryImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : BrokerRepository {

    override suspend fun getBrokersByFilters(userFilter: UserFilter): Set<UserEntity> =
        remoteDataSource.getBrokersByFilters(userFilter)

    override suspend fun updateBrokerUser(brokerUser: UserEntity) =
        remoteDataSource.updateBrokerUser(brokerUser)

    override suspend fun changeUserRoleToBroker(
        brokerUser: UserEntity,
        oldUserId: UUID,
        oldUserType: UserType
    ) = remoteDataSource.changeUserRoleToBroker(brokerUser, oldUserId, oldUserType)

}