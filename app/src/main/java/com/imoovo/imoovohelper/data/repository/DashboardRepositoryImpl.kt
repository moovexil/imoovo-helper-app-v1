package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.data.network.RemoteDataSource
import com.imoovo.imoovohelper.domain.model.dashboard.DashboardStatistics
import com.imoovo.imoovohelper.domain.model.user.UserType
import javax.inject.Inject

class DashboardRepositoryImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource
): DashboardRepository {

    override suspend fun getDashboardStatistics(userType: UserType): DashboardStatistics =
        remoteDataSource.getDashboardStatistics(userType)
}