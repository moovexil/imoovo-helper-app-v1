package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.data.entity.user.UserEntity
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.filters.UserFilter

interface AgentRepository {

    suspend fun getAgentsByFilters(userFilter: UserFilter): Set<UserEntity>
}