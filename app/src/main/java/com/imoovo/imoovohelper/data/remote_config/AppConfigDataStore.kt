package com.imoovo.imoovohelper.data.remote_config


/**
 * Provides API for working with the remote data source.
 * The source implementation responsive for periodical syncing and caching the application
 * configuration data with the remote server.
 */
interface AppConfigDataStore {

    suspend fun getUsersIdsGroups (): Map<String,Set<String>>

    suspend fun availableApkVersion (): String
}