package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.data.deveice_info.DeviceInfoDataSource
import com.imoovo.imoovohelper.data.remote_config.AppConfigDataStore
import javax.inject.Inject

class AppConfigRepositoryImpl @Inject constructor(
    private val configDataSource: AppConfigDataStore,
    private val deviceInfoDataSource: DeviceInfoDataSource
) : AppConfigRepository {


    override suspend fun getDeviceId(): String =
        deviceInfoDataSource.getDeviceId()

    override suspend fun getUsersIdsGroups(): Map<String, Set<String>> =
        configDataSource.getUsersIdsGroups()

    override suspend fun availableApkVersion(): String =
        configDataSource.availableApkVersion()

}