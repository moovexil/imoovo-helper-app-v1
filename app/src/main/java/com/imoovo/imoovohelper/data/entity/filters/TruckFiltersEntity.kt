package com.imoovo.imoovohelper.data.entity.filters

import com.imoovo.imoovohelper.domain.model.truck.TruckType

data class TruckFiltersEntity(
    var type: TruckType? = null,
    var tonnage: Double? = null
)
