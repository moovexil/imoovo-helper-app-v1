package com.imoovo.imoovohelper.data.remote_config


import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.gson.Gson
import com.imoovo.imoovohelper.common.extensions.deserializeCollection
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

const val DEVICE_ID_USER_ADMIN = "admin"
const val DEVICE_ID_USER_EX_ADMIN = "ex_admin"
const val DEVICE_ID_USER_BROKER = "broker"

class RemoteAppConfigDataSource @Inject constructor(
    private val remoteConfig: FirebaseRemoteConfig,
    private val serializer: Gson,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : AppConfigDataStore {


    
    override suspend fun getUsersIdsGroups(): Map<String, Set<String>> =
        withContext(ioDispatcher) {
            val admins = remoteConfig.getString(DEVICE_ID_ADMINS)
                .deserializeCollection<String>(serializer)
                .toSet()

            val exAdmins = remoteConfig.getString(DEVICE_ID_EX_ADMINS)
                .deserializeCollection<String>(serializer)
                .toSet()

            val brokers = remoteConfig.getString(DEVICE_ID_BROKERS)
                .deserializeCollection<String>(serializer)
                .toSet()

            mapOf(
                DEVICE_ID_USER_ADMIN to admins,
                DEVICE_ID_USER_EX_ADMIN to exAdmins,
                DEVICE_ID_USER_BROKER to brokers
            )
        }

    override suspend fun availableApkVersion(): String =
        withContext(ioDispatcher) {
            remoteConfig.getString(APK_UPDATE_REQUEST)
        }

    companion object {
        const val APK_UPDATE_REQUEST = "new_apk_version"
        const val DEVICE_ID_ADMINS = "device_id_admins"
        const val DEVICE_ID_EX_ADMINS = "device_id_ex_admins"
        const val DEVICE_ID_BROKERS = "device_id_brokers"
    }

}