package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.data.entity.user.UserEntity
import com.imoovo.imoovohelper.domain.model.user.User

interface AppUserRepository {

    suspend fun getServerUserByPhoneNumber(phoneNumber: String): UserEntity
    suspend fun createServerUser (user: UserEntity): UserEntity
    suspend fun getAppUserData (): User?
    suspend fun saveAppUserData (user: User)
}