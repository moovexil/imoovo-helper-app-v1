package com.imoovo.imoovohelper.data.entity.user

import com.imoovo.imoovohelper.domain.model.Metadata
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType
import java.util.*

data class UserEntity(
    val id: UUID?,
    val contactName: String,
    val phoneNumber: String,
    var companyName: String?,
    val userType: UserType,
    val userStatus: UserStatus,
    val email: String?,
    val country: String?,
    val region: String?,
    val city: String?,
    val metaData: Metadata?
)