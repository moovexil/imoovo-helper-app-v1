package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.data.entity.user.UserEntity
import com.imoovo.imoovohelper.data.network.RemoteDataSource
import com.imoovo.imoovohelper.data.preference.PreferenceDataSource
import com.imoovo.imoovohelper.domain.model.user.User
import javax.inject.Inject

class AppUserRepositoryImpl @Inject constructor(
    private val preferenceDataSource: PreferenceDataSource,
    private val remoteDataSource: RemoteDataSource
) : AppUserRepository {

    override suspend fun getAppUserData(): User? =
        preferenceDataSource.user

    override suspend fun saveAppUserData(user: User){
        preferenceDataSource.user = user
    }

    override suspend fun getServerUserByPhoneNumber(phoneNumber: String): UserEntity =
        remoteDataSource.getUserByPhoneNumber(phoneNumber)

    override suspend fun createServerUser(user: UserEntity): UserEntity =
        remoteDataSource.createNewUser(user)

}