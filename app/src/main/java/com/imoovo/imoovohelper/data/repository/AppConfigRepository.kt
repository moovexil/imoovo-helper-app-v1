package com.imoovo.imoovohelper.data.repository

interface AppConfigRepository {

    suspend fun getDeviceId () : String

    suspend fun getUsersIdsGroups (): Map<String,Set<String>>

    /**
     * Provides information from the remote data source about latest APK version
     * available in the Google Play. If the Google Play hasn't new version for
     * updating this matches with the [BuildConfig.VERSION_NAME].
     *
     * May not be up to date with the latest values in the remote data source (Remote Config)
     *
     * @return the new APK version available in the Google Play
     */
    suspend fun availableApkVersion(): String
}