package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.data.entity.CarrierEntity
import com.imoovo.imoovohelper.data.network.RemoteDataSource
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.filters.CarrierFilter
import com.imoovo.imoovohelper.domain.model.user.UserType
import java.util.*
import javax.inject.Inject

class CarrierRepositoryImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : CarrierRepository {

    override suspend fun createCarrierUser(user: CarrierEntity) =
        remoteDataSource.createCarrierUser(user)

    override suspend fun updateCarrierUser(user: CarrierEntity) =
        remoteDataSource.updateCarrierUser(user)

    override suspend fun getCarriersByFilters(filter: CarrierFilter): Set<CarrierEntity> =
        remoteDataSource.getCarriersByFilters(filter)

    override suspend fun getCarriersByUserStatus(userStatus: UserStatus): Set<CarrierEntity> =
        remoteDataSource.getCarriersByUserStatus(userStatus)

    override suspend fun getCarrierUserById(id: UUID): CarrierEntity =
        remoteDataSource.getCarrierUserById(id)

    override suspend fun changeUserRoleToCarrier(
        carrierUser: CarrierEntity,
        oldUserId: UUID,
        oldUserType: UserType
    ) = remoteDataSource.changeUserRoleToCarrier(carrierUser, oldUserId, oldUserType)
}