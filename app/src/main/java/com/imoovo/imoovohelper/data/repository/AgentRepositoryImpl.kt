package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.data.entity.user.UserEntity
import com.imoovo.imoovohelper.data.network.RemoteDataSource
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.filters.UserFilter
import javax.inject.Inject

class AgentRepositoryImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : AgentRepository {

    override suspend fun getAgentsByFilters(userFilter: UserFilter): Set<UserEntity> =
        remoteDataSource.getAgentsByFilters(userFilter)
}