package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.domain.model.dashboard.DashboardStatistics
import com.imoovo.imoovohelper.domain.model.user.UserType

interface DashboardRepository {

    suspend fun getDashboardStatistics (userType: UserType) : DashboardStatistics
}