package com.imoovo.imoovohelper.data.repository

import com.imoovo.imoovohelper.data.entity.user.UserEntity
import com.imoovo.imoovohelper.domain.model.filters.UserFilter
import com.imoovo.imoovohelper.domain.model.user.UserType
import java.util.*

interface BrokerRepository {

    suspend fun getBrokersByFilters(userFilter: UserFilter): Set<UserEntity>

    suspend fun updateBrokerUser(brokerUser: UserEntity)

    suspend fun changeUserRoleToBroker(
        brokerUser: UserEntity,
        oldUserId: UUID,
        oldUserType: UserType
    )
}