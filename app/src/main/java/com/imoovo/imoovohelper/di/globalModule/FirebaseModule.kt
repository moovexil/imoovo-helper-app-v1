package com.imoovo.imoovohelper.di.globalModule

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.imoovo.imoovohelper.R
import dagger.Module
import dagger.Provides
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class FirebaseModule {

    private val remoteConfigFetchInterval = TimeUnit.HOURS.toSeconds(1)

    @Singleton
    @Provides
    fun provideFirebaseRemoteConfSettings(): FirebaseRemoteConfigSettings =
        FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(remoteConfigFetchInterval)
            .build()

    @Singleton
    @Provides
    fun provideFirebaseRemoteConfig(
        configSettings: FirebaseRemoteConfigSettings
    ): FirebaseRemoteConfig {
        val remoteConfig = FirebaseRemoteConfig.getInstance()
        return remoteConfig.apply {
            setConfigSettingsAsync(configSettings)
            setDefaultsAsync(R.xml.remote_config_defaults)
            fetchAndActivate()
        }
    }
}