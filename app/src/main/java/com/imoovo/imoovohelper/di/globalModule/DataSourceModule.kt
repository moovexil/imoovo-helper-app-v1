package com.imoovo.imoovohelper.di.globalModule

import com.imoovo.imoovohelper.data.cloud.CloudAuthDataSource
import com.imoovo.imoovohelper.data.cloud.CloudAuthDataSourceImpl
import com.imoovo.imoovohelper.data.deveice_info.DeviceInfoDataSource
import com.imoovo.imoovohelper.data.deveice_info.DeviceInfoDataSourceImpl
import com.imoovo.imoovohelper.data.preference.PreferenceDataSource
import com.imoovo.imoovohelper.data.preference.SharedPreferenceStore
import com.imoovo.imoovohelper.data.remote_config.AppConfigDataStore
import com.imoovo.imoovohelper.data.remote_config.RemoteAppConfigDataSource
import dagger.Binds
import dagger.Module
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@Module
interface DataSourceModule {

    @Binds
    @Singleton
    fun bindConfigDataSource(impl: RemoteAppConfigDataSource): AppConfigDataStore

    @FlowPreview
    @ExperimentalCoroutinesApi
    @Binds
    @Singleton
    fun bindPreferenceDataSource(impl: SharedPreferenceStore): PreferenceDataSource

    @Binds
    @Singleton
    fun bindDebiceInfoDataSource (impl: DeviceInfoDataSourceImpl): DeviceInfoDataSource

    @Binds
    @Singleton
    @FlowPreview
    @ExperimentalCoroutinesApi
    fun bindCloudAuthDataSource (impl: CloudAuthDataSourceImpl): CloudAuthDataSource

}