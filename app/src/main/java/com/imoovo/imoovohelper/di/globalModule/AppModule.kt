package com.imoovo.imoovohelper.di.globalModule

import android.content.Context
import androidx.work.WorkManager
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.imoovo.imoovohelper.AndroidApplication
import com.imoovo.imoovohelper.data.network.ImoovoServerClient
import com.imoovo.imoovohelper.data.network.RemoteDataSource
import com.imoovo.imoovohelper.di.qualifier.AppUserType
import com.imoovo.imoovohelper.di.qualifier.SessionState
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.common_presentation.FilledMutableLiveData
import com.imoovo.imoovohelper.service.LiveNetwork
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule {

    @Provides
    fun provideContext(application: AndroidApplication): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    fun provideJsonSerializer(): Gson =
        GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
            .create()

    @Provides
    @Singleton
    fun provideWorkManager(appContext: Context) = WorkManager.getInstance(appContext)

    @Provides
    @Singleton
    fun provideRemoteDataSource(impl: ImoovoServerClient): RemoteDataSource {
        return impl.provideApi()
    }


    @Provides
    @Singleton
    fun provideLiveInternetConnection(appContext: Context) = LiveNetwork(appContext)

    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth = FirebaseAuth.getInstance()

    @Provides
    @Singleton
    @SessionState
    fun provideSession(): FilledMutableLiveData<SessionType> =
        FilledMutableLiveData(SessionType.AGENT)

    @Provides
    @Singleton
    @AppUserType
    fun provideUserType(): FilledMutableLiveData<UserType> =
        FilledMutableLiveData(UserType.AGENT)

}
