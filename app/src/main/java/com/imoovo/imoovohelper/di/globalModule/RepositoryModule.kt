package com.imoovo.imoovohelper.di.globalModule

import com.imoovo.imoovohelper.data.repository.*
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface RepositoryModule {

    @Binds
    @Singleton
    fun bindUserRepository (impl: CarrierRepositoryImpl): CarrierRepository

    @Binds
    @Singleton
    fun bindAppConfigRepository (impl: AppConfigRepositoryImpl): AppConfigRepository

    @Binds
    @Singleton
    fun bindDashboardRepository (impl: DashboardRepositoryImpl): DashboardRepository

    @Binds
    @Singleton
    fun bindAppUserRepository (impl: AppUserRepositoryImpl): AppUserRepository

    @Binds
    @Singleton
    fun bindAuthRepository (impl: AuthRepositoryImpl): AuthRepository

    @Binds
    @Singleton
    fun bindAgentRepository (impl: AgentRepositoryImpl): AgentRepository

    @Binds
    @Singleton
    fun bindBrokerRepository (impl: BrokerRepositoryImpl): BrokerRepository
}