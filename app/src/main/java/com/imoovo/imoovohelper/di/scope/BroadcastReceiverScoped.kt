
package com.imoovo.imoovohelper.di.scope

import javax.inject.Scope

/**
 * The BroadcastReceiverScoped custom scoping annotation specifies that the lifespan of a dependency be
 * the same as that of a BroadcastReceiver. This is used to annotate dependencies that behave like a
 * singleton within the lifespan of a BroadcastReceiver.
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@kotlin.annotation.Target(AnnotationTarget.TYPE, AnnotationTarget.FUNCTION)
annotation class BroadcastReceiverScoped
