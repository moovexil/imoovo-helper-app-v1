package com.imoovo.imoovohelper.di.globalModule

import androidx.lifecycle.ViewModelProvider
import com.imoovo.imoovohelper.common.factories.view_model.CustomViewModelFactory
import dagger.Binds
import dagger.Module

@Module
interface FactoriesModule {

    @Binds
    fun bindViewModelFactory(factoryCustom: CustomViewModelFactory): ViewModelProvider.Factory

}
