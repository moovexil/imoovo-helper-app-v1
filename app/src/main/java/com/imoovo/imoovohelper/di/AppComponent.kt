
package com.imoovo.imoovohelper.di

import com.imoovo.imoovohelper.AndroidApplication
import com.imoovo.imoovohelper.di.globalModule.*
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Main component of the app, created and persisted in the Application class.
 *
 * Whenever a new module is created, it should be added to the list of modules.
 * [AndroidSupportInjectionModule] is the module from Dagger.Android that helps with the
 * generation and location of subcomponents.
 */
@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        CoroutinesModule::class,
        ActivityBindingModule::class,
        FactoriesModule::class,
        FirebaseModule::class,
        DataSourceModule::class,
        RepositoryModule::class,
        WorkerModule::class
    ]
)
interface AppComponent : AndroidInjector<AndroidApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<AndroidApplication>()
}
