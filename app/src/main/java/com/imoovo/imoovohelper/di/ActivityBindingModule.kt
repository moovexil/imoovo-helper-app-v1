
package com.imoovo.imoovohelper.di

import com.imoovo.imoovohelper.di.scope.ActivityScoped
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.LauncherActivity
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.LauncherModule
import com.imoovo.imoovohelper.presentation.feature.main_flow.MainActivity
import com.imoovo.imoovohelper.presentation.feature.main_flow.MainActivityModule
import com.imoovo.imoovohelper.presentation.feature.updates.ApkUpdateActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * We want Dagger.Android to create a Subcomponent which has a parent Component of whichever module
 * ActivityBindingModule is on, in our case that will be [AppComponent]. You never
 * need to tell [AppComponent] that it is going to have all these subcomponents
 * nor do you need to tell these subcomponents that [AppComponent] exists.
 * We are also telling Dagger.Android that this generated SubComponent needs to include the
 * specified modules and be aware of a scope annotation [@ActivityScoped].
 * When Dagger.Android annotation processor runs it will create 2 subcomponents for us.
 */
@Module
interface ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector (modules = [LauncherModule::class])
    fun launcherActivityInjection(): LauncherActivity

    @ActivityScoped
    @ContributesAndroidInjector (modules = [MainActivityModule::class])
    fun mainActivityInjection(): MainActivity

    @ActivityScoped
    @ContributesAndroidInjector
    fun apkUpdateActivityInjection (): ApkUpdateActivity
}
