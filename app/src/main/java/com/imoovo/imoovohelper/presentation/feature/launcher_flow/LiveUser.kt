package com.imoovo.imoovohelper.presentation.feature.launcher_flow

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.common_presentation.FilledMutableLiveData
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.isStringFilterValid

class LiveUser {

    val fullName = MutableLiveData<String>(null)
    val phoneNumber = MutableLiveData<String>(null)
    val userType = MutableLiveData<UserType>(UserType.AGENT)
    val sessionType = MutableLiveData<SessionType>(SessionType.AGENT)
    val city = MutableLiveData<String>(null)
    val region = MutableLiveData<String>(null)
    val country = MutableLiveData<String>(null)

    private val _clearPhoneNumberInput = FilledMutableLiveData(false)
    val clearPhoneNumberInput: LiveData<Boolean> = _clearPhoneNumberInput

    fun clearPhoneNumberInputAction() {
        _clearPhoneNumberInput.value = true
    }

    fun onPhoneNumberDetected(_phoneNumber: String?) {
        phoneNumber.value = _phoneNumber
    }

    /**
     * Generates the session type as per [userType] for already created server-side users.
     *
     * @param userType the server-side user type
     * @return the [SessionType] enum token for the application session usage
     */
    fun suggestSessionTypePerUserType(userType: UserType) = when (userType) {
        UserType.ADMIN -> SessionType.ADMIN
        UserType.EX_ADMIN -> SessionType.EX_ADMIN
        UserType.BROKER -> SessionType.BROKER
        else -> SessionType.AGENT
    }

}

fun LiveUser.changeUserSessionType(userType: UserType) {
    sessionType.value = suggestSessionTypePerUserType(userType)
}

fun LiveUser.changeUserRole(type: UserType) {
    userType.value = type
    sessionType.value = suggestSessionTypePerUserType(type)
}

fun LiveUser.isPhoneNumberValid(): Boolean = phoneNumber.isStringFilterValid()

fun LiveUser.isValid(): Boolean =
            phoneNumber.isStringFilterValid() &&
            fullName.isStringFilterValid() &&
            city.isStringFilterValid() &&
            region.isStringFilterValid() &&
            country.isStringFilterValid() &&
            userType.value != null &&
            sessionType.value != null


fun LiveUser.update(user: User) {
    fullName.value = user.contactName
    phoneNumber.value = user.phoneNumber
    userType.value = user.userType
    city.value = user.city
    region.value = user.region
    country.value = user.country

    if (user.sessionType == null) {
        sessionType.value = suggestSessionTypePerUserType(user.userType)
    } else {
        sessionType.value = user.sessionType
    }
}

fun LiveUser.getUser(): User =
    User(
        contactName = fullName.value!!,
        phoneNumber = phoneNumber.value!!,
        userType = userType.value!!,
        sessionType = sessionType.value!!,
        city = city.value!!,
        region = region.value!!,
        country = country.value!!,
        userStatus = UserStatus.PENDING
    )
