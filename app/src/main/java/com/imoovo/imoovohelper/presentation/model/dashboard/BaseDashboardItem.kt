package com.imoovo.imoovohelper.presentation.model.dashboard

open class BaseDashboardItem(
    val viewType: DashboardViewType,
    val actionType: DashboardActionType,
    val title: Int,
    val color: Int
){
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BaseDashboardItem) return false

        if (viewType != other.viewType) return false
        if (actionType != other.actionType) return false
        if (title != other.title) return false
        if (color != other.color) return false

        return true
    }

    override fun hashCode(): Int {
        var result = viewType.hashCode()
        result = 31 * result + actionType.hashCode()
        result = 31 * result + title
        result = 31 * result + color
        return result
    }

    override fun toString(): String {
        return "BaseDashboardItem(viewType=$viewType, actionType=$actionType, title=$title, color=$color)"
    }


}