package com.imoovo.imoovohelper.presentation.model.dashboard

enum class DashboardViewType(val code: Int) {
    CARRIER(0),
    PAYMENT(1)
}