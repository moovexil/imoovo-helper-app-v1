package com.imoovo.imoovohelper.presentation.feature.main_flow.create_or_update_user

import androidx.lifecycle.*
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.di.qualifier.SessionState
import com.imoovo.imoovohelper.domain.interactor.broker.ChangeUserRoleToBrokerUseCase
import com.imoovo.imoovohelper.domain.interactor.broker.UpdateBrokerUserUseCase
import com.imoovo.imoovohelper.domain.interactor.carrier.ChangeUserRoleToCarrierUseCase
import com.imoovo.imoovohelper.domain.interactor.carrier.CreateCarrierUserUseCase
import com.imoovo.imoovohelper.domain.interactor.carrier.UpdateCarrierUserUseCase
import com.imoovo.imoovohelper.domain.interactor.user.CreateNewUserUseCase
import com.imoovo.imoovohelper.domain.model.*
import com.imoovo.imoovohelper.domain.model.carrier.Carrier
import com.imoovo.imoovohelper.domain.model.truck.Truck
import com.imoovo.imoovohelper.domain.model.user.ChangeUserRoleRequest
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.common_presentation.FilledMutableLiveData
import com.imoovo.imoovohelper.presentation.feature.CITIES
import com.imoovo.imoovohelper.presentation.feature.COUNTRIES
import com.imoovo.imoovohelper.presentation.feature.REGIONS
import com.imoovo.imoovohelper.presentation.feature.common_dialogs.ActionHandlerCreateTruck
import com.imoovo.imoovohelper.presentation.feature.common_dialogs.UpdateUserStatusActionHandler
import com.imoovo.imoovohelper.presentation.model.Event
import com.imoovo.imoovohelper.presentation.model.NullableEvent
import com.imoovo.imoovohelper.presentation.model.UnitEvent
import com.imoovo.imoovohelper.presentation.model.user.UserView
import com.imoovo.imoovohelper.service.LiveNetwork
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class CreateOrUpdateUserViewModel @Inject constructor(
    private val createCarrierUserUseCase: CreateCarrierUserUseCase,
    private val createNewUserUseCase: CreateNewUserUseCase,
    private val updateCarrierUserUseCase: UpdateCarrierUserUseCase,
    private val updateBrokerUserUseCase: UpdateBrokerUserUseCase,
    private val changeUserRoleToCarrierUseCase: ChangeUserRoleToCarrierUseCase,
    private val changeUserRoleToBrokerUseCase: ChangeUserRoleToBrokerUseCase,
    val isNetworkExist: LiveNetwork,
    @SessionState val sessionType: FilledMutableLiveData<SessionType>
) : ViewModel(),
    ActionHandlerCreateTruck,
    ActionHandlerUpdateTruck,
    UpdateUserStatusActionHandler {

    private val _uiState = MutableLiveData<State>(State.DEFAULT)
    val uiState: LiveData<State> = _uiState

    private val _eventMessage = MutableLiveData<Event<Int>>()
    val eventMessage: LiveData<Event<Int>> = _eventMessage

    //------------ Autocomplete data ----------------------

    val countries: LiveData<Array<String>> = FilledMutableLiveData(COUNTRIES)
    val cities: LiveData<Array<String>> = FilledMutableLiveData(CITIES)
    val regions: LiveData<Array<String>> = FilledMutableLiveData(REGIONS)

    private val isCreateUserRequest = FilledMutableLiveData(true)
    private val originalUserData = MutableLiveData<UserView>(null)
    val userDetailsView = LiveUserDetails()

    fun initViewModel(userDetails: UserView?, phoneNumber: String?) {
        if (userDetails == null) {
            userDetailsView.phoneNumber.value = phoneNumber
        } else {
            isCreateUserRequest.value = false
            originalUserData.value = userDetails
            userDetailsView.update(userDetails)
        }
    }

    val screenTitle: LiveData<Int> = originalUserData.map { user ->
        if (user != null) {
            R.string.screen_title_update_carrier
        } else {
            R.string.screen_title_create_carrier
        }
    }

    //--------------------- Update truck ----------------------------

    private val _eventAddOrUpdateTruck = MutableLiveData<NullableEvent<Truck>>()
    val eventAddOrUpdateTruck: LiveData<NullableEvent<Truck>> = _eventAddOrUpdateTruck

    fun onAddNewTruckClicked() {
        _eventAddOrUpdateTruck.value = NullableEvent()
    }

    override fun setTruckAction(truck: Truck) {
        _eventAddOrUpdateTruck.value = NullableEvent(truck)
    }

    override fun truckAction(newTruck: Truck) {
        viewModelScope.launch {
            val tmpTrucks = userDetailsView.trucks.value?.toMutableSet() ?: mutableSetOf()
            val matchedTruck = tmpTrucks
                .find { oldTruck -> oldTruck isTruckConfigurationSame newTruck }

            if (matchedTruck != null) {
                //remove matched truck from the list
                tmpTrucks.remove(matchedTruck)
                //user can update only trucks number
                val truck = Truck(
                    id = matchedTruck.id,
                    type = matchedTruck.type,
                    tonnage = matchedTruck.tonnage,
                    trucksNumber = newTruck.trucksNumber
                )
                //set updated truck to the list
                tmpTrucks.add(truck)
            } else {
                tmpTrucks.add(newTruck)
            }

            //set updated trucks to the view
            userDetailsView.trucks.value = (tmpTrucks.toList())
        }
    }

    /**
     * Search for a [Truck] with same configuration. We do this validation stage because we need
     * to prevent create another [Truck] with same configuration
     * if user want to update number of trucks.
     */
    private infix fun Truck.isTruckConfigurationSame(newTruck: Truck): Boolean =
        if (newTruck.id != null && newTruck.id == id) {
            true
        } else {
            //if no id check if truck with particular configuration already presents
            newTruck.tonnage == tonnage && newTruck.type == type
        }

    // ---------------------- Update user status ---------------------------

    private val _eventUpdateUserStatus = MutableLiveData<Event<UserStatus>>()
    val eventUpdateUserStatus: LiveData<Event<UserStatus>> = _eventUpdateUserStatus

    fun onUpdateUserStatusClicked() {
        _eventUpdateUserStatus.value = Event(userDetailsView.userStatus.value!!)
    }

    override fun setUserStatusAction(userStatus: UserStatus) {
        userDetailsView.userStatus.value = userStatus
    }


    // ------------------------ Create or update user -------------------------

    private val _eventUserCreatedOrUpdated = MutableLiveData<UnitEvent>()
    val eventUserCreatedOrUpdated: LiveData<UnitEvent> = _eventUserCreatedOrUpdated

    fun onCreateOrUpdateUserClicked() {
        if (userDetailsView.isValid()) {
            _uiState.value = State.LOADING

            val originalUser = originalUserData.value
            val oldUserType = originalUserData.value?.userType
            val newUserType = userDetailsView.userType.value

            viewModelScope.launch {
                if (isCreateUserRequest.value) {
                    /* for the create new request we do not need to check PREVIOUS user type
                    because this is brand new user id the DB */
                    when (newUserType) {
                        UserType.CARRIER -> {
                            val carrierUser = userDetailsView.getCarrierUser()
                            createCarrierUser(carrierUser)
                        }
                        else -> {
                            val brokerUser = userDetailsView.getUser()
                            createBrokerUser(brokerUser)
                        }
                    }
                } else {
                    /* For the update user request we need to check if user type was changed.
                       If user role wasn't change -> we need to update the current user,
                       otherwise we need to change user role */
                    when {
                        //update a carrier user
                        newUserType == UserType.CARRIER && newUserType == oldUserType -> {
                            val carrierUser = userDetailsView.getCarrierUser()
                            updateCarrierUser(carrierUser)
                        }
                        //change a carrier user role
                        newUserType == UserType.CARRIER && newUserType != oldUserType -> {
                            changeUserRoleToCarrier(
                                originalUser?.id!!,
                                originalUser.userType,
                                userDetailsView.getCarrierUser()
                            )
                        }
                        //update a broker user
                        newUserType == UserType.BROKER && newUserType == oldUserType -> {
                            val brokerUser = userDetailsView.getUser()
                            updateBrokerUser(brokerUser)
                        }
                        //change a broker user role
                        newUserType == UserType.BROKER && newUserType != oldUserType -> {
                            changeUserRoleToBroker(
                                originalUser?.id!!,
                                originalUser.userType,
                                userDetailsView.getUser()
                            )
                        }
                    }
                }
            }
        } else {
            _eventMessage.value = Event(R.string.info_mandatory_fields_absent)
        }
    }

    private suspend fun createCarrierUser(carrierUser: Carrier) {
        createCarrierUserUseCase(carrierUser)
            .run {
                _uiState.value = state
                ifSuccess {
                    _eventMessage.value = Event(R.string.info_user_created)
                    _eventUserCreatedOrUpdated.value = UnitEvent()
                }
                ifError {
                    _eventMessage.value = Event(R.string.error_server_request)
                }
            }
    }

    private suspend fun createBrokerUser(brokerUser: User) {
        createNewUserUseCase(brokerUser)
            .run {
                _uiState.value = state
                ifSuccess {
                    _eventMessage.value = Event(R.string.info_user_created)
                    _eventUserCreatedOrUpdated.value = UnitEvent()
                }
                ifError {
                    _eventMessage.value = Event(R.string.error_server_request)
                }
            }
    }

    private suspend fun updateCarrierUser(carrierUser: Carrier) {
        updateCarrierUserUseCase(carrierUser)
            .run {
                _uiState.value = state
                ifSuccess {
                    _eventMessage.value = Event(R.string.info_user_updated)
                    _eventUserCreatedOrUpdated.value = UnitEvent()
                }
                ifError {
                    _eventMessage.value = Event(R.string.error_server_request)
                }
            }
    }

    private suspend fun updateBrokerUser(brokerUser: User) {
        updateBrokerUserUseCase(brokerUser)
            .run {
                _uiState.value = state
                ifSuccess {
                    _eventMessage.value = Event(R.string.info_user_updated)
                    _eventUserCreatedOrUpdated.value = UnitEvent()
                }
                ifError {
                    _eventMessage.value = Event(R.string.error_server_request)
                }
            }
    }

    private suspend fun changeUserRoleToCarrier(
        oldUserId: UUID,
        oldUserType: UserType,
        carrierUser: Carrier
    ) {
        val request = ChangeUserRoleRequest(carrierUser, oldUserId, oldUserType)
        changeUserRoleToCarrierUseCase(request)
            .run {
                _uiState.value = state
                ifSuccess {
                    _eventMessage.value = Event(R.string.info_user_role_changed)
                    _eventUserCreatedOrUpdated.value = UnitEvent()
                }
                ifError {
                    _eventMessage.value = Event(R.string.error_server_request)
                }
            }
    }

    private suspend fun changeUserRoleToBroker(
        oldUserId: UUID,
        oldUserType: UserType,
        brokerUser: User
    ) {
        val request = ChangeUserRoleRequest(brokerUser, oldUserId, oldUserType)
        changeUserRoleToBrokerUseCase(request)
            .run {
                _uiState.value = state
                ifSuccess {
                    _eventMessage.value = Event(R.string.info_user_role_changed)
                    _eventUserCreatedOrUpdated.value = UnitEvent()
                }
                ifError {
                    _eventMessage.value = Event(R.string.error_server_request)
                }
            }
    }

}