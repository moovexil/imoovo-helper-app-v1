package com.imoovo.imoovohelper.presentation.feature.main_flow.user_details

import androidx.lifecycle.ViewModel
import com.imoovo.imoovohelper.di.scope.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface UserDetailsModule {

    @Binds
    @IntoMap
    @ViewModelKey(UserDetailsViewModel::class)
    fun bindLauncherViewModel(viewModelUpdate: UserDetailsViewModel): ViewModel
}