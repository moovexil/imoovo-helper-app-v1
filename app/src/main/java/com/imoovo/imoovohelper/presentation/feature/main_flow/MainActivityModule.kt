package com.imoovo.imoovohelper.presentation.feature.main_flow

import androidx.lifecycle.ViewModel
import com.imoovo.imoovohelper.di.scope.FragmentScoped
import com.imoovo.imoovohelper.di.scope.ViewModelKey
import com.imoovo.imoovohelper.presentation.feature.main_flow.change_session.ChangeSessionFragment
import com.imoovo.imoovohelper.presentation.feature.main_flow.change_session.ChangeSessionModule
import com.imoovo.imoovohelper.presentation.feature.main_flow.create_or_update_user.CreateOrUpdateUserFragment
import com.imoovo.imoovohelper.presentation.feature.main_flow.create_or_update_user.CreateOrUpdateUserModule
import com.imoovo.imoovohelper.presentation.feature.main_flow.dashboard.carrier_overview.DashboardCarrierFragment
import com.imoovo.imoovohelper.presentation.feature.main_flow.dashboard.carrier_overview.DashboardCarrierModule
import com.imoovo.imoovohelper.presentation.feature.main_flow.user_details.UserDetailsFragment
import com.imoovo.imoovohelper.presentation.feature.main_flow.user_details.UserDetailsModule
import com.imoovo.imoovohelper.presentation.feature.main_flow.user_search.FilterSheetFragment
import com.imoovo.imoovohelper.presentation.feature.main_flow.user_search.SearchCarrierModule
import com.imoovo.imoovohelper.presentation.feature.main_flow.user_search.SearchCarriersFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
interface MainActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    fun bindLauncherViewModel(viewModel: MainActivityViewModel): ViewModel

    @FragmentScoped
    @ContributesAndroidInjector(modules = [SearchCarrierModule::class])
    fun searchFragmentInjection(): SearchCarriersFragment

    @FragmentScoped
    @ContributesAndroidInjector(modules = [SearchCarrierModule::class])
    fun filterSheetFragmentInjections(): FilterSheetFragment

    @FragmentScoped
    @ContributesAndroidInjector(modules = [DashboardCarrierModule::class])
    fun dashboardFragmentInjections(): DashboardCarrierFragment

    @FragmentScoped
    @ContributesAndroidInjector(modules = [UserDetailsModule::class])
    fun carrierDetailsFragmentInjection(): UserDetailsFragment

    @FragmentScoped
    @ContributesAndroidInjector(modules = [CreateOrUpdateUserModule::class])
    fun createOrUpdateUserFragmentInjections(): CreateOrUpdateUserFragment


    @FragmentScoped
    @ContributesAndroidInjector (modules = [ChangeSessionModule::class])
    fun  changeSessionFragmentInjections(): ChangeSessionFragment


}