@file:JvmName("EditTextBindingAdapters")

package com.imoovo.imoovohelper.presentation.common_presentation.data_binding

import android.text.InputFilter
import android.widget.EditText
import androidx.databinding.BindingAdapter

@BindingAdapter("setInputLengthFilter")
fun setInputLengthFilter(view: EditText, length: Int) {
    view.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(length))
}
