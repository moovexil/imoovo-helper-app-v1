package com.imoovo.imoovohelper.presentation.common_presentation.extensions

import androidx.lifecycle.MutableLiveData
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.truck.TruckType

fun MutableLiveData<String>.isStringFilterValid(): Boolean {
    val data = value
    return (data != null && data.isNotEmpty() && data.isNotBlank())
}

fun MutableLiveData<Boolean>.isBooleanFilterValid(): Boolean =
    (value == true)


fun MutableLiveData<Double>.isDoubleFilterValid(): Boolean {
    val data = value
    return (data != null && data > 0)
}

fun MutableLiveData<TruckType>.isTruckTypeFilterValid(): Boolean {
    val data = value
    return data != null && data != TruckType.UNKNOWN
}

fun MutableLiveData<UserStatus>.isUserStatusFilterValid (): Boolean {
    return value != null
}
