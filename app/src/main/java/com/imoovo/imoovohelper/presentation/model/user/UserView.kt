package com.imoovo.imoovohelper.presentation.model.user

import com.imoovo.imoovohelper.domain.model.Metadata
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.carrier.Carrier
import com.imoovo.imoovohelper.domain.model.truck.Truck
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType
import java.io.Serializable
import java.util.*

class UserView : Serializable {
    var id: UUID? = null
    var contactName: String = ""
    var phoneNumber: String =""
    var userType: UserType = UserType.CARRIER
    var userStatus: UserStatus = UserStatus.PENDING
    var companyName: String? = null
    var adminComment: String? = null
    var email: String? = null
    var country: String? = null
    var region: String? = null
    var city: String? = null
    var metaData: Metadata? = null
    var trucks: List<Truck>? = null
    var cbtAvailable: Boolean? = null
    var gitAvailable: Boolean? = null
}

fun <T: User> UserView.update (user: T): UserView = apply {
    id = user.id
    metaData = user.metaData

    adminComment = user.adminComment
    contactName = user.contactName
    companyName = user.companyName
    phoneNumber = user.phoneNumber
    email = user.email
    userType = user.userType
    userStatus = user.userStatus
    city = user.city
    region = user.region
    country = user.country
    trucks = (user as? Carrier)?.trucks?.toList()
    cbtAvailable = (user as? Carrier)?.cbtAvailable
    gitAvailable = (user as? Carrier)?.gitAvailable
}

