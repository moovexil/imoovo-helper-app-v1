package com.imoovo.imoovohelper.presentation.feature.main_flow.dashboard.carrier_overview

import androidx.lifecycle.LiveData
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.carrier.Carrier

/**
 * The action handler interface that is used in conjunction with the [CarrierAdapter] to
 * handle actions which provide list items.
 */
interface ActionHandlerCarrier {

    /**
     * Provides information about current user session.
     */
    val sessionInfo: LiveData<SessionType>

    /**
     * Notifies action handler observers that the [CarrierAdapter] item was clicked.
     * It means that user clicked carrier item on the list to see more information.
     *
     * @param carrier a corresponding carrier item to the selected item on the [CarrierAdapter] list.
     */
    fun onCarrierItemSelected(carrier: Carrier)

    /**
     * Notifies action handler observers that a WatsApp message action was requested.
     *
     * @param phoneNumber a corresponding phone number to the selected item on the [CarrierAdapter] list.
     */
    fun watsAppCall(phoneNumber: String)

    /**
     * Notifies action handler observers that a phone call action was requested.
     *
     * @param phoneNumber a corresponding phone number to the selected item on the [CarrierAdapter] list.
     */
    fun phoneCall(phoneNumber: String)
}