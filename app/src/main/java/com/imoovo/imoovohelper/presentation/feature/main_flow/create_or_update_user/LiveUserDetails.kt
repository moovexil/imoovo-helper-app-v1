package com.imoovo.imoovohelper.presentation.feature.main_flow.create_or_update_user

import androidx.lifecycle.MutableLiveData
import com.imoovo.imoovohelper.common.extensions.notNullAndNotBlank
import com.imoovo.imoovohelper.domain.model.Metadata
import com.imoovo.imoovohelper.domain.model.carrier.Carrier
import com.imoovo.imoovohelper.domain.model.truck.Truck
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.common_presentation.FilledMutableLiveData
import com.imoovo.imoovohelper.presentation.model.user.UserView
import java.util.*

class LiveUserDetails {

    var id: UUID? = null
    var metaData: Metadata? = null

    val adminComment = MutableLiveData<String>()
    val contactName = MutableLiveData<String>()
    val companyName = MutableLiveData<String>()
    val phoneNumber = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val userType = MutableLiveData(UserType.CARRIER)
    val userStatus = MutableLiveData(UserStatus.PENDING)
    val city = MutableLiveData<String>()
    val region = MutableLiveData<String>()
    val country = MutableLiveData<String>()
    val trucks = MutableLiveData<List<Truck>>()
    val cbtAvailable = MutableLiveData<Boolean>()
    val gitAvailable = MutableLiveData<Boolean>()

}

fun LiveUserDetails.update (user: UserView){
    id = user.id
    metaData = user.metaData

    adminComment.value = user.adminComment
    contactName.value = user.contactName
    companyName.value = user.companyName
    phoneNumber.value = user.phoneNumber
    email.value = user.email
    userType.value = user.userType
    userStatus.value = user.userStatus
    city.value = user.city
    region.value = user.region
    country.value = user.country
    trucks.value = user.trucks
    cbtAvailable.value = user.cbtAvailable
    gitAvailable.value = user.gitAvailable
}

fun LiveUserDetails.isValid (): Boolean =
    contactName.value.notNullAndNotBlank() && phoneNumber.value.notNullAndNotBlank()

fun LiveUserDetails.getUser () : User = User(
    id = id,
    metaData = metaData,
    userStatus = userStatus.value!!,
    userType = userType.value!!,
    contactName = contactName.value!!,
    phoneNumber = phoneNumber.value!!,
    companyName = companyName.value,
    email = email.value,
    city = city.value,
    region = region.value,
    country = country.value,
    adminComment = adminComment.value
)

fun LiveUserDetails.getCarrierUser (): Carrier = Carrier(
    id = id,
    metaData = metaData,
    userStatus = userStatus.value!!,
    userType = userType.value!!,
    contactName = contactName.value!!,
    phoneNumber = phoneNumber.value!!,
    companyName = companyName.value,
    email = email.value,
    city = city.value,
    region = region.value,
    country = country.value,
    trucks = trucks.value?.toSet(),
    cbtAvailable = cbtAvailable.value,
    gitAvailable = gitAvailable.value,
    adminComment = adminComment.value
)