package com.imoovo.imoovohelper.presentation.model.dashboard

class DashboardPayment(
    viewType: DashboardViewType,
    actionType: DashboardActionType,
    title: Int,
    color: Int,
    val sum : Int,
    val currency: String
) : BaseDashboardItem(viewType, actionType, title,color){

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is DashboardPayment) return false
        if (!super.equals(other)) return false

        if (sum != other.sum) return false
        if (currency != other.currency) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + sum
        result = 31 * result + currency.hashCode()
        return result
    }

    override fun toString(): String {
        return "DashboardPayment(viewType=$viewType, actionType=$actionType, title=$title, color=$color, sum=$sum, currency='$currency')"
    }


}