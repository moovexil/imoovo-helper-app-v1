package com.imoovo.imoovohelper.presentation.feature.main_flow.user_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.domain.interactor.carrier.GetCarrierByIdUseCase
import com.imoovo.imoovohelper.domain.model.*
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.model.Event
import com.imoovo.imoovohelper.presentation.model.user.UserView
import com.imoovo.imoovohelper.presentation.model.user.update
import com.imoovo.imoovohelper.service.LiveNetwork
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class UserDetailsViewModel @Inject constructor(
    private val getUserByIdUseCase: GetCarrierByIdUseCase,
    val isNetworkAvailable: LiveNetwork
) : ViewModel() {

    private val _uiState = MutableLiveData<State>(State.DEFAULT)
    val uiState: LiveData<State> = _uiState

    private val _eventMessage = MutableLiveData<Event<Int>>()
    val eventMessage: LiveData<Event<Int>> = _eventMessage

    private val _userDetails = MutableLiveData<UserView>()
    val userDetails: LiveData<UserView> = _userDetails

    fun initViewModel(user: User) {
        when (user.userType) {
            UserType.CARRIER -> loadCarrierDetails(user.id!!)
            else -> _userDetails.value = UserView().update(user)
        }
    }

    private fun loadCarrierDetails(id: UUID) {
        _uiState.value = State.LOADING

        viewModelScope.launch {
            getUserByIdUseCase(id).run {
                _uiState.value = state
                ifSuccess { carrier ->
                    _userDetails.value = UserView().update(carrier)
                }
                ifEmpty {
                    _eventMessage.value = Event(R.string.error_not_found)
                }
                ifError {
                    _eventMessage.value = Event(R.string.error_server_request)
                }

            }
        }
    }

    private val _eventUpdateUserDetails = MutableLiveData<Event<UserView>>()
    val eventUpdateUserDetails: LiveData<Event<UserView>> = _eventUpdateUserDetails

    fun updateUserDetailsClicked() {
        //navigate to the update user details screen only if user carrier or broker
        when (_userDetails.value!!.userType) {
            UserType.CARRIER, UserType.BROKER -> {
                _eventUpdateUserDetails.value = Event(_userDetails.value!!)
            }
            else -> {
                //do nothing
            }
        }
    }

}

