package com.imoovo.imoovohelper.presentation.feature.main_flow.change_session

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.imoovo.imoovohelper.databinding.LayoutFragmentChangeSessionBinding
import com.imoovo.imoovohelper.di.qualifier.AppUserType
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.common_presentation.FilledMutableLiveData
import com.imoovo.imoovohelper.presentation.common_presentation.MainNavigationFragment
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeUnitEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.openActivity
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.toastLong
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.LauncherActivity
import javax.inject.Inject

class ChangeSessionFragment : MainNavigationFragment() {

    @Inject
    @AppUserType lateinit var appUserType: FilledMutableLiveData<UserType>

    @Inject lateinit var viewModelProvider: ViewModelProvider.Factory
    private val viewModel: ChangeSessionViewModel by viewModels { viewModelProvider }
    private lateinit var navController: NavController
    private lateinit var binding: LayoutFragmentChangeSessionBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutFragmentChangeSessionBinding.inflate(inflater, container, false)
            .apply {
                lifecycleOwner = viewLifecycleOwner
                viewModel = this@ChangeSessionFragment.viewModel
            }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navController = findNavController()

        viewModel.eventMessage.observeEvent(viewLifecycleOwner) {
            requireActivity().toastLong(it)
        }

        viewModel.eventOpenUpdateSessionScreen
            .observeEvent(viewLifecycleOwner, ::openUpdateSessionTypeScreen)

        viewModel.eventOpenLauncherScreen.observeUnitEvent(viewLifecycleOwner) {
            requireActivity().openActivity<LauncherActivity>()
            requireActivity().finish()
        }
    }

    private fun openUpdateSessionTypeScreen(currentSessionType: SessionType) {
        ChangeSessionFragmentDirections
            .actionGlobalDestinationUpdateSessionTypeDialog(viewModel, currentSessionType,appUserType.value)
            .run(navController::navigate)
    }
}