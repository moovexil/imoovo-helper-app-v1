package com.imoovo.imoovohelper.presentation.model.dashboard

enum class DashboardActionType {
    ACTION_APPROVE_CARRIERS,
    ACTION_PENDING_CARRIERS,
    ACTION_UNAVAILABLE_CARRIERS,
    ACTION_MY_APPROVED_CARRIERS,
    ACTION_MY_PENDING_CARRIERS,
    ACTION_MY_UNAVAILABLE_CARRIERS,
    ACTION_PAYMENT_DETAILS

}