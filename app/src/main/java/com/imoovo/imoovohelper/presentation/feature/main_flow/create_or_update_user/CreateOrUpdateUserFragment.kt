package com.imoovo.imoovohelper.presentation.feature.main_flow.create_or_update_user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.databinding.LayoutFragmentCreateOrUpdateUserBinding
import com.imoovo.imoovohelper.domain.model.truck.Truck
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.presentation.common_presentation.MainNavigationFragment
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeNullableEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeUnitEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.toastLong
import kotlinx.android.synthetic.main.layout_fragment_carrier_details.*
import javax.inject.Inject

class CreateOrUpdateUserFragment : MainNavigationFragment() {

    @Inject lateinit var viewModelProvider: ViewModelProvider.Factory
    val viewModel: CreateOrUpdateUserViewModel by viewModels { viewModelProvider }
    private val args: CreateOrUpdateUserFragmentArgs by navArgs()
    private lateinit var navController: NavController
    private lateinit var binding: LayoutFragmentCreateOrUpdateUserBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(args){
            viewModel.initViewModel(userDetails,phoneNumber)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutFragmentCreateOrUpdateUserBinding.inflate(inflater,container,false)
            .apply {
                lifecycleOwner = viewLifecycleOwner
                viewModel = this@CreateOrUpdateUserFragment.viewModel
            }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navController = findNavController()

        viewModel.eventMessage.observeEvent(viewLifecycleOwner){
            requireActivity().toastLong(it)
        }

        viewModel.eventAddOrUpdateTruck
            .observeNullableEvent(viewLifecycleOwner, ::openUpdateOrCreateTruckDialog)

        viewModel.eventUpdateUserStatus
            .observeEvent(viewLifecycleOwner,::openUpdateUserStatusDialog)

        viewModel.userDetailsView.trucks.observe(viewLifecycleOwner,::displayTrucks)

        viewModel.eventUserCreatedOrUpdated.observeUnitEvent(viewLifecycleOwner){
            navController.navigate(R.id.action_destination_createOrUpdateUser_to_destination_searchCarrier)
        }
    }

    private fun displayTrucks(trucks: List<Truck>?) {
        if (recyclerView.adapter == null) {
            recyclerView.adapter = UpdatableTruckAdapter(viewModel)
        }

        if (trucks.isNullOrEmpty()) {
            recyclerView.visibility = View.GONE
        } else {
            recyclerView.visibility = View.VISIBLE
            (recyclerView.adapter as UpdatableTruckAdapter).submitList(trucks)
        }
    }

    private fun openUpdateOrCreateTruckDialog(truck: Truck?){
        CreateOrUpdateUserFragmentDirections
            .actionGlobalDestinationCreateOrUpdateTruck(truck,viewModel)
            .run(navController::navigate)
    }

    private fun openUpdateUserStatusDialog (userStatus: UserStatus){
        CreateOrUpdateUserFragmentDirections
            .actionGlobalDestinationUpdateUserStatusDialog(userStatus,viewModel)
            .run(navController::navigate)
    }
}