@file:JvmName("CreateOrUpdateTruck")

package com.imoovo.imoovohelper.presentation.feature.common_dialogs

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.navArgs
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.databinding.LayoutDialogCreateOrUpdateTruckBinding
import com.imoovo.imoovohelper.domain.model.truck.Truck
import com.imoovo.imoovohelper.presentation.common_presentation.FilledMutableLiveData
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.inflater
import com.imoovo.imoovohelper.presentation.feature.TRUCK_TYPES
import com.imoovo.imoovohelper.presentation.feature.main_flow.create_or_update_user.TruckView
import com.imoovo.imoovohelper.presentation.feature.main_flow.create_or_update_user.getTruck
import com.imoovo.imoovohelper.presentation.feature.main_flow.create_or_update_user.update
import java.io.Serializable

interface ActionHandlerCreateTruck : Serializable {
    fun truckAction(newTruck: Truck)
}

class CreateOrUpdateTruckDialog : AppCompatDialogFragment() {

    private lateinit var actionHandler: ActionHandlerCreateTruck
    private val args: CreateOrUpdateTruckDialogArgs by navArgs()
    private lateinit var binding: LayoutDialogCreateOrUpdateTruckBinding

    private val truckTypes: LiveData<Array<String>> = FilledMutableLiveData(TRUCK_TYPES)
    private val isCreateTruckRequest = FilledMutableLiveData(false)
    private val truckData = TruckView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(args) {
            actionHandler = callback
            if (truck != null){
                truckData.update(truck)
            }else{
                isCreateTruckRequest.value = true
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DataBindingUtil.inflate(
            requireContext().inflater(),
            R.layout.layout_dialog_create_or_update_truck, null, false
        )

        binding.apply {
            lifecycleOwner = this@CreateOrUpdateTruckDialog
            truckData = this@CreateOrUpdateTruckDialog.truckData
            truckTypes = this@CreateOrUpdateTruckDialog.truckTypes
            isCreateTruckRequest = this@CreateOrUpdateTruckDialog.isCreateTruckRequest
        }

        return AlertDialog.Builder(requireContext())
            .setView(binding.root)
            .setCancelable(false)
            .setPositiveButton(R.string.btn_update) { _, _ -> updateTruck() }
            .setNegativeButton(R.string.btn_cancel) { _, _ -> dismiss() }
            .create()
    }

    private fun updateTruck() {
        actionHandler.truckAction(truckData.getTruck())
        dismiss()
    }

}

