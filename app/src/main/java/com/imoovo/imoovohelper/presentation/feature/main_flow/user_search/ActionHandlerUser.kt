package com.imoovo.imoovohelper.presentation.feature.main_flow.user_search

import androidx.lifecycle.LiveData
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.user.User

/**
 * The action handler interface that is used in conjunction with the [UserAdapter] to
 * handle actions which provide list items.
 */
interface ActionHandlerUser {

    /**
     * Provides information about current user session.
     */
    val sessionInfo: LiveData<SessionType>

    /**
     * Notifies action handler observers that the [UserAdapter] item was clicked.
     * It means that user clicked carrier item on the list to see more information.
     *
     * @param user a corresponding carrier item to the selected item on the [UserAdapter] list.
     */
    fun onCarrierItemSelected(user: User)

    /**
     * Notifies action handler observers that a WatsApp message action was requested.
     *
     * @param phoneNumber a corresponding phone number to the selected item on the [UserAdapter] list.
     */
    fun watsAppCall(phoneNumber: String)

    /**
     * Notifies action handler observers that a phone call action was requested.
     *
     * @param phoneNumber a corresponding phone number to the selected item on the [UserAdapter] list.
     */
    fun phoneCall(phoneNumber: String)
}