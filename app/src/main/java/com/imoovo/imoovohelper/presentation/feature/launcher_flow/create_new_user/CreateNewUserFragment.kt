package com.imoovo.imoovohelper.presentation.feature.launcher_flow.create_new_user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.databinding.LayoutFragmentCreateNewUserBinding
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeUnitEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.openActivity
import com.imoovo.imoovohelper.presentation.feature.main_flow.MainActivity
import com.imoovo.imoovohelper.presentation.common_presentation.MainNavigationFragment
import com.imoovo.imoovohelper.presentation.feature.common_dialogs.UpdateUserTypeActionHandler
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.LauncherViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class CreateNewUserFragment : MainNavigationFragment(), UpdateUserTypeActionHandler {

    @Inject lateinit var viewModelProvider: ViewModelProvider.Factory
    private val viewModel: LauncherViewModel by activityViewModels { viewModelProvider }
    private lateinit var navController: NavController
    private lateinit var binding: LayoutFragmentCreateNewUserBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutFragmentCreateNewUserBinding.inflate(inflater,container,false)
            .apply {
                lifecycleOwner = viewLifecycleOwner
                viewModel = this@CreateNewUserFragment.viewModel
            }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navController = findNavController()

        viewModel.eventUpdateUserType.observeEvent(viewLifecycleOwner, ::openUpdateUserTypeDialog)

        viewModel.eventOpenHomeScreen.observeUnitEvent(viewLifecycleOwner){
            requireActivity().openActivity<MainActivity>()
            requireActivity().finish()
        }
        viewModel.eventOpenSmsVerificationScreen.observeUnitEvent(viewLifecycleOwner){
            navController.navigate(R.id.action_destination_createNewUser_to_destination_smsVerification)
        }

        viewModel.eventOpenApproveScreen.observeUnitEvent(viewLifecycleOwner){
            navController.navigate(R.id.action_destination_createNewUser_to_destination_approve)
        }
    }

    override fun userTypeAction(userType: UserType) {
        viewModel.updateUserType(userType)
    }

    private fun openUpdateUserTypeDialog (userType: UserType){
        CreateNewUserFragmentDirections
            .actionDestinationCreateNewUserToDestinationUpdateUserType(userType,this)
            .run { navController.navigate(this) }
    }



}