package com.imoovo.imoovohelper.presentation.feature.launcher_flow

import androidx.lifecycle.ViewModel
import com.imoovo.imoovohelper.di.scope.FragmentScoped
import com.imoovo.imoovohelper.di.scope.ViewModelKey
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.approve.ApproveFragment
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.create_new_user.CreateNewUserFragment
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.phone_verification.PhoneVerificationFragment
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.sms_verification.SmsVerificationFragment
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.splash_screen_placeholder.SplashFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi

@Module
@ExperimentalCoroutinesApi
interface LauncherModule {

    @Binds
    @IntoMap
    @ViewModelKey(LauncherViewModel::class)
    fun bindLauncherViewModel (viewModel: LauncherViewModel): ViewModel

    @FragmentScoped
    @ContributesAndroidInjector
    fun splashFragmentInjections (): SplashFragment

    @FragmentScoped
    @ContributesAndroidInjector
    fun phoneVerificationFragmentInjections(): PhoneVerificationFragment

    @FragmentScoped
    @ContributesAndroidInjector
    fun approveFragmentInjections() : ApproveFragment

    @FragmentScoped
    @ContributesAndroidInjector
    fun createNewUserFragmentInjections(): CreateNewUserFragment

    @FragmentScoped
    @ContributesAndroidInjector
    fun smsVerificationFragmentInjection (): SmsVerificationFragment

}