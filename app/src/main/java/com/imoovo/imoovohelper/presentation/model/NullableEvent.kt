package com.imoovo.imoovohelper.presentation.model

import androidx.lifecycle.Observer

/**
 * Used as a wrapper for data that is exposed via a LiveData that represents an event.
 */
open class NullableEvent<out T>(private val content: T? = null) {

    private var hasBeenHandled = false

    fun hasBeenHandled() = hasBeenHandled

    /**
     * Returns the content and prevents its use again.
     */
    fun getContent(): T? {
        hasBeenHandled = true
        return content
    }

}

/**
 * An [Observer] for [Event]s, simplifying the pattern of checking if the [Event]'s content has
 * already been handled.
 *
 * [onEventUnhandledContent] is *only* called if the [Event]'s contents has not been handled.
 */
class NullableEventObserver<T>(private val onEventUnhandledContent: (T?) -> Unit) :
    Observer<NullableEvent<T?>> {

    override fun onChanged(event: NullableEvent<T?>) {
       if (!event.hasBeenHandled()){
           onEventUnhandledContent(event.getContent())
       }
    }

}
