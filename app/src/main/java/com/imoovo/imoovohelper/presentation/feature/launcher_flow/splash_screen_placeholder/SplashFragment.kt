package com.imoovo.imoovohelper.presentation.feature.launcher_flow.splash_screen_placeholder

import android.os.Bundle
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeUnitEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.openActivity
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.setDefaultWindowAppearance
import com.imoovo.imoovohelper.presentation.feature.main_flow.MainActivity
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.LauncherViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class SplashFragment : DaggerFragment() {

    @Inject lateinit var viewModelProvider: ViewModelProvider.Factory
    private val viewModel: LauncherViewModel by activityViewModels { viewModelProvider }
    private lateinit var navController: NavController

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navController = findNavController()

        viewModel.eventOpenPhoneValidationScreen.observeUnitEvent(this) {
            requireActivity().setDefaultWindowAppearance()
            navController.navigate(R.id.action_destination_splashFragment_to_destination_phoneValidation)
        }

        viewModel.eventOpenSmsVerificationScreen.observeUnitEvent(this) {
            requireActivity().setDefaultWindowAppearance()
            navController.navigate(R.id.action_destination_splashFragment_to_destination_smsVerification)
        }

        viewModel.eventOpenApproveScreen.observeUnitEvent(this) {
            requireActivity().setDefaultWindowAppearance()
            navController.navigate(R.id.action_destination_splashFragment_to_destination_approve)
        }

        viewModel.eventOpenApkUpdateScreen.observeUnitEvent(this){
            requireActivity().setDefaultWindowAppearance()
            navController.navigate(R.id.action_destination_splashFragment_to_destination_updateAppVersion)
        }

        viewModel.eventOpenHomeScreen.observeUnitEvent(this) {
            requireActivity().openActivity<MainActivity>()
            requireActivity().finish()
        }
    }

}