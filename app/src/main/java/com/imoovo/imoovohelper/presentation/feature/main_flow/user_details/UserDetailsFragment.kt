package com.imoovo.imoovohelper.presentation.feature.main_flow.user_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.databinding.LayoutFragmentCarrierDetailsBinding
import com.imoovo.imoovohelper.domain.model.truck.Truck
import com.imoovo.imoovohelper.presentation.common_presentation.MainNavigationFragment
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.toastLong
import com.imoovo.imoovohelper.presentation.model.user.UserView
import kotlinx.android.synthetic.main.layout_fragment_carrier_details.*
import javax.inject.Inject

class UserDetailsFragment : MainNavigationFragment() {

    @Inject lateinit var viewModelProvider: ViewModelProvider.Factory
    private val viewModel: UserDetailsViewModel by viewModels { viewModelProvider }
    private val args: UserDetailsFragmentArgs by navArgs()
    private lateinit var navController: NavController
    private lateinit var binding: LayoutFragmentCarrierDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        applyArgs()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutFragmentCarrierDetailsBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = this@UserDetailsFragment.viewModel
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initToolbarMenuListener()

        navController = findNavController()

        viewModel.userDetails.observe(viewLifecycleOwner) {
            displayTrucks(it.trucks)
        }
        viewModel.eventMessage.observeEvent(viewLifecycleOwner) {
            requireActivity().toastLong(it)
        }

        viewModel.eventUpdateUserDetails.observeEvent(viewLifecycleOwner, ::openUpdateUserScreen)
    }

    private fun applyArgs() {
        args.userDetails.run {
            viewModel.initViewModel(this)
        }
    }

    private fun initToolbarMenuListener() {
        toolbar.run {
            setOnMenuItemClickListener { item ->
                if (item.itemId == R.id.action_updateCarrier) {
                    viewModel.updateUserDetailsClicked()
                    true
                } else {
                    false
                }
            }
        }
    }

    private fun displayTrucks(trucks: List<Truck>?) {
        if (recyclerView.adapter == null) {
            recyclerView.adapter = TruckAdapter()
        }

        if (trucks.isNullOrEmpty()) {
            recyclerView.visibility = View.GONE
        } else {
            recyclerView.visibility = View.VISIBLE
            (recyclerView.adapter as TruckAdapter).submitList(trucks)
        }
    }

    private fun openUpdateUserScreen(user: UserView) {
        UserDetailsFragmentDirections
            .actionDestinationCarrierDetailsToDestinationCreateOrUpdateUser(user)
            .run(navController::navigate)
    }

}