package com.imoovo.imoovohelper.presentation.model

import androidx.lifecycle.MutableLiveData
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.domain.model.truck.TruckType
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.*

class CarrierFiltersView {
    val userType = MutableLiveData<UserType>(null)
    val userStatus = MutableLiveData<UserStatus>(null)
    val phoneNumber = MutableLiveData<String>(null)
    val city = MutableLiveData<String>(null)
    val region = MutableLiveData<String>(null)
    val country = MutableLiveData<String>(null)
    val hasGitDoc = MutableLiveData<Boolean>(false)
    val cbtAvailable = MutableLiveData<Boolean>(false)
    val truckType = MutableLiveData<TruckType>(TruckType.UNKNOWN)
    val truckTonnage = MutableLiveData<Double>(null)

    /* helper field to solve library problems to clear view when reset filters
     after country was changed */
    val isClearPhoneNumberInput = MutableLiveData<Boolean>(false)

    fun onCountryChanged() {
        isClearPhoneNumberInput.value = true
    }

    fun onPhoneNumberDetected(number: String?) {
        phoneNumber.value = number
    }

    override fun toString(): String {
        return buildString {
            append("[CarrierFiltersView -> ")
            append(" userType: ${userType.value},")
            append(" userStatus: ${userStatus.value},")
            append(" phoneNumber: ${phoneNumber.value},")
            append(" city: ${city.value},")
            append(" country: ${country.value},")
            append(" hasGitDoc: ${hasGitDoc.value},")
            append(" cbtAvailable: ${cbtAvailable.value}, ")
            append(" truckType: ${truckType.value}, ")
            append(" truckTonnage: ${truckType.value} ]")
        }
    }
}

fun CarrierFiltersView.setDefaults(sessionType: SessionType?) {
    when (sessionType) {
        SessionType.ADMIN, SessionType.EX_ADMIN ->
            this.apply {
                userType.value = UserType.CARRIER
                userStatus.value = null
                phoneNumber.value = ""
                city.value = ""
                region.value = ""
                country.value = ""
                hasGitDoc.value = false
                cbtAvailable.value = false
                truckType.value = TruckType.UNKNOWN
                truckTonnage.value = -1.0
                isClearPhoneNumberInput.value = true
            }

        else -> this.apply {
            userType.value = null
            userStatus.value = null
            phoneNumber.value = ""
            city.value = ""
            region.value = ""
            country.value = ""
            hasGitDoc.value = false
            cbtAvailable.value = false
            truckType.value = TruckType.UNKNOWN
            truckTonnage.value = -1.0
            isClearPhoneNumberInput.value = true
        }
    }
}

/**
 * Check if any filters was applied based on required user type
 */
fun CarrierFiltersView.hasAnyFilters(): Boolean =
    when (userType.value) {
        UserType.CARRIER ->
                userStatus.isUserStatusFilterValid() ||
                city.isStringFilterValid() ||
                region.isStringFilterValid() ||
                country.isStringFilterValid() ||
                hasGitDoc.isBooleanFilterValid() ||
                cbtAvailable.isBooleanFilterValid() ||
                truckType.isTruckTypeFilterValid() ||
                truckTonnage.isDoubleFilterValid()

        UserType.BROKER, UserType.AGENT ->
                    userStatus.isUserStatusFilterValid() ||
                    city.isStringFilterValid() ||
                    region.isStringFilterValid() ||
                    country.isStringFilterValid()

        // this is an agent user
        else -> phoneNumber.isStringFilterValid()
    }


/**
 * Counts number of applied filters.
 */
fun CarrierFiltersView.appliedFiltersNumber(): Int {
    var appliedFilterCount = 0

    when (userType.value) {
        UserType.CARRIER -> {
            if (userStatus.isUserStatusFilterValid()) {
                appliedFilterCount++
            }
            if (city.isStringFilterValid()) {
                appliedFilterCount++
            }
            if (region.isStringFilterValid()) {
                appliedFilterCount++
            }
            if (country.isStringFilterValid()) {
                appliedFilterCount++
            }
            if (hasGitDoc.isBooleanFilterValid()) {
                appliedFilterCount++
            }
            if (cbtAvailable.isBooleanFilterValid()) {
                appliedFilterCount++
            }
            if (truckType.isTruckTypeFilterValid()) {
                appliedFilterCount++
            }
            if (truckTonnage.isDoubleFilterValid()) {
                appliedFilterCount++
            }
        }


        UserType.BROKER, UserType.AGENT -> {
            if (userStatus.isUserStatusFilterValid()) {
                appliedFilterCount++
            }
            if (city.isStringFilterValid()) {
                appliedFilterCount++
            }
            if (region.isStringFilterValid()) {
                appliedFilterCount++
            }
            if (country.isStringFilterValid()) {
                appliedFilterCount++
            }
        }

        // this is an agent user
        else -> {
            if (phoneNumber.isStringFilterValid()) {
                appliedFilterCount++
            }
        }
    }

    return appliedFilterCount
}



