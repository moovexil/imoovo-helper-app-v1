package com.imoovo.imoovohelper.presentation.feature.main_flow.dashboard.carrier_overview

import androidx.lifecycle.*
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.di.qualifier.SessionState
import com.imoovo.imoovohelper.domain.interactor.carrier.GetCarriersByUserStatus
import com.imoovo.imoovohelper.domain.model.*
import com.imoovo.imoovohelper.domain.model.carrier.Carrier
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.presentation.common_presentation.FilledMutableLiveData
import com.imoovo.imoovohelper.presentation.model.Event
import javax.inject.Inject

enum class RequireCarrierType {
    APPROVED,
    PENDING,
    UNAVAILABLE
}

class DashboardViewModelCarrier @Inject constructor(
    private val getCarriersByUserStatus: GetCarriersByUserStatus,
    @SessionState private val sessionType: FilledMutableLiveData<SessionType>
) : ViewModel(),
    ActionHandlerCarrier {


    private val _uiState = MutableLiveData<State>(State.DEFAULT)
    val uiState: LiveData<State> = _uiState

    private val _eventMessage = MutableLiveData<Event<Int>>()
    val eventMessage: LiveData<Event<Int>> = _eventMessage

    override val sessionInfo: LiveData<SessionType> = sessionType

    private val requireCarriersType = MutableLiveData<RequireCarrierType>()

    /**
     * Provide information about the requested carrier type Dashboard info which
     * [DashboardViewModelCarrier] must to load.
     */
    fun setRequireCarrierType (type: RequireCarrierType){
        requireCarriersType.value = type
    }


    /**
     * Provides carriers with the [UserStatus] corresponding to the [requireCarriersType] .
     */
    val carriers: LiveData<Set<Carrier>> = requireCarriersType.switchMap { requireType ->
        when (requireType!!) {
            RequireCarrierType.APPROVED -> loadCarriersByUserType(UserStatus.APPROVED)
            RequireCarrierType.PENDING -> loadCarriersByUserType(UserStatus.PENDING)
            RequireCarrierType.UNAVAILABLE -> loadCarriersByUserType(UserStatus.NO_ANSWER)
        }
    }

    /**
     * Provides screen titles corresponding to the [requireCarriersType].
     */
    val screenTitle: LiveData<Int> = requireCarriersType.map { requireType ->
        when (requireType!!) {
            RequireCarrierType.APPROVED -> R.string.screen_title_approved_carriers
            RequireCarrierType.PENDING -> R.string.screen_title_pending_carriers
            RequireCarrierType.UNAVAILABLE -> R.string.screen_title_unavailable_carriers
        }
    }

    /**
     * Loads carriers corresponding to the requested [userStatus]
     *
     * @return carriers as per [UserStatus] live data source.
     */
    private fun loadCarriersByUserType(userStatus: UserStatus): LiveData<Set<Carrier>> = liveData {
        _uiState.value = State.LOADING
        getCarriersByUserStatus(userStatus).run {
            _uiState.value = state
            ifSuccess { carriers ->
                emit(carriers)
            }
            ifError {
                _eventMessage.value = Event(R.string.error_server_request)
            }
            ifEmpty {
                println("No data found")
            }
        }
    }


    //---------------- Call \ Messaging logic --------------------------------

    /**
     * Event which notifies observers that a phone call action was requested.
     * Provides phone number to make the phone call action.
     */
    private val _eventPhoneCall = MutableLiveData<Event<String>>()
    val eventPhoneCall: LiveData<Event<String>> = _eventPhoneCall

    override fun phoneCall(phoneNumber: String) {
        _eventPhoneCall.value = Event(phoneNumber)
    }

    /**
     * Event which notifies observers that a WatsApp message action was requested.
     * Provides phone number to make the WatsApp sending message action.
     */
    private val _eventWatsAppMessage = MutableLiveData<Event<String>>()
    val eventWatsAppMessage: LiveData<Event<String>> = _eventWatsAppMessage

    override fun watsAppCall(phoneNumber: String) {
        _eventWatsAppMessage.value = Event(phoneNumber)
    }


    // --------------- Update  carrier ---------------------

    /**
     * Event which notifies observers to open update carrier details screen.
     * Provides a [Carrier] data for updating.
     */
    private val _eventShowCarrierDetails = MutableLiveData<Event<Carrier>>()
    val eventShowCarrierDetails: LiveData<Event<Carrier>> = _eventShowCarrierDetails

    override fun onCarrierItemSelected(carrier: Carrier) {
        when(sessionType.value){
            SessionType.ADMIN, SessionType.EX_ADMIN -> {
                _eventShowCarrierDetails.value = Event(carrier)
            }
            else -> {
                //do nothing for now
            }
        }
    }


}