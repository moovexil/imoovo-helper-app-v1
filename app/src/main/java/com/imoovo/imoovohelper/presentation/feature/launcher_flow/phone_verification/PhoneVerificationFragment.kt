package com.imoovo.imoovohelper.presentation.feature.launcher_flow.phone_verification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.databinding.LayoutFragmentPhoneVerificationBinding
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeUnitEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.openActivity
import com.imoovo.imoovohelper.presentation.feature.main_flow.MainActivity
import com.imoovo.imoovohelper.presentation.common_presentation.MainNavigationFragment
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.LauncherViewModel
import kotlinx.android.synthetic.main.layout_fragment_phone_verification.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class PhoneVerificationFragment : MainNavigationFragment() {

    @Inject lateinit var viewModelProvider: ViewModelProvider.Factory
    private val viewModel: LauncherViewModel by activityViewModels { viewModelProvider }
    private lateinit var navController: NavController
    private lateinit var binding: LayoutFragmentPhoneVerificationBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutFragmentPhoneVerificationBinding.inflate(inflater, container, false)
            .apply {
                lifecycleOwner = viewLifecycleOwner
                viewModel = this@PhoneVerificationFragment.viewModel
            }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ccp.registerCarrierNumberEditText(field_phoneNumber)
        navController = findNavController()

        viewModel.eventOpenSmsVerificationScreen.observeUnitEvent(viewLifecycleOwner) {
            navController.navigate(R.id.action_destination_phoneValidation_to_destination_smsVerification)
        }

        viewModel.eventOpenApproveScreen.observeUnitEvent(viewLifecycleOwner) {
            navController.navigate(R.id.action_destination_phoneValidation_to_destination_approve)
        }

        viewModel.eventOpenHomeScreen.observeUnitEvent(viewLifecycleOwner) {
            requireActivity().openActivity<MainActivity>()
            requireActivity().finish()
        }

        viewModel.eventOpenCreateNewUserScreen
            .observeEvent(viewLifecycleOwner, ::openCreateNewUserScreen)

    }

    private fun openCreateNewUserScreen (phoneNumber: String){
        PhoneVerificationFragmentDirections
            .actionDestinationPhoneValidationToDestinationCeateNewUser(phoneNumber)
            .run { navController.navigate(this) }
    }

}