@file:JvmName("SearchBindingStuff")

package com.imoovo.imoovohelper.presentation.feature.main_flow.user_search

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseMethod
import com.hbb20.CountryCodePicker
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.domain.model.user.UserType

@BindingAdapter("filtersDescriptionText")
fun setFilterDescriptionText(view: TextView, numberAppliedFilters: Int) {
    if (numberAppliedFilters > 0) {
        view.text = buildString {
            append(numberAppliedFilters)
            append(" ")
            append(view.context.getString(R.string.info_has_filters))
        }
    } else {
        view.setText(R.string.info_no_filters)
    }
}

/**
 * User type converter from the [UserType] to radio button position and wise versa
 */
object UserTypeFilterConverter {

    private enum class RadioButton(val id: Int) {
        CARRIER(R.id.b_carrier),
        AGENT(R.id.b_agent),
        BROKER(R.id.b_broker),
        All(R.id.b_all)
    }

    @InverseMethod("fromPosition")
    @JvmStatic
    fun toPosition(value: UserType?): Int = when (value) {
        UserType.CARRIER -> RadioButton.CARRIER.id
        UserType.BROKER -> RadioButton.BROKER.id
        UserType.AGENT -> RadioButton.AGENT.id
        else -> RadioButton.All.id
    }

    @JvmStatic
    fun fromPosition(position: Int): UserType? = when (position) {
        RadioButton.CARRIER.id -> UserType.CARRIER
        RadioButton.BROKER.id -> UserType.BROKER
        RadioButton.AGENT.id -> UserType.AGENT
        else -> null
    }
}


interface OnValidPhoneNumberDetected {
    fun onDetected (phoneNumber: String?)
}

@BindingAdapter ("onPhoneNumberDetected")
fun setOnPhoneNumberDetectedListener (view: CountryCodePicker, function: OnValidPhoneNumberDetected){

    view.setPhoneNumberValidityChangeListener { isValid ->
        if (isValid){
            function.onDetected(view.fullNumber)
        }else{
            function.onDetected(null)
        }
    }
}

