package com.imoovo.imoovohelper.presentation.feature.common_dialogs

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.databinding.DataBindingUtil
import androidx.databinding.InverseMethod
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.navArgs
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.databinding.LayoutDialogUpdateSessionTypeBinding
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.inflater
import java.io.Serializable

interface UpdateSessionTypeActionHandler : Serializable {
    fun userSessionAction(sessionType: SessionType)
}

class UpdateSessionTypeDialog : AppCompatDialogFragment() {

    private lateinit var actionHandler: UpdateSessionTypeActionHandler
    private lateinit var binding: LayoutDialogUpdateSessionTypeBinding
    private val args: UpdateSessionTypeDialogArgs by navArgs()
    private val appSessionType = MutableLiveData<SessionType>()
    private val appUserType = MutableLiveData<UserType>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(args) {
            actionHandler = callback
            appUserType.value = userType
            appSessionType.value = currentSession
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DataBindingUtil.inflate(
            requireContext().inflater(),
            R.layout.layout_dialog_update_session_type, null, false
        )

        binding.apply {
            lifecycleOwner = this@UpdateSessionTypeDialog
            sessionType = appSessionType
            userType = appUserType
        }

        return AlertDialog.Builder(requireContext())
            .setView(binding.root)
            .setCancelable(false)
            .setPositiveButton(getString(R.string.btn_update)) { _, _ -> updateSessionType() }
            .setNegativeButton(getString(R.string.btn_cancel)) { _, _ -> dismiss() }
            .create()
    }

    private fun updateSessionType() {
        appSessionType.value?.let { sessionType ->
            actionHandler.userSessionAction(sessionType)
        }

        dismiss()
    }
}

object SessionTypeDialogConverter {

    private enum class RadioButton(val id: Int) {
        ADMIN(R.id.b_admin),
        EX_ADMIN(R.id.b_exAdmin),
        BROKER(R.id.b_broker),
        AGENT(R.id.b_agent)
    }

    @InverseMethod("fromPosition")
    @JvmStatic
    fun toPosition(value: SessionType): Int = when (value) {
        SessionType.ADMIN -> RadioButton.ADMIN.id
        SessionType.EX_ADMIN -> RadioButton.EX_ADMIN.id
        SessionType.BROKER -> RadioButton.BROKER.id
        else -> RadioButton.AGENT.id
    }

    @JvmStatic
    fun fromPosition(position: Int): SessionType = when (position) {
        RadioButton.ADMIN.id -> SessionType.ADMIN
        RadioButton.EX_ADMIN.id -> SessionType.EX_ADMIN
        RadioButton.BROKER.id -> SessionType.BROKER
        else -> SessionType.AGENT
    }
}