package com.imoovo.imoovohelper.presentation.model

import androidx.lifecycle.Observer

/**
 * Used as a wrapper for [UnitEvent]'s which has no data and exposed from the ViewModel.
 * For example navigation event or some UI trigger ect.
 */
class UnitEvent {

    private var hasBeenHandled = false

    fun getEventIfNoHandled(): Boolean =
        if (hasBeenHandled) {
            false
        } else {
            hasBeenHandled = true
            true
        }
}

/**
 * An [Observer] for [UnitEvent]s, simplifying the pattern of checking if the [UnitEvent]'s  has
 * already been handled.
 *
 * [onEventUnhandled] is *only* called if the [UnitEvent]'s has not been handled.
 */
class UnitEventObserver(private val onEventUnhandled: () -> Unit) : Observer<UnitEvent> {
    override fun onChanged(event: UnitEvent?) {
        if (event?.getEventIfNoHandled() == true) {
            onEventUnhandled()
        }
    }
}