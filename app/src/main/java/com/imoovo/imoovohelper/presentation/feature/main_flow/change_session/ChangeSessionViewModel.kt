package com.imoovo.imoovohelper.presentation.feature.main_flow.change_session

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.di.qualifier.SessionState
import com.imoovo.imoovohelper.domain.interactor.session.ChangeSessionTypeUseCase
import com.imoovo.imoovohelper.domain.interactor.user.GetAppUserDataUseCase
import com.imoovo.imoovohelper.domain.model.*
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.presentation.common_presentation.FilledMutableLiveData
import com.imoovo.imoovohelper.presentation.feature.common_dialogs.UpdateSessionTypeActionHandler
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.LiveUser
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.getUser
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.update
import com.imoovo.imoovohelper.presentation.model.Event
import com.imoovo.imoovohelper.presentation.model.UnitEvent
import com.imoovo.imoovohelper.service.LiveNetwork
import kotlinx.coroutines.launch
import javax.inject.Inject

class ChangeSessionViewModel @Inject constructor(
    private val getAppUserDataUseCase: GetAppUserDataUseCase,
    private val changeSessionTypeUseCase: ChangeSessionTypeUseCase,
    @SessionState private val sessionState: FilledMutableLiveData<SessionType>,
    val isNetworkAvailable: LiveNetwork
) : ViewModel(), UpdateSessionTypeActionHandler {

    /**
     * Represents current UI state.
     * By default state is [State.LOADING] because this view model performs request when initiated.
     */
    private val _uiState = FilledMutableLiveData(State.LOADING)
    val uiState: LiveData<State> = _uiState

    private val _eventMessage = MutableLiveData<Event<Int>>()
    val eventMessage: LiveData<Event<Int>> = _eventMessage

    //original data from the cache
    private lateinit var originUser: User
    //user data to display in the UI
    val userView = LiveUser()

    init {
        viewModelScope.launch {
            getAppUserDataUseCase(Unit)
                .run {
                    _uiState.value = state
                    ifSuccess { userData ->
                        if (userData != null) {
                            originUser = userData
                            userView.update(userData)
                        } else {
                            throw NullPointerException("App user data can't be null")
                        }
                    }
                    ifError {
                        _eventMessage.value = Event(R.string.error_server_request)
                    }

                }
        }
    }


    //------------------ Update session type -------------------

    private val _eventOpenUpdateSessionScreen = MutableLiveData<Event<SessionType>>()
    val eventOpenUpdateSessionScreen: LiveData<Event<SessionType>> = _eventOpenUpdateSessionScreen

    fun changeUserSessionTypeClicked() {
        _eventOpenUpdateSessionScreen.value = Event(userView.sessionType.value!!)
    }

    override fun userSessionAction(sessionType: SessionType) {
        userView.sessionType.value = sessionType
    }

    //------------------- Launch new session ------------------

    private val _eventOpenLauncherScreen = MutableLiveData<UnitEvent>()
    val eventOpenLauncherScreen: LiveData<UnitEvent> = _eventOpenLauncherScreen

    fun launchNewSessionTypeClicked() {
        //launch new session only if the selected session type different from the current
        val currentSession = originUser.sessionType
        val requestedSession = userView.sessionType.value
        if (currentSession != null && requestedSession != null && currentSession != requestedSession) {
            _uiState.value = State.LOADING
            viewModelScope.launch {
                changeSessionTypeUseCase(userView.getUser())
                    .run {
                        _uiState.value = state
                        ifSuccess {
                            sessionState.value = requestedSession
                            _eventOpenLauncherScreen.value = UnitEvent()
                        }
                        ifError {
                            _eventMessage.value = Event(R.string.error_server_request)
                        }
                    }
            }
        }
    }

}
