package com.imoovo.imoovohelper.presentation.model.dashboard

class DashboardCarrier(
    viewType: DashboardViewType,
    actionType: DashboardActionType,
    title: Int,
    color: Int,
    val count: Int
) : BaseDashboardItem(viewType, actionType, title, color){

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is DashboardCarrier) return false
        if (!super.equals(other)) return false

        if (count != other.count) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + count
        return result
    }

    override fun toString(): String {
        return "DashboardCarrier(viewType=$viewType, actionType=$actionType, title=$title, color=$color, count=$count)"
    }


}