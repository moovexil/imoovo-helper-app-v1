package com.imoovo.imoovohelper.presentation.feature.main_flow.user_details

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.imoovo.imoovohelper.databinding.LayoutItemTruckBinding
import com.imoovo.imoovohelper.domain.model.truck.Truck
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.inflater

class TruckAdapter : ListAdapter<Truck, TruckViewHolder>(TruckDiff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TruckViewHolder =
        TruckViewHolder(
            LayoutItemTruckBinding.inflate(parent.inflater(), parent, false)
        )

    override fun onBindViewHolder(holderUpdate: TruckViewHolder, position: Int) {
        holderUpdate.bind(getItem(position))
    }
}

class TruckViewHolder(
    private val binding: LayoutItemTruckBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(truck: Truck) {
        binding.apply {
            truckDetails = truck
            executePendingBindings()
        }
    }
}

private object TruckDiff : DiffUtil.ItemCallback<Truck>() {
    override fun areItemsTheSame(oldItem: Truck, newItem: Truck): Boolean =
        oldItem.id == newItem.id


    override fun areContentsTheSame(oldItem: Truck, newItem: Truck): Boolean =
        oldItem == newItem

}
