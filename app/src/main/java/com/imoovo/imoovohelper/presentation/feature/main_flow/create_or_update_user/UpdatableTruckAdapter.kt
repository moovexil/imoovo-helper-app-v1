package com.imoovo.imoovohelper.presentation.feature.main_flow.create_or_update_user

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.imoovo.imoovohelper.databinding.LayoutItemTruckUpdatableBinding
import com.imoovo.imoovohelper.domain.model.truck.Truck
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.inflater

interface ActionHandlerUpdateTruck {
    fun setTruckAction(truck: Truck)
}

class UpdatableTruckAdapter(
    private val handler: ActionHandlerUpdateTruck
) : ListAdapter<Truck, UpdatableTruckViewHolder>(TruckDiff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UpdatableTruckViewHolder =
        UpdatableTruckViewHolder(
            LayoutItemTruckUpdatableBinding.inflate(parent.inflater(), parent, false),
            handler
        )

    override fun onBindViewHolder(holderUpdate: UpdatableTruckViewHolder, position: Int) {
        holderUpdate.bind(getItem(position))
    }
}

 class UpdatableTruckViewHolder(
     private val binding: LayoutItemTruckUpdatableBinding,
     private val handler: ActionHandlerUpdateTruck
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(truck: Truck) {
        binding.apply {
            truckDetails = truck
            actionHandler = handler
            executePendingBindings()
        }
    }
}

private object TruckDiff : DiffUtil.ItemCallback<Truck>() {
    override fun areItemsTheSame(oldItem: Truck, newItem: Truck): Boolean =
        oldItem.id == newItem.id


    override fun areContentsTheSame(oldItem: Truck, newItem: Truck): Boolean =
        oldItem == newItem

}
