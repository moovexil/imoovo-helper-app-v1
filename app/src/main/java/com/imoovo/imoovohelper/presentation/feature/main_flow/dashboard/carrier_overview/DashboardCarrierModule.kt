package com.imoovo.imoovohelper.presentation.feature.main_flow.dashboard.carrier_overview

import androidx.lifecycle.ViewModel
import com.imoovo.imoovohelper.di.scope.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface DashboardCarrierModule {

    @Binds
    @IntoMap
    @ViewModelKey(DashboardViewModelCarrier::class)
    fun binDushboardViewModel (viewModel: DashboardViewModelCarrier): ViewModel
}