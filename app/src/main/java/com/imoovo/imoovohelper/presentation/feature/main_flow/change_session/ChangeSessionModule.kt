package com.imoovo.imoovohelper.presentation.feature.main_flow.change_session

import androidx.lifecycle.ViewModel
import com.imoovo.imoovohelper.di.scope.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ChangeSessionModule {

    @Binds
    @IntoMap
    @ViewModelKey(ChangeSessionViewModel::class)
    fun bindLauncherViewModel(viewModelUpdate: ChangeSessionViewModel): ViewModel
}