package com.imoovo.imoovohelper.presentation.mapper

import com.imoovo.imoovohelper.domain.model.filters.CarrierFilter
import com.imoovo.imoovohelper.domain.model.filters.TruckFilter
import com.imoovo.imoovohelper.domain.model.filters.UserFilter
import com.imoovo.imoovohelper.domain.model.truck.TruckType
import com.imoovo.imoovohelper.presentation.model.CarrierFiltersView

/**
 * Maps the view model [CarrierFiltersView] to the domain [CarrierFilter].
 */
fun CarrierFiltersView.toDomainCarrierFilters(): CarrierFilter =
    CarrierFilter(
        userFilter = toDomainUserFilter(),
        truckFilter = toDomainTruckFilter(),
        hasGitDoc = hasGitDoc.value,
        cbtAvailable = cbtAvailable.value
    )

fun CarrierFiltersView.toDomainUserFilter (): UserFilter = UserFilter(
    userType = userType.value,
    userStatus = userStatus.value,
    phoneNumber = phoneNumber.value,
    country = country.value,
    region = region.value,
    city = city.value
)

fun CarrierFiltersView.toDomainTruckFilter () : TruckFilter = TruckFilter(
    type = if (truckType.value == TruckType.UNKNOWN) null else truckType.value,
    tonnage = truckTonnage.value
)