package com.imoovo.imoovohelper.presentation.mapper

import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.dashboard.DashboardStatistics
import com.imoovo.imoovohelper.presentation.model.dashboard.BaseDashboardItem
import com.imoovo.imoovohelper.presentation.model.dashboard.DashboardActionType
import com.imoovo.imoovohelper.presentation.model.dashboard.DashboardCarrier
import com.imoovo.imoovohelper.presentation.model.dashboard.DashboardViewType


/**
 * Generates the dashboard items based on the [SessionType].
 */
fun DashboardStatistics.generateDashboard(sessionType: SessionType): List<BaseDashboardItem> {
    val result = mutableListOf<BaseDashboardItem>()

    when(sessionType){
        SessionType.ADMIN, SessionType.EX_ADMIN -> {
            result.add(getPendingCarriersItem())
            result.add(getApprovedCarriersItem())
            result.add(getUnavailableCarriersItem())
        }

        SessionType.AGENT -> {
            result.add(getMyPendingCarriersItem())
            result.add(getMyApprovedCarriersItem())
            result.add(getMyUnavailableCarriersItem())
        }

        else -> {
            //return empty list for others user types for now
        }
    }

    return result
}

private fun DashboardStatistics.getPendingCarriersItem(): DashboardCarrier =
    DashboardCarrier(
        DashboardViewType.CARRIER,
        DashboardActionType.ACTION_PENDING_CARRIERS,
        R.string.dashboard_title_pending_carriers,
        R.color.color_primary,
        pendingCarriers
    )

private fun DashboardStatistics.getApprovedCarriersItem(): DashboardCarrier =
    DashboardCarrier(
        DashboardViewType.CARRIER,
        DashboardActionType.ACTION_APPROVE_CARRIERS,
        R.string.dashboard_title_approved_carriers,
        R.color.android_green,
        approvedCarriers
    )


private fun DashboardStatistics.getUnavailableCarriersItem(): DashboardCarrier =
    DashboardCarrier(
        DashboardViewType.CARRIER,
        DashboardActionType.ACTION_UNAVAILABLE_CARRIERS,
        R.string.dashboard_title_unavailable_carriers,
        R.color.android_holo_orange,
        notAnsweredCarriers
    )

private fun DashboardStatistics.getMyPendingCarriersItem(): DashboardCarrier =
    DashboardCarrier(
        DashboardViewType.CARRIER,
        DashboardActionType.ACTION_MY_PENDING_CARRIERS,
        R.string.dashboard_title_my_pending_carriers,
        R.color.color_primary,
        myPendingCarriers
    )

private fun DashboardStatistics.getMyApprovedCarriersItem(): DashboardCarrier =
    DashboardCarrier(
        DashboardViewType.CARRIER,
        DashboardActionType.ACTION_MY_APPROVED_CARRIERS,
        R.string.dashboard_title_my_approved_carriers,
        R.color.android_green,
        myApprovedCarriers
    )

private fun DashboardStatistics.getMyUnavailableCarriersItem(): DashboardCarrier =
    DashboardCarrier(
        DashboardViewType.CARRIER,
        DashboardActionType.ACTION_MY_UNAVAILABLE_CARRIERS,
        R.string.dashboard_title_my_unavailable_carriers,
        R.color.android_holo_orange,
        myNotAnsweredCarriers
    )

