package com.imoovo.imoovohelper.presentation.feature.main_flow.user_search

import androidx.lifecycle.ViewModel
import com.imoovo.imoovohelper.di.scope.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface SearchCarrierModule {

    @Binds
    @IntoMap
    @ViewModelKey(SearchCarrierViewModel::class)
    fun bindLauncherViewModel(viewModel: SearchCarrierViewModel): ViewModel
}