package com.imoovo.imoovohelper.presentation.feature.main_flow.user_search

import androidx.lifecycle.LiveData
import com.imoovo.imoovohelper.presentation.model.dashboard.BaseDashboardItem

interface DashboardActionHandler {

    /**
     * Provides information about the internet connection state.
     */
    val isConnectionExist: LiveData<Boolean>

    /**
     * Notifies observers that the dashboard item was clicked to see more details.
     */
    fun onDashboardItemSelected(item: BaseDashboardItem)
}