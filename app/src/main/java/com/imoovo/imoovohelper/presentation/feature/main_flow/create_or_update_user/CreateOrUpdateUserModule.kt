package com.imoovo.imoovohelper.presentation.feature.main_flow.create_or_update_user

import androidx.lifecycle.ViewModel
import com.imoovo.imoovohelper.di.scope.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface CreateOrUpdateUserModule {

    @Binds
    @IntoMap
    @ViewModelKey(CreateOrUpdateUserViewModel::class)
    fun bindCreateOrUpdateUserFragmetViewModel (viewModel: CreateOrUpdateUserViewModel): ViewModel
}