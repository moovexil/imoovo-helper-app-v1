package com.imoovo.imoovohelper.presentation.feature.main_flow

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.Transformation
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.observe
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.databinding.LayoutActivityMainBinding
import com.imoovo.imoovohelper.databinding.LayoutSnippetDrawerHeaderAdminsBinding
import com.imoovo.imoovohelper.di.qualifier.AppUserType
import com.imoovo.imoovohelper.di.qualifier.SessionState
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.common_presentation.FilledMutableLiveData
import com.imoovo.imoovohelper.presentation.common_presentation.NavigationHost
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.getColorCompat
import com.imoovo.imoovohelper.service.LiveNetwork
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.layout_activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(),
    NavigationHost {

    companion object {
        private val TOP_LEVEL_DESTINATIONS = setOf(
            R.id.destination_searchCarrier,
            R.id.destination_changeSession
        )
    }

    @Inject
    @AppUserType
    lateinit var appUserType: FilledMutableLiveData<UserType>

    @Inject
    @SessionState
    lateinit var appSessionType: FilledMutableLiveData<SessionType>

    @Inject
    lateinit var liveNetwork: LiveNetwork
    private lateinit var binding: LayoutActivityMainBinding
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil
            .setContentView<LayoutActivityMainBinding>(this, R.layout.layout_activity_main)
            .apply {
                lifecycleOwner = this@MainActivity
            }

        inflateDrawerMenu()
        inflateDrawerHeader()

        navController = findNavController(R.id.nav_host_fragment)

        navigationView.setupWithNavController(navController)

        liveNetwork.observe(this@MainActivity) { isConnectionExist ->
            if (isConnectionExist) hideNoConnectionBanner() else showNoConnectionBanner()
        }

    }

    override fun registerToolbarWithNavigation(toolbar: Toolbar) {
        val appBarConfig = AppBarConfiguration(TOP_LEVEL_DESTINATIONS, drawerLayout)
        toolbar.setupWithNavController(navController, appBarConfig)
    }

    /**
     * Inflates [NavigationView] menu as per app user type.
     */
    private fun inflateDrawerMenu() {
        when (appUserType.value) {
            UserType.ADMIN, UserType.EX_ADMIN ->
                navigationView.inflateMenu(R.menu.navigation_menu_admin)

            else -> navigationView.inflateMenu(R.menu.navigation_menu_agent)
        }
    }

    private fun inflateDrawerHeader (){
        when (appUserType.value) {
            UserType.ADMIN, UserType.EX_ADMIN -> {

                val binding = LayoutSnippetDrawerHeaderAdminsBinding.inflate(layoutInflater)
                    .apply {
                        lifecycleOwner = this@MainActivity
                        sessionType = appSessionType
                        userType = appUserType
                    }

                navigationView.addHeaderView(binding.root)
            }

            else -> {
                //for now only administrators have the header view
            }
        }

    }

    private fun showNoConnectionBanner() {
        connectionBanner.apply {
            //this if check protects banner from double appearance if the view lifecycle was updated
            if (visibility != View.VISIBLE) {
                //change system bar color
                window.apply {
                    addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                    statusBarColor = getColorCompat(R.color.android_holo_orange)
                }
                //shows banner
                visibility = View.VISIBLE

                //init transition animation
                val animation = object : Animation() {
                    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                        translationY = 112 * interpolatedTime
                    }
                }
                animation.duration = 350
                startAnimation(animation)
            }
        }
    }

    private fun hideNoConnectionBanner() {
        connectionBanner.apply {
            //this if check protects banner from double appearance if the view lifecycle was updated
            if (visibility == View.VISIBLE) {
                //init transition animation
                val animation = object : Animation() {
                    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                        val value = (112 - 112 * interpolatedTime)
                        translationY = value
                        //when animation finished and banner hided applies changes to the
                        //system bar
                        if (value == 0.0F) {
                            //hides banner
                            visibility = View.INVISIBLE
                            //change system bar color
                            window.apply {
                                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                                statusBarColor = getColorCompat(R.color.status_bar_scrim)
                            }
                        }
                    }
                }
                animation.duration = 250
                startAnimation(animation)
            }

        }
    }


}