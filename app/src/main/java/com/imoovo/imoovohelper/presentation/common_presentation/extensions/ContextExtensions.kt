package com.imoovo.imoovohelper.presentation.common_presentation.extensions

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.view.LayoutInflater
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.RequiresPermission
import androidx.annotation.StringRes
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.imoovo.imoovohelper.BuildConfig
import com.imoovo.imoovohelper.R

/**
 * Call startActivity() and assigning bundles and flags
 * if required.
 *
 * @param extra optional param for assigning bundles and flags for the intent
 */
inline fun <reified T : Context> Context.openActivity(noinline extra: (Intent.() -> Unit)? = null) {
    val intent = Intent(this, T::class.java)

    if (extra != null) {
        intent.extra()
    }

    startActivity(intent)
}

/**
 * Call startActivity() with specified action.
 *
 * @param actionScreen the destination action
 */
fun Context.openActionActivity(actionScreen: String) {
    Intent(actionScreen).apply { startActivity(this) }
}

/**
 * Opens device settings menu for current application.
 */
fun Context.openAppSettingsActivity() {
    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
    intent.data = Uri.parse("package:" + BuildConfig.APPLICATION_ID)
    startActivity(intent)
}

@RequiresPermission(Manifest.permission.CALL_PHONE)
fun  Context.phoneCall (phoneNumber: String){
    try {
        val actionUri = "tel:+$phoneNumber"
        val intent = Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse(actionUri)
        }
        startActivity(intent)
    } catch (e: Exception) {
        println(e)
    }
}

fun Context.whatsAppMessage (phoneNumber: String){
//    try {
//        val actionUri = "smsto:+$phoneNumber"
//        val intent = Intent(Intent.ACTION_SENDTO, Uri.parse(actionUri)).apply {
//            `package` = "com.whatsapp"
//        }
//        startActivity(intent)
//    } catch (e: ActivityNotFoundException) {
//        println(e)
//    }


}

fun Context.inflater (): LayoutInflater = LayoutInflater.from(this)

/**
 * Shows the [Toast] with the given [msgId] for text message.
 */
fun Context.toastLong(@StringRes msgId: Int) {
    Toast.makeText(this, getString(msgId), Toast.LENGTH_LONG).show()
}

/**
 * Hides android soft keyboard from [Fragment].
 */
fun Activity.hideKeyboard() {
    currentFocus?.let { view ->
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun Activity.setDefaultWindowAppearance (){
    window.setBackgroundDrawable(getDrawable(R.color.white))
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    window.statusBarColor = getColorCompat(R.color.white)
}

/**
 * Wrapper for [ResourcesCompat] to prevent building long method signature every time.
 * Also this call compatible with all android versions.
 */
fun Context.getColorCompat(@ColorRes id: Int): Int = ResourcesCompat.getColor(resources, id, theme)