package com.imoovo.imoovohelper.presentation.feature.main_flow.dashboard.carrier_overview

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.imoovo.imoovohelper.databinding.LayoutFragmentDashboardCarriersOverviewBinding
import com.imoovo.imoovohelper.domain.model.carrier.Carrier
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.phoneCall
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.toastLong
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.whatsAppMessage
import com.imoovo.imoovohelper.presentation.common_presentation.MainNavigationFragment
import kotlinx.android.synthetic.main.layout_fragment_dashboard_carriers_overview.*

import javax.inject.Inject

class DashboardCarrierFragment : MainNavigationFragment() {

    @Inject lateinit var viewModelProvider: ViewModelProvider.Factory
    private val viewModel: DashboardViewModelCarrier by viewModels { viewModelProvider }
    private val args : DashboardCarrierFragmentArgs by navArgs()
    private lateinit var binding: LayoutFragmentDashboardCarriersOverviewBinding
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setRequireCarrierType(args.dashboardContentType)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutFragmentDashboardCarriersOverviewBinding.inflate(inflater, container, false)
            .apply {
                lifecycleOwner = viewLifecycleOwner
                viewModel = this@DashboardCarrierFragment.viewModel
            }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navController = findNavController()

        viewModel.eventMessage.observeEvent(viewLifecycleOwner) {
            activity?.toastLong(it)
        }

        viewModel.eventWatsAppMessage.observeEvent(viewLifecycleOwner) {
            context?.whatsAppMessage(it)
        }

        viewModel.eventPhoneCall.observeEvent(viewLifecycleOwner, ::handlePhoneCall)
        viewModel.carriers.observe(viewLifecycleOwner, ::displayCarriers)
        viewModel.eventShowCarrierDetails.observeEvent(viewLifecycleOwner,::openCarrierDetailsDestination)
    }

    private fun displayCarriers(carriers: Set<Carrier>) {
        if (recyclerView.adapter == null) {
            recyclerView.adapter =
                CarrierAdapter(
                    viewModel
                )
        }

        if (carriers.isNullOrEmpty()) {
            recyclerView.isVisible = false
        } else {
            recyclerView.isVisible = true
            (recyclerView.adapter as CarrierAdapter).submitList(carriers.toList())
        }
    }

    private fun openCarrierDetailsDestination(carrier: Carrier) {
        DashboardCarrierFragmentDirections
            .actionDestinationDashboardOverviewToDestinationCarrierDetails(carrier)
            .run(navController::navigate)
    }

    private fun handlePhoneCall(phoneNumber: String) {
        val result = ActivityCompat.checkSelfPermission(requireContext(),
            Manifest.permission.CALL_PHONE
        )
        if (result == PackageManager.PERMISSION_GRANTED) {
            context?.phoneCall(phoneNumber)
        } else {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CALL_PHONE), 0)
        }
    }
}