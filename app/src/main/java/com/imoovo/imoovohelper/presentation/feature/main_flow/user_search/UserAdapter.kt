package com.imoovo.imoovohelper.presentation.feature.main_flow.user_search

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.imoovo.imoovohelper.databinding.LayoutItemUserBinding
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.inflater

class UserAdapter (
    private val actionHandlerUser: ActionHandlerUser
) : ListAdapter<User, UserViewHolder>(UserDiff){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder =
        UserViewHolder(
            LayoutItemUserBinding.inflate(parent.inflater(), parent, false),
            actionHandlerUser
        )


    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}

class UserViewHolder(
    private val binding: LayoutItemUserBinding,
    private val handlerUser: ActionHandlerUser
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(data: User) {
        binding.apply {
            user = data
            actionHandler = handlerUser
            executePendingBindings()
        }
    }
}

object UserDiff : DiffUtil.ItemCallback<User>() {

    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean =
        oldItem == newItem

}