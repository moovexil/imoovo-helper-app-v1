@file:JvmName("AutocompleteTextViewBindingAdapters")
package com.imoovo.imoovohelper.presentation.common_presentation.data_binding

import androidx.databinding.BindingAdapter
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.simpleAutocompleteText

@BindingAdapter("autocompleteData")
fun <T> setAutocompleteData(
    view: androidx.appcompat.widget.AppCompatAutoCompleteTextView,
    data: Array<T>
) {
    view.simpleAutocompleteText(data)
}