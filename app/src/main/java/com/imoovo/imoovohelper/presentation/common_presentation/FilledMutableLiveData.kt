package com.imoovo.imoovohelper.presentation.common_presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * Wrapper over [LiveData] to create custom behavior implementation which accepts
 * the non nullable value at the initiation state and also provides
 * non nullable value when [getValue] requested.
 *
 * This implementation has two main differences form the library version of [MutableLiveData]:
 *  1) It hasn't default empty constructor -> this prevents nullable data result when
 *  [getValue] will invoked.
 *  2) Has generic constraints which not allows to use nullable data type.
 */
open class FilledMutableLiveData<T : Any> (value: T) : LiveData<T>(value) {

    override fun getValue(): T = super.getValue() as T
    public override fun setValue(value: T) = super.setValue(value)
    public override fun postValue(value: T) = super.postValue(value)
}