package com.imoovo.imoovohelper.presentation.feature.main_flow.user_search

import androidx.lifecycle.*
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.di.qualifier.SessionState
import com.imoovo.imoovohelper.domain.interactor.GetDashboardStatisticsUseCase
import com.imoovo.imoovohelper.domain.interactor.agent.GetAgentByFiltersUseCase
import com.imoovo.imoovohelper.domain.interactor.broker.GetBrokerByFiltersUseCase
import com.imoovo.imoovohelper.domain.interactor.carrier.GetCarriersByFilersUseCase
import com.imoovo.imoovohelper.domain.interactor.user.GetUserByPhoneNumber
import com.imoovo.imoovohelper.domain.model.*
import com.imoovo.imoovohelper.domain.model.filters.CarrierFilter
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.common_presentation.FilledMutableLiveData
import com.imoovo.imoovohelper.presentation.feature.CITIES
import com.imoovo.imoovohelper.presentation.feature.COUNTRIES
import com.imoovo.imoovohelper.presentation.feature.REGIONS
import com.imoovo.imoovohelper.presentation.feature.TRUCK_TYPES
import com.imoovo.imoovohelper.presentation.feature.main_flow.dashboard.carrier_overview.RequireCarrierType
import com.imoovo.imoovohelper.presentation.mapper.generateDashboard
import com.imoovo.imoovohelper.presentation.mapper.toDomainCarrierFilters
import com.imoovo.imoovohelper.presentation.mapper.toDomainUserFilter
import com.imoovo.imoovohelper.presentation.model.*
import com.imoovo.imoovohelper.presentation.model.dashboard.BaseDashboardItem
import com.imoovo.imoovohelper.presentation.model.dashboard.DashboardActionType
import com.imoovo.imoovohelper.service.LiveNetwork
import kotlinx.coroutines.launch
import javax.inject.Inject

class SearchCarrierViewModel @Inject constructor(
    private val getCarriersByFilersUseCase: GetCarriersByFilersUseCase,
    private val getBrokerByFiltersUseCase: GetBrokerByFiltersUseCase,
    private val getAgentByFiltersUseCase: GetAgentByFiltersUseCase,
    private val getDashboardStatisticsUseCase: GetDashboardStatisticsUseCase,
    private val getUserByPhoneNumber: GetUserByPhoneNumber,
    val isNetworkAvailable: LiveNetwork,
    @SessionState private val sessionType: FilledMutableLiveData<SessionType>

) : ViewModel(), ActionHandlerUser, DashboardActionHandler {

    override val sessionInfo: LiveData<SessionType> = sessionType

    override val isConnectionExist: LiveData<Boolean> = isNetworkAvailable

    private val _uiState = FilledMutableLiveData(State.DEFAULT)
    val uiState: LiveData<State> = _uiState

    private val _eventMessage = MutableLiveData<Event<Int>>()
    val eventMessage: LiveData<Event<Int>> = _eventMessage

    val carrierFilters = CarrierFiltersView()

    //------------ Autocomplete data ----------------------

    val countries: LiveData<Array<String>> = FilledMutableLiveData(COUNTRIES)
    val cities: LiveData<Array<String>> = FilledMutableLiveData(CITIES)
    val regions: LiveData<Array<String>> = FilledMutableLiveData(REGIONS)
    val truckTypes: LiveData<Array<String>> = FilledMutableLiveData(TRUCK_TYPES)

    //------------ Layout params as per filter sheet state ------------
    private val _filtersSheetHeaderAlpha = FilledMutableLiveData(0f)
    val filtersSheetHeaderAlpha: LiveData<Float> = _filtersSheetHeaderAlpha

    private val _filtersSheetDescriptionAlpha = FilledMutableLiveData(1f)
    val filtersSheetDescriptionAlpha: LiveData<Float> = _filtersSheetDescriptionAlpha

    private val _clearFocus = FilledMutableLiveData(false)
    val clearFocus: LiveData<Boolean> = _clearFocus

    /**
     * Sets the Alpha value for the [FilterSheetFragment] view header
     */
    fun updateSheetHeaderAlpha(value: Float) {
        _filtersSheetHeaderAlpha.value = value
    }

    /**
     * Sets the Alpha value for the [FilterSheetFragment] view description
     */
    fun updateSheetDescriptionAlpha(value: Float) {
        _filtersSheetDescriptionAlpha.value = value
    }

    //----------- Carrier filters  and filters info --------------


    private val _hasAnyFilters = MutableLiveData<Boolean>(false)
    val hasAnyFilters: LiveData<Boolean> = _hasAnyFilters

    private val _appliedFiltersNumber = MutableLiveData(0)
    val appliedFiltersNumber: LiveData<Int> = _appliedFiltersNumber

    /**
     * Analyzes filters and sets filters info corresponding to the applied filters in the
     * [CarrierFilter] object.
     */
    private fun updateFiltersInfo() {
        _hasAnyFilters.value = carrierFilters.hasAnyFilters()
        _appliedFiltersNumber.value = carrierFilters.appliedFiltersNumber()
    }

    // ------------------- User status updates ------------------------------

    private val _eventUpdateUserStatus = MutableLiveData<Event<UserStatus>>()
    val eventUpdateUserStatus: LiveData<Event<UserStatus>> = _eventUpdateUserStatus

    fun onUpdateUserStatusClicked() {
        val status = carrierFilters.userStatus.value
        if (status != null) {
            _eventUpdateUserStatus.value = Event(status)
        } else {
            _eventUpdateUserStatus.value = Event(UserStatus.APPROVED)
        }
    }

    fun updateUserStatus(status: UserStatus) {
        carrierFilters.userStatus.value = status
    }

    //---------------- Search logic -------------------------------

    private val _displayUsers = MutableLiveData<List<User>>()
    val displayUsers: LiveData<List<User>> = _displayUsers

    /**
     * Initiates search by applied filters if some filters present.
     * If no filters applied displays warning message.
     */
    fun applyFilters() {
        updateFiltersInfo()
        //clears focus from UI input fields before before init data loading
        _clearFocus.value = true

        if (carrierFilters.hasAnyFilters()) {
            _uiState.value = State.LOADING
            viewModelScope.launch {

                when (carrierFilters.userType.value) {

                    UserType.CARRIER -> getCarriersByFilersUseCase(carrierFilters.toDomainCarrierFilters())
                        .run {
                            _uiState.value = state
                            ifSuccess {
                                _displayUsers.value = it
                            }
                            ifEmpty {
                                _displayUsers.value = null
                            }
                            ifError {
                                _eventMessage.value = Event(R.string.error_server_request)
                            }
                        }

                    UserType.BROKER -> getBrokerByFiltersUseCase(carrierFilters.toDomainUserFilter())
                        .run {
                            _uiState.value = state
                            ifSuccess {
                                _displayUsers.value = it
                            }
                            ifEmpty {
                                _displayUsers.value = null
                            }
                            ifError {
                                _eventMessage.value = Event(R.string.error_server_request)
                            }
                        }

                    UserType.AGENT -> getAgentByFiltersUseCase(carrierFilters.toDomainUserFilter())
                        .run {
                            _uiState.value = state
                            ifSuccess {
                                _displayUsers.value = it
                            }
                            ifEmpty {
                                _displayUsers.value = null
                            }
                            ifError {
                                _eventMessage.value = Event(R.string.error_server_request)
                            }
                        }

                    /* ELSE means that user not applied the user type and searching for all users by
                    phone number */
                    else -> getUserByPhoneNumber(carrierFilters.phoneNumber.value!!)
                        .run {
                            _uiState.value = state
                            ifSuccess {
                                _displayUsers.value = listOf(it)
                            }
                            ifEmpty {
                                _displayUsers.value = null
                            }
                            ifError {
                                _eventMessage.value = Event(R.string.error_server_request)
                            }
                        }
                }
            }
        } else {
            when (carrierFilters.userType.value) {
                /* null means that user not applied the user type and searching for all users by
                   phone number */
                null -> {
                    _eventMessage.value = Event(R.string.info_invalid_phone_number)
                }
                else -> {
                    _eventMessage.value = Event(R.string.info_no_filters)
                }
            }
        }
    }

    /**
     * Resets the [carrierFilters] data and the filters info [appliedFiltersNumber] and [_hasAnyFilters]
     */
    fun resetFilters() {
        //sets default value to the filters
        carrierFilters.setDefaults(sessionInfo.value)
        //recalculates filters info
        updateFiltersInfo()
        //clears focus from UI input fields before before init data loading
        _clearFocus.value = true
    }


    //---------------- Call logic --------------------------------

    /**
     * Event which notifies observers that a WatsApp message action was requested.
     * Provides phone number to make the WatsApp sending message action.
     */
    private val _eventWatsAppMessage = MutableLiveData<Event<String>>()
    val eventWatsAppMessage: LiveData<Event<String>> = _eventWatsAppMessage

    override fun watsAppCall(phoneNumber: String) {
        _eventWatsAppMessage.value = Event(phoneNumber)
    }

    /**
     * Event which notifies observers that a phone call action was requested.
     * Provides phone number to make the phone call action.
     */
    private val _eventPhoneCall = MutableLiveData<Event<String>>()
    val eventPhoneCall: LiveData<Event<String>> = _eventPhoneCall

    override fun phoneCall(phoneNumber: String) {
        _eventPhoneCall.value = Event(phoneNumber)
    }

    // ---------------  Details / Create carrier ---------------------

    /**
     * Event which notifies observers to open user details screen.
     */
    private val _eventShowDetails = MutableLiveData<Event<User>>()
    val eventShowCarrierDetails: LiveData<Event<User>> = _eventShowDetails

    override fun onCarrierItemSelected(user: User) {
        when (sessionType.value) {
            SessionType.ADMIN, SessionType.EX_ADMIN -> {
                _eventShowDetails.value = Event(user)
            }
            else -> {
                //do nothing
            }
        }
    }

    /**
     * Event which notifies observers to open create carrier screen.
     * Provides the phone number on which the carrier search request was.
     */
    private val _eventCrateNewCarrier = MutableLiveData<Event<String>>()
    val eventCrateNewCarrier: LiveData<Event<String>> = _eventCrateNewCarrier

    fun createNewCarrier() {
        carrierFilters.phoneNumber.value?.let { phoneNumber ->
            _eventCrateNewCarrier.value = Event(phoneNumber)
        }
    }


    //----------------------  Dashboard logic ----------------------------

    /**
     * Indicates if the dashboard content was loaded.
     * Also has dependent live data [dashboardItems] and [isRefreshing].
     *
     * Provides true if the Dashboard content was updated otherwise false.
     */
    private val _isDashboardInitiated = FilledMutableLiveData(false)

    /**
     * Initiates the dashboard statistics refresh request.
     */
    fun refreshStatisticsClicked() {
        _isDashboardInitiated.value = false
    }

    /**
     * Provides the visibility attribute for the SwipeRefreshLayout.
     */
    val isRefreshing: LiveData<Boolean> = _isDashboardInitiated.map { isDashboardInitiated ->
        !isDashboardInitiated
    }


    /**
     * Provides information for the Dashboard based on the [SessionType]. Automatically launches updates when the
     * [_isDashboardInitiated] state was changed.
     */
    val dashboardItems: LiveData<List<BaseDashboardItem>> = _isDashboardInitiated.switchMap {
        _isDashboardInitiated.switchMap { isInitiated ->
            liveData {
                if (!isInitiated) {
                    getDashboardStatisticsUseCase(getUserTypeByCurrentSession())
                        .run {
                            ifSuccess { statistics ->
                                emit(statistics.generateDashboard(sessionType.value))
                                _isDashboardInitiated.value = true
                            }
                        }
                }
            }
        }
    }
    /**
     * [SessionType] -> [UserType] mapping. We need this mapping because the dashboard statistics
     * request requires [UserType] value and can't work with the [SessionType] and also we have more
     * user types than session types.
     */
    private fun getUserTypeByCurrentSession(): UserType =
        when (sessionType.value) {
            SessionType.ADMIN -> UserType.ADMIN
            SessionType.EX_ADMIN -> UserType.EX_ADMIN
            SessionType.BROKER -> UserType.BROKER
            SessionType.AGENT -> UserType.AGENT
        }


    /**
     * Event which notifies observers to navigate to the dashboard overview screen.
     */
    private val _eventOpenDashboardOverview = MutableLiveData<Event<RequireCarrierType>>()
    val eventOpenDashboardOverview: LiveData<Event<RequireCarrierType>> =
        _eventOpenDashboardOverview


    override fun onDashboardItemSelected(item: BaseDashboardItem) {
        when (item.actionType) {
            DashboardActionType.ACTION_PENDING_CARRIERS ->
                _eventOpenDashboardOverview.value = Event(RequireCarrierType.PENDING)

            DashboardActionType.ACTION_APPROVE_CARRIERS ->
                _eventOpenDashboardOverview.value = Event(RequireCarrierType.APPROVED)

            DashboardActionType.ACTION_UNAVAILABLE_CARRIERS ->
                _eventOpenDashboardOverview.value = Event(RequireCarrierType.UNAVAILABLE)

            DashboardActionType.ACTION_PAYMENT_DETAILS -> {//do nothing for now}
            }
        }
    }

}