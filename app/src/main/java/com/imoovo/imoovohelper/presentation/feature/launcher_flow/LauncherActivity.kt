package com.imoovo.imoovohelper.presentation.feature.launcher_flow

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.*
import com.imoovo.imoovohelper.presentation.common_presentation.NavigationHost
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class LauncherActivity : DaggerAppCompatActivity(),
    NavigationHost {

    @Inject lateinit var viewModelProvider: ViewModelProvider.Factory
    private val viewModel: LauncherViewModel by viewModels { viewModelProvider }
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_activity_launcher)
        navController = findNavController(R.id.nav_host_fragment)
        viewModel.eventMessage.observeEvent(this) { toastLong(it) }
    }


    override fun registerToolbarWithNavigation(toolbar: Toolbar) {
        val topLevelDestinations = setOf(
            R.id.destination_phoneValidation,
            R.id.destination_approve
        )

        val config = AppBarConfiguration(topLevelDestinations)
        toolbar.setupWithNavController(navController,config)
    }


}
