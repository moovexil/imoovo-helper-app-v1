@file:JvmName("CreateOrUpdateCarrierBindingStuff")
package com.imoovo.imoovohelper.presentation.feature.main_flow.create_or_update_user

import androidx.databinding.InverseMethod
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.domain.model.user.UserType

/**
 * User type converter from the [UserType] to radio button position and wise versa
 */
object UserTypeFilterConverter {

    private enum class RadioButton(val id: Int) {
        CARRIER(R.id.b_carrier),
        BROKER(R.id.b_broker)
    }

    @InverseMethod("fromPosition")
    @JvmStatic
    fun toPosition(value: UserType): Int = when (value) {
        UserType.CARRIER -> RadioButton.CARRIER.id
        else -> RadioButton.BROKER.id
    }

    @JvmStatic
    fun fromPosition(position: Int): UserType = when (position) {
        RadioButton.CARRIER.id -> UserType.CARRIER
        else -> UserType.BROKER
    }
}