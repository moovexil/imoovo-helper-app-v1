package com.imoovo.imoovohelper.presentation.feature.launcher_flow

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.di.qualifier.AppUserType
import com.imoovo.imoovohelper.di.qualifier.SessionState
import com.imoovo.imoovohelper.domain.interactor.app_config.IsApkValidUseCase
import com.imoovo.imoovohelper.domain.interactor.authentication.GetUserAuthenticationStateUseCase
import com.imoovo.imoovohelper.domain.interactor.authentication.ObserveAuthenticationFlowUseCase
import com.imoovo.imoovohelper.domain.interactor.authentication.SetPhoneAuthSmsVerificationCodeUseCase
import com.imoovo.imoovohelper.domain.interactor.authentication.SignInViaPhoneNumberUseCase
import com.imoovo.imoovohelper.domain.interactor.session.GetAllowedUserSessionTypeUseCase
import com.imoovo.imoovohelper.domain.interactor.user.CacheAppUserDataUseCase
import com.imoovo.imoovohelper.domain.interactor.user.CreateNewUserUseCase
import com.imoovo.imoovohelper.domain.interactor.user.GetAppUserDataUseCase
import com.imoovo.imoovohelper.domain.interactor.user.GetUserByPhoneNumber
import com.imoovo.imoovohelper.domain.model.*
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.common_presentation.FilledMutableLiveData
import com.imoovo.imoovohelper.presentation.feature.CITIES
import com.imoovo.imoovohelper.presentation.feature.COUNTRIES
import com.imoovo.imoovohelper.presentation.feature.REGIONS
import com.imoovo.imoovohelper.presentation.model.*
import com.imoovo.imoovohelper.service.LiveNetwork
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
class LauncherViewModel @Inject constructor(
    private val getAppUserDataUseCase: GetAppUserDataUseCase,
    private val cacheAppUserDataUseCase: CacheAppUserDataUseCase,
    private val getUserAuthenticationStateUseCase: GetUserAuthenticationStateUseCase,
    private val observeAuthenticationFlowUseCase: ObserveAuthenticationFlowUseCase,
    private val setPhoneAuthSmsVerificationCodeUseCase: SetPhoneAuthSmsVerificationCodeUseCase,
    private val createNewUserUseCase: CreateNewUserUseCase,
    private val signInViaPhoneNumberUseCase: SignInViaPhoneNumberUseCase,
    private val getAllowedUserSessionTypeUseCase: GetAllowedUserSessionTypeUseCase,
    private val isApkValidUseCase: IsApkValidUseCase,
    private val getUserByPhoneNumber: GetUserByPhoneNumber,
    val isNetworkAvailable: LiveNetwork,
    @SessionState private val sessionState: FilledMutableLiveData<SessionType>,
    @AppUserType private val appUserType : FilledMutableLiveData<UserType>
) : ViewModel() {

    private val _uiState = FilledMutableLiveData(State.DEFAULT)
    val uiState: LiveData<State> = _uiState

    private val _eventMessage = MutableLiveData<Event<Int>>()
    val eventMessage: LiveData<Event<Int>> = _eventMessage

    //------------ Autocomplete data ----------------------
    val countries: LiveData<Array<String>> = FilledMutableLiveData(COUNTRIES)
    val cities: LiveData<Array<String>> = FilledMutableLiveData(CITIES)
    val regions: LiveData<Array<String>> = FilledMutableLiveData(REGIONS)

    //cached user data for interaction inside current view model
    val userView = LiveUser()

    private var isServerSideUserExist = true


    private val _eventOpenApkUpdateScreen = MutableLiveData<UnitEvent>()
    val eventOpenApkUpdateScreen : LiveData<UnitEvent> = _eventOpenApkUpdateScreen

    init {
        viewModelScope.launch {
            isApkValidUseCase(Unit)
                .run {
                    ifSuccess { isUpdateRequired ->
                        if (isUpdateRequired){
                            _eventOpenApkUpdateScreen.value = UnitEvent()
                        }else{
                            initAppStart()
                        }
                    }
                    ifError { initAppStart() }
                }
        }
    }

    private suspend fun initAppStart (){
        resolveUserData()
        observeAuthFlow()
    }

    private suspend fun cacheUserData(user: User) {
        //cache data into the backing field for usage inside viewModel
        userView.update(user)
        //cache data into the shared pref
        cacheAppUserDataUseCase(userView.getUser())
    }

    private val _eventOpenPhoneNumberValidationScreen = MutableLiveData<UnitEvent>()
    val eventOpenPhoneValidationScreen: LiveData<UnitEvent> = _eventOpenPhoneNumberValidationScreen

    private suspend fun resolveUserData() {
        getAppUserDataUseCase(Unit).run {
            ifSuccess { userData ->
                if (userData != null) {
                    //cache data into the backing field for usage inside viewModel
                    userView.update(userData)
                    //set global scope app user type
                    appUserType.value = userData.userType

                    resolveUserAuthState()
                } else {
                    _uiState.value = State.SUCCESS
                    _eventOpenPhoneNumberValidationScreen.value = UnitEvent()
                }
            }
            ifError {
                _uiState.value = State.ERROR
                _eventMessage.value = Event(R.string.error_server_request)
            }
        }
    }


    private val _eventOpenApproveScreen = MutableLiveData<UnitEvent>()
    val eventOpenApproveScreen: LiveData<UnitEvent> = _eventOpenApproveScreen

    private val _eventOpenHomeScreen = MutableLiveData<UnitEvent>()
    val eventOpenHomeScreen: LiveData<UnitEvent> = _eventOpenHomeScreen

    private suspend fun resolveUserSessionType() {
        getAllowedUserSessionTypeUseCase(Unit).run {
            _uiState.value = state
            ifSuccess { allowedSessionType ->
                val userRequiredSessionType = userView.sessionType.value
                /* If a user want to use the app like the Agent he do not need be approved for
                special admin permissions and he can start use the app immediately */
                if (userRequiredSessionType == SessionType.AGENT) {
                    _eventOpenHomeScreen.value = UnitEvent()

                } else {
                    // This is special check for users which required admin type permissions
                    if (allowedSessionType == userRequiredSessionType) {
                        //update global session type scope (by default this is Agent session)
                        sessionState.value = userRequiredSessionType
                        //launch the app
                        _eventOpenHomeScreen.value = UnitEvent()
                    } else {
                        _eventOpenApproveScreen.value = UnitEvent()
                    }
                }
            }
            ifError {
                _eventMessage.value = Event(R.string.error_server_request)
            }
        }
    }

    //----------- Phone authentication logic --------------------------

    private suspend fun resolveUserAuthState() {
        getUserAuthenticationStateUseCase(Unit).run {
            ifSuccess { authenticated ->
                if (authenticated) {
                    resolveUserSessionType()
                } else {
                    /* concatenate + char to user phone number because lib which was used to get
                    user phoneNumber provides it without + */
                    val phoneNumber = "+${userView.phoneNumber.value}"
                    signInViaPhoneNumberUseCase(phoneNumber)
                }
            }
            ifError {
                _uiState.value = State.ERROR
                _eventMessage.value = Event(R.string.error_server_request)
            }
        }
    }

    private val _eventOpenSmsVerificationScreen = MutableLiveData<UnitEvent>()
    val eventOpenSmsVerificationScreen: LiveData<UnitEvent> = _eventOpenSmsVerificationScreen

    private suspend fun observeAuthFlow() {
        observeAuthenticationFlowUseCase(Unit).collect { result ->
            result.run {
                ifSuccess { authState ->
                    // Auto authentication not allowed and a user must verify sms code manually
                    if (authState == PhoneAuthState.ON_CODE_SENT) {
                        _uiState.value = State.SUCCESS
                        _eventOpenSmsVerificationScreen.value = UnitEvent()
                    }
                    // Authentication succeed
                    else if (authState == PhoneAuthState.SUCCESS) {
                        if (isServerSideUserExist) {
                            resolveUserSessionType()
                        } else {
                            createServerSideUser()
                        }
                    }
                }
                ifError { exception ->
                    _uiState.value = State.ERROR

                    when (exception.getPhoneAuthErrorState()) {
                        PhoneAuthState.SMS_QUOTA_EXCEEDED -> _eventMessage.value =
                            Event(R.string.error_sms_quota)
                        PhoneAuthState.BAD_CREDENTIALS -> _eventMessage.value =
                            Event(R.string.error_bad_auth_credentials)
                        else -> _eventMessage.value = Event(R.string.error_auth_failed)
                    }
                }
            }
        }
    }

    fun validatePhoneNumberClicked() {
        if (userView.isPhoneNumberValid()) {
            _uiState.value = State.LOADING

            viewModelScope.launch {
                getUserByPhoneNumber(userView.phoneNumber.value!!).run {
                    ifSuccess { serverSideUser ->
                        /* if we found user on the server-side it means that locale data was cleared
                        and we need cache data from the server again
                         */
                        cacheUserData(serverSideUser)
                        resolveUserAuthState()
                        //set global scope app user type
                        appUserType.value = serverSideUser.userType
                    }
                    ifEmpty {
                        _uiState.value = State.NO_DATA
                        // if no user data next step this is create new server - side user
                        isServerSideUserExist = false
                        _eventOpenCreateNewUserScreen.value = Event(userView.phoneNumber.value!!)
                    }
                    ifError {
                        _uiState.value = State.ERROR
                        _eventMessage.value = Event(R.string.error_server_request)
                    }
                }
            }
        } else {
            _eventMessage.value = Event(R.string.info_invalid_phone_number)
        }

    }

    val smsValidationCode = MutableLiveData<String>()

    fun validateSmsCodeClicked() {
        val code = smsValidationCode.value
        if (code != null && code.length == 6) {
            _uiState.value = State.LOADING

            viewModelScope.launch {
                setPhoneAuthSmsVerificationCodeUseCase(code)
            }
        }
    }

    //------------ Approve user ------------------------

    private val _eventUpdateUserType = MutableLiveData<Event<UserType>>()
    val eventUpdateUserType: LiveData<Event<UserType>> = _eventUpdateUserType

    fun updateNotApprovedUserTypeClicked() {
        _eventUpdateUserType.value = Event(userView.userType.value!!)
    }

    fun updateNotApprovedUserType(userType: UserType) {
        //update the user role as per user type selection
        userView.changeUserSessionType(userType)

        viewModelScope.launch {
            //caches updated user data with new required user type
            userView.getUser().run { cacheUserData(this) }
            //checks  if user permissions granted for the new user type
            resolveUserSessionType()
        }
    }

    //------------ Create new server - side user --------------------------------------

    private val _eventOpenCreateNewUserScreen = MutableLiveData<Event<String>>()
    val eventOpenCreateNewUserScreen: LiveData<Event<String>> = _eventOpenCreateNewUserScreen

    fun setUserTypeClicked() {
        _eventUpdateUserType.value = Event(userView.userType.value ?: UserType.AGENT)
    }

    fun updateUserType(userType: UserType) {
        userView.changeUserRole(userType)
    }

    fun createUserClicked() {
        if (userView.isValid()) {
            _uiState.value = State.LOADING

            viewModelScope.launch {
                resolveUserAuthState()
            }
        } else {
            _eventMessage.value = Event(R.string.info_mandatory_fields_absent)
        }

    }

    private suspend fun createServerSideUser() {
        createNewUserUseCase(userView.getUser()).run {
            ifSuccess { newUser ->
                cacheUserData(newUser)
                resolveUserSessionType()
                //set global scope app user type
                appUserType.value = newUser.userType
            }
            ifError {
                _uiState.value = State.ERROR
                _eventMessage.value = Event(R.string.error_server_request)
            }
        }
    }

}