package com.imoovo.imoovohelper.presentation.feature.launcher_flow.approve

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.databinding.LayoutFragmentApproveBinding
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeUnitEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.openActivity
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.toastLong
import com.imoovo.imoovohelper.presentation.feature.main_flow.MainActivity
import com.imoovo.imoovohelper.presentation.common_presentation.MainNavigationFragment
import com.imoovo.imoovohelper.presentation.feature.common_dialogs.UpdateUserTypeActionHandler
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.LauncherViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class ApproveFragment : MainNavigationFragment(), UpdateUserTypeActionHandler {

    @Inject lateinit var viewModelProvider: ViewModelProvider.Factory
    private val viewModel: LauncherViewModel by activityViewModels { viewModelProvider }
    private lateinit var navController: NavController
    private lateinit var binding: LayoutFragmentApproveBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutFragmentApproveBinding.inflate(inflater, container, false)
            .apply {
                lifecycleOwner = viewLifecycleOwner
                viewModel = this@ApproveFragment.viewModel
            }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navController = findNavController()

        viewModel.eventUpdateUserType.observeEvent(viewLifecycleOwner, ::openUpdateUserTypeDialog)

        viewModel.eventOpenHomeScreen.observeUnitEvent(viewLifecycleOwner) {
            requireActivity().openActivity<MainActivity>()
            requireActivity().finish()
        }

        viewModel.eventOpenApproveScreen.observeUnitEvent(viewLifecycleOwner){
            /* if after user updated his type and we got this callback it means
            that user still not approved for new SessionType. Show the info message
            in this case*/
            requireActivity().toastLong(R.string.info_user_not_approved)
        }
    }

    private fun openUpdateUserTypeDialog(userType: UserType) {
        ApproveFragmentDirections
            .actionDestinationApproveToDestinationUpdateUserType(userType, this)
            .run { navController.navigate(this) }
    }

    override fun userTypeAction(userType: UserType) {
        viewModel.updateNotApprovedUserType(userType)
    }
}