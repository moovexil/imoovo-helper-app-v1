package com.imoovo.imoovohelper.presentation.feature.main_flow.create_or_update_user

import androidx.lifecycle.MutableLiveData
import com.imoovo.imoovohelper.domain.model.truck.Truck
import com.imoovo.imoovohelper.domain.model.truck.TruckType
import java.io.Serializable
import java.util.*

class TruckView : Serializable {
    var id: UUID? = null
    val type = MutableLiveData<TruckType>()
    val tonnage = MutableLiveData(1.0)
    val trucksNumber = MutableLiveData(1)
}

fun TruckView.update(truck: Truck): TruckView = apply {
    id = truck.id
    type.value = truck.type
    tonnage.value = truck.tonnage
    trucksNumber.value = truck.trucksNumber
}

fun TruckView.getTruck (): Truck = Truck(
    id = id,
    type = type.value,
    tonnage = tonnage.value,
    trucksNumber = trucksNumber.value
)