package com.imoovo.imoovohelper.presentation.common_presentation.extensions

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Spinner


/**
 * Instantiates simple [AutoCompleteTextView] implementation with primitive data.
 */
fun <T> AutoCompleteTextView.simpleAutocompleteText(data: Array<T>): AutoCompleteTextView {
    val adapter = ArrayAdapter(context, android.R.layout.simple_list_item_1, data)
    setAdapter(adapter)
    return this
}

/**
 * Instantiates simple [Spinner] implementation with primitive data.
 */
fun <T> Spinner.simpleSpinner(data: Array<T>): Spinner {
    val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, data)
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    setAdapter(adapter)
    return this
}

/**
 * Inflates the [LayoutInflater] from a given [ViewGroup].
 */
fun ViewGroup.inflater(): LayoutInflater = LayoutInflater.from(this.context)