package com.imoovo.imoovohelper.presentation.feature.launcher_flow.sms_verification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.databinding.LayoutFragmentSmsVerificationBinding
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeUnitEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.openActivity
import com.imoovo.imoovohelper.presentation.feature.main_flow.MainActivity
import com.imoovo.imoovohelper.presentation.feature.launcher_flow.LauncherViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class SmsVerificationFragment : DaggerFragment() {

    @Inject lateinit var viewModelProvider: ViewModelProvider.Factory
    private val viewModel: LauncherViewModel by activityViewModels { viewModelProvider }
    private lateinit var binding: LayoutFragmentSmsVerificationBinding
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutFragmentSmsVerificationBinding.inflate(inflater,container,false)
            .apply {
                lifecycleOwner = viewLifecycleOwner
                viewModel = this@SmsVerificationFragment.viewModel
            }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navController = findNavController()

        viewModel.eventOpenHomeScreen.observeUnitEvent(viewLifecycleOwner){
            requireActivity().openActivity<MainActivity>()
            requireActivity().finish()
        }

        viewModel.eventOpenApproveScreen.observeUnitEvent(viewLifecycleOwner){
            navController.navigate(R.id.action_destination_smsVerification_to_destination_approve)
        }
    }
}