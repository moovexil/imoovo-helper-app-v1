package com.imoovo.imoovohelper.presentation.feature.main_flow.dashboard.carrier_overview

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.imoovo.imoovohelper.databinding.LayoutItemCarrierBinding
import com.imoovo.imoovohelper.domain.model.carrier.Carrier
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.inflater

class CarrierAdapter (
    private val actionHandlerCarrier: ActionHandlerCarrier
) : ListAdapter<Carrier, CarrierViewHolder>(
    CarrierDiff
){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarrierViewHolder =
        CarrierViewHolder(
            LayoutItemCarrierBinding.inflate(parent.inflater(), parent, false),
            actionHandlerCarrier
        )


    override fun onBindViewHolder(holder: CarrierViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}

class CarrierViewHolder(
    private val binding: LayoutItemCarrierBinding,
    private val actionHandlerCarrier: ActionHandlerCarrier
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(carrierData: Carrier) {
        binding.apply {
            carrier = carrierData
            actionHandler = this@CarrierViewHolder.actionHandlerCarrier
            executePendingBindings()
        }
    }
}

object CarrierDiff : DiffUtil.ItemCallback<Carrier>() {

    override fun areItemsTheSame(oldItem: Carrier, newItem: Carrier): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Carrier, newItem: Carrier): Boolean =
        oldItem == newItem

}