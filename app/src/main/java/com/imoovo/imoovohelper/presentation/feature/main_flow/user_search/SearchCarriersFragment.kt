package com.imoovo.imoovohelper.presentation.feature.main_flow.user_search

import android.Manifest.permission.CALL_PHONE
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.databinding.LayoutFragmentSearchCarrierBinding
import com.imoovo.imoovohelper.domain.model.State
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.presentation.common_presentation.GridSpacingItemDecoration
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.observeEvent
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.phoneCall
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.toastLong
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.whatsAppMessage
import com.imoovo.imoovohelper.presentation.common_presentation.MainNavigationFragment
import com.imoovo.imoovohelper.presentation.feature.common_dialogs.UpdateUserStatusActionHandler
import com.imoovo.imoovohelper.presentation.feature.main_flow.dashboard.carrier_overview.RequireCarrierType
import com.imoovo.imoovohelper.presentation.model.dashboard.BaseDashboardItem
import kotlinx.android.synthetic.main.layout_fragment_search_carrier.*
import kotlinx.android.synthetic.main.layout_snippet_dashboard.*
import javax.inject.Inject

class SearchCarriersFragment : MainNavigationFragment(),
    UpdateUserStatusActionHandler {

    companion object {
        // Threshold for when normal header views and description views should "change places".
        // This should be a value between 0 and 1, coinciding with a point between the bottom
        // sheet's collapsed (0) and expanded (1) states.
        private const val ALPHA_CHANGEOVER = 0.33f
        // Threshold for when description views reach maximum alpha. Should be a value between
        // 0 and [ALPHA_CHANGEOVER], inclusive.
        private const val ALPHA_DESC_MAX = 0f
        // Threshold for when normal header views reach maximum alpha. Should be a value between
        // [ALPHA_CHANGEOVER] and 1, inclusive.
        private const val ALPHA_HEADER_MAX = 0.67f
    }

    @Inject lateinit var viewModelProvider: ViewModelProvider.Factory
    private val searchViewModel: SearchCarrierViewModel by viewModels { viewModelProvider }
    private lateinit var binding: LayoutFragmentSearchCarrierBinding
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<*>
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutFragmentSearchCarrierBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            searchViewModel = this@SearchCarriersFragment.searchViewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBottomSheetView(view)

        navController = findNavController()

        searchViewModel.eventMessage.observeEvent(viewLifecycleOwner) {
            activity?.toastLong(it)
        }

        searchViewModel.eventWatsAppMessage.observeEvent(viewLifecycleOwner) {
            context?.whatsAppMessage(it)
        }
        searchViewModel.eventPhoneCall.observeEvent(viewLifecycleOwner, ::handlePhoneCall)
        searchViewModel.eventShowCarrierDetails.observeEvent(viewLifecycleOwner, ::openCarrierDetailsDestination)
        searchViewModel.eventUpdateUserStatus.observeEvent(viewLifecycleOwner, ::openUpdateUserStatusDestination)
        searchViewModel.eventCrateNewCarrier.observeEvent(viewLifecycleOwner, ::openCreateNewCarrierDestination)
        searchViewModel.eventOpenDashboardOverview.observeEvent(viewLifecycleOwner,::openDashboardOverview)
        searchViewModel.dashboardItems.observe(viewLifecycleOwner,::displayDashboardItems)
        searchViewModel.displayUsers.observe(viewLifecycleOwner, ::displayUsers)

        searchViewModel.uiState.observe(viewLifecycleOwner) { uiState ->
            // hides filter sheet when data in loading state
            if (uiState == State.LOADING) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
    }

    private fun initBottomSheetView(view: View) {
        bottomSheetBehavior =
            BottomSheetBehavior.from(view.findViewById<View>(R.id.filterFragmentSheet))
        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                searchViewModel.updateSheetHeaderAlpha(
                    slideOffsetToAlpha(
                        slideOffset,
                        ALPHA_CHANGEOVER,
                        ALPHA_HEADER_MAX
                    )
                )

                searchViewModel.updateSheetDescriptionAlpha(
                    slideOffsetToAlpha(
                        slideOffset,
                        ALPHA_CHANGEOVER,
                        ALPHA_DESC_MAX
                    )
                )
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
            }

        })
    }

    private fun displayUsers(carriers: List<User>) {
        if (recyclerView.adapter == null) {
            recyclerView.adapter =
                UserAdapter(searchViewModel)
        }

        if (carriers.isNullOrEmpty()) {
            recyclerView.isVisible = false
        } else {
            recyclerView.isVisible = true
            (recyclerView.adapter as UserAdapter).submitList(carriers)
        }
    }

    private fun displayDashboardItems(items: List<BaseDashboardItem>) {
        val recyclerView = grid_recyclerView
        if (recyclerView.adapter == null) {
            val gridManager = GridLayoutManager(context,2)
            with(recyclerView){
                layoutManager = gridManager
                addItemDecoration(GridSpacingItemDecoration(2,16,true))
                adapter = DashboardAdapter(searchViewModel)
            }
        }

        (recyclerView.adapter as DashboardAdapter).submitList(items)
    }

    private fun handlePhoneCall(phoneNumber: String) {
        val result = ActivityCompat.checkSelfPermission(requireContext(), CALL_PHONE)
        if (result == PackageManager.PERMISSION_GRANTED) {
            context?.phoneCall(phoneNumber)
        } else {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(CALL_PHONE), 0)
        }
    }

    override fun setUserStatusAction(userStatus: UserStatus) {
        searchViewModel.updateUserStatus(userStatus)
    }

    private fun openCarrierDetailsDestination(user: User) {
        SearchCarriersFragmentDirections
            .actionDestinationSearchCarrierToDestinationCarrierDetails(user)
            .run(navController::navigate)

    }

    private fun openUpdateUserStatusDestination(userStatus: UserStatus) {
        SearchCarriersFragmentDirections
            .actionGlobalDestinationUpdateUserStatusDialog(userStatus, this)
            .run(navController::navigate)
    }

    private fun openCreateNewCarrierDestination(phoneNumber: String) {
        SearchCarriersFragmentDirections
            .actionDestinationSearchCarrierToDestinationCreateOrUpdateUser(phoneNumber = phoneNumber)
            .run(navController::navigate)
    }

    private fun openDashboardOverview(type: RequireCarrierType) {
        SearchCarriersFragmentDirections
            .actionDestinationSearchCarrierToDestinationDashboardOverview(type)
            .run(navController::navigate)
    }

}