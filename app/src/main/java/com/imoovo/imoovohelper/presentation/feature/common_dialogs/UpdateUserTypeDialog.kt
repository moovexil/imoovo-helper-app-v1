package com.imoovo.imoovohelper.presentation.feature.common_dialogs

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.databinding.DataBindingUtil
import androidx.databinding.InverseMethod
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.navArgs
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.databinding.LayoutDialogUpdateUserTypeBinding
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.inflater
import java.io.Serializable

interface UpdateUserTypeActionHandler : Serializable {
    fun userTypeAction(userType: UserType)
}

class UpdateUserTypeDialog : AppCompatDialogFragment() {

    private lateinit var updateListener: UpdateUserTypeActionHandler
    private lateinit var binding: LayoutDialogUpdateUserTypeBinding
    private val args: UpdateUserTypeDialogArgs by navArgs()
    val userType = MutableLiveData(UserType.AGENT)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(args) {
            updateListener = callback
            this@UpdateUserTypeDialog.userType.value = userType
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DataBindingUtil.inflate(
            requireContext().inflater(),
            R.layout.layout_dialog_update_user_type, null, false
        )

        binding.apply {
            userType = this@UpdateUserTypeDialog.userType
        }

        return AlertDialog.Builder(requireContext())
            .setView(binding.root)
            .setCancelable(false)
            .setPositiveButton(getString(R.string.btn_update)) { _, _ -> updateUserStatus() }
            .setNegativeButton(getString(R.string.btn_cancel)) { _, _ -> dismiss() }
            .create()
    }

    private fun updateUserStatus() {
        updateListener.userTypeAction(userType.value!!)
        dismiss()
    }
}

object UserTypeDialogConverter {

    private enum class RadioButton(val id: Int) {
        ADMIN(R.id.b_admin),
        EX_ADMIN(R.id.b_exAdmin),
        BROKER(R.id.b_broker),
        AGENT(R.id.b_agent)
    }

    @InverseMethod("fromPosition")
    @JvmStatic
    fun toPosition(value: UserType): Int = when (value) {
        UserType.ADMIN -> RadioButton.ADMIN.id
        UserType.EX_ADMIN -> RadioButton.EX_ADMIN.id
        UserType.BROKER -> RadioButton.BROKER.id
        else -> RadioButton.AGENT.id
    }

    @JvmStatic
    fun fromPosition(position: Int): UserType = when (position) {
        RadioButton.ADMIN.id -> UserType.ADMIN
        RadioButton.EX_ADMIN.id -> UserType.EX_ADMIN
        RadioButton.BROKER.id -> UserType.BROKER
        else -> UserType.AGENT
    }
}