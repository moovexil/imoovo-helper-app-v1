@file:JvmName("ViewBindingAdapters")

package com.imoovo.imoovohelper.presentation.common_presentation.data_binding

import android.view.View
import androidx.databinding.BindingAdapter
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.State
import com.imoovo.imoovohelper.domain.model.user.UserType

/**
 * One way data binding adapters to control the [View] visibility as per the [State] of the UI.
 */
object VisibilityAsPerUiStateBindingAdapters {

    @BindingAdapter("loadingInvisible")
    @JvmStatic
    fun setInvisibleForLoading(view: View, state: State) =
        if (state == State.LOADING) view.visibility = View.INVISIBLE else view.visibility =
            View.VISIBLE

    @BindingAdapter("loadingVisible")
    @JvmStatic
    fun setVisibleForLoading(view: View, state: State) =
        if (state == State.LOADING) view.visibility = View.VISIBLE else view.visibility = View.GONE

    @BindingAdapter("noDataVisible")
    @JvmStatic
    fun setVisibleForNoData(view: View, state: State) =
        if (state == State.NO_DATA) view.visibility = View.VISIBLE else view.visibility = View.GONE

    @BindingAdapter("hasDataVisible")
    @JvmStatic
    fun setHasDataVisible(view: View, state: State) =
        if (state == State.SUCCESS) view.visibility = View.VISIBLE else view.visibility =
            View.INVISIBLE

    @BindingAdapter("defaultVisible")
    @JvmStatic
    fun setDefaultVisible(view: View, state: State) =
        if (state == State.DEFAULT) view.visibility = View.VISIBLE else view.visibility = View.GONE

}

/**
 * One way data binding adapters to control the [View] visibility as per the [SessionType].
 */
object VisibilityAsPerSessionType {

    @BindingAdapter("visibleForAdminsSession")
    @JvmStatic
    fun setVisibleForAdmins(view: View, session: SessionType) {
        if (session == SessionType.ADMIN || session == SessionType.EX_ADMIN) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }

    @BindingAdapter("visibleForTopAdminsSession")
    @JvmStatic
    fun setVisibleForTopAdmins(view: View, session: SessionType) {
        if (session == SessionType.ADMIN) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }

    @BindingAdapter(value = ["visibleForAdminsSession","uiState"], requireAll = true)
    @JvmStatic
    fun setVisibleForAdminsInDefaultUiState(view: View, session: SessionType, uiState: State) {
        if ((session == SessionType.ADMIN || session == SessionType.EX_ADMIN) && uiState == State.DEFAULT) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }
}


/**
 * One way data binding adapters to control the [View] visibility as per the [UserType].
 */
object VisibilityAsPerUserType {

    @BindingAdapter("visibleForCarrierType")
    @JvmStatic
    fun setVisibleForCarrierTypes(view: View,userType: UserType?) {
        if (userType != null && userType == UserType.CARRIER) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }

    @BindingAdapter("visibleForTopAdminType")
    @JvmStatic
    fun setVisibleForTopAdminType(view: View,userType: UserType?) {
        if (userType != null && userType == UserType.ADMIN) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }

    @BindingAdapter("visibleForNotNullTypes")
    @JvmStatic
    fun setVisibleForNotNullTypes(view: View, userType: UserType?) {
        if (userType != null) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }

    @BindingAdapter("visibleForNullType")
    @JvmStatic
    fun setVisibleForAllType(view: View,userType: UserType?) {
        if (userType == null) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }
}

/**
 * One way data binding adapters to control the [View] click actions as per the [SessionType] .
 */
object ViewActionAsPerSessionType {

    @BindingAdapter("clickableForAdmins")
    @JvmStatic
    fun setClickableForAdmins(view: View, session: SessionType) {
        view.isClickable = session == SessionType.ADMIN || session == SessionType.EX_ADMIN
    }

}


@BindingAdapter("goneUnless")
fun goneUnless(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter("clearFocus")
fun clearFocusFromView(view: View, clearFocus: Boolean) {
    if (clearFocus) view.clearFocus()
}

