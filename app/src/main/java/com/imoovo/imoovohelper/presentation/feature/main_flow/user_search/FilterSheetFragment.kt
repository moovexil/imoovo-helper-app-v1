package com.imoovo.imoovohelper.presentation.feature.main_flow.user_search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.imoovo.imoovohelper.databinding.LayoutFragmentFilterSheetBinding
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.layout_fragment_filter_sheet.*
import javax.inject.Inject

class FilterSheetFragment : DaggerFragment() {

    @Inject lateinit var viewModelProvider: ViewModelProvider.Factory
    private val viewModel: SearchCarrierViewModel by viewModels({requireParentFragment()},{viewModelProvider})
    private lateinit var binding: LayoutFragmentFilterSheetBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutFragmentFilterSheetBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = this@FilterSheetFragment.viewModel
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        ccp.registerCarrierNumberEditText(field_carrierNumber)
    }

}