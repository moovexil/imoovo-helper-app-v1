package com.imoovo.imoovohelper.presentation.feature.common_dialogs

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.databinding.DataBindingUtil
import androidx.databinding.InverseMethod
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.navArgs
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.databinding.LayoutDialogUpdateUserStatusBinding
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.inflater
import java.io.Serializable

interface UpdateUserStatusActionHandler : Serializable{
    fun setUserStatusAction(userStatus: UserStatus)
}

class UpdateUserStatusDialog : AppCompatDialogFragment() {

    private lateinit var updateListener: UpdateUserStatusActionHandler
    private lateinit var binding: LayoutDialogUpdateUserStatusBinding
    private val args: UpdateUserStatusDialogArgs by navArgs()
    val status = MutableLiveData<UserStatus>()

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        updateListener = childFragment as UpdateUserStatusActionHandler
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DataBindingUtil.inflate(
            requireContext().inflater(),
            R.layout.layout_dialog_update_user_status, null, false
        )

        return AlertDialog.Builder(requireContext())
            .setView(binding.root)
            .setCancelable(false)
            .setPositiveButton(getString(R.string.btn_update)) { _, _ -> updateUserStatus() }
            .setNegativeButton(getString(R.string.btn_cancel)) { _, _ -> dismiss() }
            .create()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        with(args){
            updateListener = callback
            status.value = userStatus
        }

        binding.apply {
            userStatus = this@UpdateUserStatusDialog.status
        }
    }

    private fun updateUserStatus() {
        status.value?.let { userStatus ->
            updateListener.setUserStatusAction(userStatus)
        }

        dismiss()
    }
}


object UserStatusDialogConverter {

    private enum class RadioButton(val id: Int) {
        PENDING(R.id.b_pending),
        APPROVED(R.id.b_approved),
        INCORRECT(R.id.b_incorrect),
        NO_ANSWER(R.id.b_noAnswer)
    }

    @InverseMethod("fromPosition")
    @JvmStatic
    fun toPosition(value: UserStatus): Int = when (value) {
        UserStatus.APPROVED -> RadioButton.APPROVED.id
        UserStatus.INCORRECT -> RadioButton.INCORRECT.id
        UserStatus.NO_ANSWER -> RadioButton.NO_ANSWER.id
        UserStatus.PENDING -> RadioButton.PENDING.id
    }

    @JvmStatic
    fun fromPosition(position: Int): UserStatus = when (position) {
        RadioButton.APPROVED.id -> UserStatus.APPROVED
        RadioButton.INCORRECT.id -> UserStatus.INCORRECT
        RadioButton.NO_ANSWER.id -> UserStatus.NO_ANSWER
        else -> UserStatus.PENDING
    }
}