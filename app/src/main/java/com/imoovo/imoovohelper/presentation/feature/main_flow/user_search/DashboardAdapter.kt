package com.imoovo.imoovohelper.presentation.feature.main_flow.user_search

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.imoovo.imoovohelper.databinding.LayoutItemDashboardCarrierBinding
import com.imoovo.imoovohelper.databinding.LayoutItemDashboardPaymentBinding
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.inflater
import com.imoovo.imoovohelper.presentation.model.dashboard.BaseDashboardItem
import com.imoovo.imoovohelper.presentation.model.dashboard.DashboardCarrier
import com.imoovo.imoovohelper.presentation.model.dashboard.DashboardPayment
import com.imoovo.imoovohelper.presentation.model.dashboard.DashboardViewType

class DashboardAdapter (
    private val actionHandler: DashboardActionHandler
) : ListAdapter<BaseDashboardItem, BaseViewHolder>(DashboardDiff){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder = when (viewType){
        DashboardViewType.CARRIER.code -> DashboardCarrierViewHolder(
            LayoutItemDashboardCarrierBinding.inflate(parent.inflater(),parent,false),
            actionHandler
        )
        else -> DashboardPaymentViewHolder(
            LayoutItemDashboardPaymentBinding.inflate(parent.inflater(),parent,false),
            actionHandler
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).viewType.code
    }
}

abstract class BaseViewHolder (
    binding: ViewDataBinding
): RecyclerView.ViewHolder(binding.root){
    abstract fun bind (data: BaseDashboardItem)
}

class DashboardCarrierViewHolder(
    private val binding: LayoutItemDashboardCarrierBinding,
    private val actionHandler: DashboardActionHandler
) : BaseViewHolder(binding) {

    override fun bind(data: BaseDashboardItem) {
        binding.apply {
            value = data as DashboardCarrier
            actionHandler = this@DashboardCarrierViewHolder.actionHandler
            executePendingBindings()
        }
    }
}

class DashboardPaymentViewHolder(
    private val binding: LayoutItemDashboardPaymentBinding,
    private val actionHandler: DashboardActionHandler
) : BaseViewHolder(binding) {
    override fun bind(data: BaseDashboardItem) {
        binding.apply {
            value = data as DashboardPayment
            actionHandler = this@DashboardPaymentViewHolder.actionHandler
            executePendingBindings()
        }
    }
}


object DashboardDiff : DiffUtil.ItemCallback<BaseDashboardItem>() {
    override fun areItemsTheSame(oldItem: BaseDashboardItem, newItem: BaseDashboardItem): Boolean =
        //check if we compare right children objects
        when {
            oldItem is DashboardPayment && newItem is DashboardPayment -> {
                oldItem == newItem
            }
            oldItem is DashboardCarrier && newItem is DashboardCarrier -> {
                oldItem == newItem
            }
            else -> false
        }


    override fun areContentsTheSame(oldItem: BaseDashboardItem, newItem: BaseDashboardItem): Boolean  =
        when {
            oldItem is DashboardPayment && newItem is DashboardPayment -> {
                oldItem == newItem
            }
            oldItem is DashboardCarrier && newItem is DashboardCarrier -> {
                oldItem == newItem
            }
            else -> false
        }
}