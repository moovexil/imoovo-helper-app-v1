@file:JvmName("TextViewBindingAdapters")

package com.imoovo.imoovohelper.presentation.common_presentation.data_binding

import android.widget.EditText
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import com.imoovo.imoovohelper.common.extensions.nullIfBlankOrEmpty
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.truck.TruckType
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType
import com.imoovo.imoovohelper.presentation.feature.TRUCK_TYPES

/**
 * Binding adapters for support two way data binding with NULLABLE [Double] values for
 * widgets which children of the [EditText].
 */
object NullableDoubleValueBindingAdapters {

    @BindingAdapter("android:text")
    @JvmStatic
    fun setDouble(view: TextView, newValue: Double?) {
        val oldValue = view.text?.toString()?.toDoubleOrNull()

        if (newValue != null && oldValue != newValue) {
            /* If the newValue is -1 it means that we want co clear view in this
            case set empty string
             */
            if (newValue == -1.0) {
                view.text = ""
            } else {
                view.text = newValue.toString()
            }
        }
    }

    @InverseBindingAdapter(attribute = "android:text")
    @JvmStatic
    fun getDouble(view: TextView): Double? =
        view.text?.toString()?.toDoubleOrNull()

}

/**
 * Binding adapters for support two way data binding with NULLABLE [Integer] values for
 * widgets which children of the [EditText].
 */
object NullableIntValueBindingAdapters {

    @BindingAdapter("android:text")
    @JvmStatic
    fun setDouble(view: TextView, newValue: Int?) {
        val oldValue = view.text?.toString()?.toIntOrNull()

        if (newValue != null && oldValue != newValue) {
            /* If the newValue is -1 it means that we want co clear view in this
            case set empty string
             */
            if (newValue == -1) {
                view.text = ""
            } else {
                view.text = newValue.toString()
            }
        }
    }

    @InverseBindingAdapter(attribute = "android:text")
    @JvmStatic
    fun getDouble(view: TextView): Int? =
        view.text?.toString()?.toIntOrNull()

}

/**
 * Binding adapters for support two way data binding with nullable [String] values for
 * widgets which children of the [EditText].
 */
object NullableStringValueBindingAdapters {

    @BindingAdapter("android:text")
    @JvmStatic
    fun setText(view: TextView, newValue: String?) {
        val oldValue = view.text?.toString()
        if (newValue != null && oldValue != newValue) {
            view.text = newValue
        }
    }

    @InverseBindingAdapter(attribute = "android:text")
    @JvmStatic
    fun getText(view: TextView): String? {
        return view.text?.toString()?.nullIfBlankOrEmpty()
    }
}

/**
 * Binding adapters for support two way data binding with nullable [TruckType] values for
 * widgets which children of the [EditText].
 */
object NullableTruckTypeValueBindingAdapters {

    @BindingAdapter("android:text")
    @JvmStatic
    fun setText(view: TextView, newValue: TruckType?) {
        val oldTextValue: String? = view.text?.toString()
        val newTextValue: String? = when (newValue) {
            //use this UNKNOWN type to reset data in field applying empty string
            TruckType.UNKNOWN -> ""
            TruckType.FLATBED -> TRUCK_TYPES[0]
            TruckType.ENCLOSED -> TRUCK_TYPES[1]
            TruckType.REFRIGERATED -> TRUCK_TYPES[2]
            TruckType.LOWBOY -> TRUCK_TYPES[3]
            TruckType.STEP_DECK -> TRUCK_TYPES[4]
            TruckType.EXTENDABLE_FLATBED -> TRUCK_TYPES[5]
            TruckType.STRETCH_SINGLE_DROP_DECK -> TRUCK_TYPES[6]
            TruckType.REMOVABLE_GOOSENECK -> TRUCK_TYPES[7]
            TruckType.SPECIALITY -> TRUCK_TYPES[8]
            TruckType.SIDE_KIT -> TRUCK_TYPES[9]
            TruckType.EXPANDABLE_DOUBLE_DROP -> TRUCK_TYPES[10]
            TruckType.STRETCH_RGN -> TRUCK_TYPES[11]
            TruckType.CONESTOGA -> TRUCK_TYPES[12]
            TruckType.POWER_ONLY -> TRUCK_TYPES[13]
            TruckType.MULTI_CAR -> TRUCK_TYPES[14]
            else -> null
        }

        if (newTextValue != null && !oldTextValue.equals(newTextValue, ignoreCase = true)
        ) {
            view.text = newTextValue
        }
    }

    @InverseBindingAdapter(attribute = "android:text")
    @JvmStatic
    fun getText(view: TextView): TruckType? {
        return when (view.text?.toString()) {
            TRUCK_TYPES[0] -> TruckType.FLATBED
            TRUCK_TYPES[1] -> TruckType.ENCLOSED
            TRUCK_TYPES[2] -> TruckType.REFRIGERATED
            TRUCK_TYPES[3] -> TruckType.LOWBOY
            TRUCK_TYPES[4] -> TruckType.STEP_DECK
            TRUCK_TYPES[5] -> TruckType.EXTENDABLE_FLATBED
            TRUCK_TYPES[6] -> TruckType.STRETCH_SINGLE_DROP_DECK
            TRUCK_TYPES[7] -> TruckType.REMOVABLE_GOOSENECK
            TRUCK_TYPES[8] -> TruckType.SPECIALITY
            TRUCK_TYPES[9] -> TruckType.SIDE_KIT
            TRUCK_TYPES[10] -> TruckType.EXPANDABLE_DOUBLE_DROP
            TRUCK_TYPES[11] -> TruckType.STRETCH_RGN
            TRUCK_TYPES[12] -> TruckType.CONESTOGA
            TRUCK_TYPES[13] -> TruckType.POWER_ONLY
            TRUCK_TYPES[14] -> TruckType.MULTI_CAR
            else -> null
        }
    }
}

@BindingAdapter("clearViewText")
fun clearViewText(view: TextView, isClear: Boolean) {
    if (isClear) {
        view.text = ""
    }
}

@BindingAdapter("android:text")
fun setText(view: TextView, userStatus: UserStatus?) {
    if (userStatus != null) {
        view.text = userStatus.displayName
    }
}

@BindingAdapter("android:text")
fun setText(view: TextView, userType: UserType?) {
    if (userType != null) {
        view.text = userType.toString()
    }
}

@BindingAdapter("android:text")
fun setText(view: TextView, sessionType: SessionType?) {
    if (sessionType != null) {
        view.text = sessionType.toString()
    }
}

@BindingAdapter("setResourceText")
fun setResourceText(view: TextView, resId: Int) {
    view.text = view.context.getString(resId)
}