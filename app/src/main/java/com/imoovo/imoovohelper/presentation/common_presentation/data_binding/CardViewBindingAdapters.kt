@file:JvmName("CardViewBindingAdapters")
package com.imoovo.imoovohelper.presentation.common_presentation.data_binding

import androidx.databinding.BindingAdapter
import com.google.android.material.card.MaterialCardView
import com.imoovo.imoovohelper.R
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.presentation.common_presentation.extensions.getColorCompat

@BindingAdapter("app:cardBackgroundColor")
fun setCardBackgroundColor(
    view: MaterialCardView,
    status: UserStatus
) {
    val context = view.context
    when (status) {
        UserStatus.APPROVED -> view.setCardBackgroundColor(context.getColorCompat(R.color.android_green))
        UserStatus.NO_ANSWER -> view.setCardBackgroundColor(context.getColorCompat(R.color.android_holo_orange))
        UserStatus.PENDING -> view.setCardBackgroundColor(context.getColorCompat(R.color.color_primary))
        UserStatus.INCORRECT -> view.setCardBackgroundColor(context.getColorCompat(R.color.android_red))
    }
}

@BindingAdapter("cardBackgroundColor")
fun setCardBackgroundColor(
    view: MaterialCardView,
    colorRes: Int
) {
    view.setCardBackgroundColor(view.context.getColorCompat(colorRes))
}
