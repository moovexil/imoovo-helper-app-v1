package com.imoovo.imoovohelper.domain.interactor.user

import com.imoovo.imoovohelper.data.repository.AppUserRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.model.user.User
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class GetAppUserDataUseCase @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val appUserRepository: AppUserRepository
) : SuspendUseCase<Unit, User?>(ioDispatcher) {

    override suspend fun execute(parameters: Unit): User? =
        appUserRepository.getAppUserData()
}