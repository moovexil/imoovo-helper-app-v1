package com.imoovo.imoovohelper.domain.model.user

import java.util.*

data class ChangeUserRoleRequest<T: User> (
    val user : T,
    val oldUserId: UUID,
    val oldUserType: UserType
)