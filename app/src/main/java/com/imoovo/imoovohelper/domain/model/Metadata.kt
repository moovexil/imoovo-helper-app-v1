package com.imoovo.imoovohelper.domain.model

import java.io.Serializable
import java.util.*

data class Metadata(
    val creationDate: Date,
    val lastModifiedDate: Date,
    val version: Long
):Serializable