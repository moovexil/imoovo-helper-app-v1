package com.imoovo.imoovohelper.domain.interactor.authentication

import com.imoovo.imoovohelper.data.repository.AuthRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetUserAuthenticationStateUseCase @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val authRepository: AuthRepository
) : SuspendUseCase<Unit, Boolean>(ioDispatcher) {

    override suspend fun execute(parameters: Unit): Boolean =
        authRepository.isUserSignedIn()
}