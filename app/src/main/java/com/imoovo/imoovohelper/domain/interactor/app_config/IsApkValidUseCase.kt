package com.imoovo.imoovohelper.domain.interactor.app_config

import com.imoovo.imoovohelper.BuildConfig
import com.imoovo.imoovohelper.data.repository.AppConfigRepository
import com.imoovo.imoovohelper.di.qualifier.DefaultDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class IsApkValidUseCase @Inject constructor(
    private val configRepository: AppConfigRepository,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) : SuspendUseCase<Unit, Boolean>(defaultDispatcher) {

    /**
     * Decides if current APK update required based on requirements from the remote config
     * and current [BuildConfig.VERSION_NAME].
     */
    override suspend fun execute(parameters: Unit): Boolean =
        configRepository.availableApkVersion().run {
            BuildConfig.VERSION_NAME != this && "unknown" != this
        }
}