package com.imoovo.imoovohelper.domain.model.filters

data class CarrierFilter(

    val userFilter: UserFilter? = null,
    val truckFilter: TruckFilter? = null,
    var hasGitDoc: Boolean? = null,
    var cbtAvailable: Boolean? = null
)

