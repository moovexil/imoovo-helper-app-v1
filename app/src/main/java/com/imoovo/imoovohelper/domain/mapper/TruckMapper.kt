package com.imoovo.imoovohelper.domain.mapper

import com.imoovo.imoovohelper.data.entity.TruckEntity
import com.imoovo.imoovohelper.domain.mapper.base.BaseListMapper
import com.imoovo.imoovohelper.domain.mapper.base.BaseMapper
import com.imoovo.imoovohelper.domain.model.truck.Truck
import javax.inject.Inject

class TruckMapper @Inject constructor() : BaseMapper<TruckEntity, Truck>,
    BaseListMapper<TruckEntity, Truck> {

    override fun toEntity(domain: Truck): TruckEntity =
        TruckEntity(
            id = domain.id,
            type = domain.type,
            tonnage = domain.tonnage,
            trucksNumber = domain.trucksNumber
        )

    override fun toDomain(entity: TruckEntity): Truck =
        Truck(
            id = entity.id,
            type = entity.type,
            tonnage = entity.tonnage,
            trucksNumber = entity.trucksNumber
        )

    override fun toDomainList(entities: Iterable<TruckEntity>): Collection<Truck> =
        entities.map(::toDomain)

    override fun toEntityList(domains: Iterable<Truck>): Collection<TruckEntity> =
        domains.map(::toEntity)
}