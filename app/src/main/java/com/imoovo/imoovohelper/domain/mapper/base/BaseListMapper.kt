package com.imoovo.imoovohelper.domain.mapper.base

/**
 * Maps the domain model type into the entity model type and wise-versa.
 *
 * @param <E> the entity representation of a model
 * @param <D> the domain representation of a model
 */
interface BaseListMapper<E,D> {
    fun toDomainList(entities: Iterable<E>): Collection<D>
    fun toEntityList(domains: Iterable<D>): Collection<E>
}