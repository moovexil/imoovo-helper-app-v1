package com.imoovo.imoovohelper.domain.model.dashboard

data class DashboardStatistics (
    val approvedCarriers: Int,
    val notAnsweredCarriers: Int,
    val pendingCarriers: Int,
    val myApprovedCarriers: Int,
    val myNotAnsweredCarriers: Int,
    val myPendingCarriers: Int
)