package com.imoovo.imoovohelper.domain.interactor.user

import com.imoovo.imoovohelper.data.repository.AppUserRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.model.user.User
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class CacheAppUserDataUseCase @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val appUserRepository: AppUserRepository
) : SuspendUseCase<User, Unit>(ioDispatcher) {

    override suspend fun execute(parameters: User) =
        appUserRepository.saveAppUserData(parameters)

}