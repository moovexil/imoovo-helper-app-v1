package com.imoovo.imoovohelper.domain.interactor.broker

import com.imoovo.imoovohelper.data.repository.BrokerRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.mapper.UserMapper
import com.imoovo.imoovohelper.domain.model.user.ChangeUserRoleRequest
import com.imoovo.imoovohelper.domain.model.user.User
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChangeUserRoleToBrokerUseCase @Inject constructor(
    private val brokerRepository: BrokerRepository,
    private val userMapper: UserMapper,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : SuspendUseCase<ChangeUserRoleRequest<User>, Unit>(ioDispatcher) {

    override suspend fun execute(parameters: ChangeUserRoleRequest<User>) {
        val user = userMapper.toEntity(parameters.user)
        brokerRepository.changeUserRoleToBroker(user,parameters.oldUserId,parameters.oldUserType)
    }

}