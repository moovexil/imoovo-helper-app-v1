package com.imoovo.imoovohelper.domain.model.carrier

import com.imoovo.imoovohelper.domain.model.Metadata
import com.imoovo.imoovohelper.domain.model.SessionType
import com.imoovo.imoovohelper.domain.model.truck.Truck
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType
import java.io.Serializable
import java.util.*

class Carrier(
    id: UUID?,
    adminComment: String?,
    contactName: String,
    phoneNumber: String,
    userType: UserType,
    userStatus: UserStatus,
    companyName: String?,
    sessionType: SessionType? = null,
    email: String?,
    country: String?,
    region: String?,
    city: String?,
    metaData: Metadata?,
    val trucks: Set<Truck>?,
    val cbtAvailable: Boolean?,
    val gitAvailable: Boolean?

) : User(
    id,
    adminComment,
    contactName,
    phoneNumber,
    userType,
    userStatus,
    companyName,
    sessionType,
    email,
    country,
    region,
    city,
    metaData
), Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Carrier) return false
        if (!super.equals(other)) return false

        if (trucks != other.trucks) return false
        if (cbtAvailable != other.cbtAvailable) return false
        if (gitAvailable != other.gitAvailable) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + trucks.hashCode()
        result = 31 * result + cbtAvailable.hashCode()
        result = 31 * result + gitAvailable.hashCode()
        return result
    }
}
