package com.imoovo.imoovohelper.domain.interactor.authentication

import com.imoovo.imoovohelper.data.repository.AuthRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.FlowUseCase
import com.imoovo.imoovohelper.domain.model.PhoneAuthState
import com.imoovo.imoovohelper.domain.model.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ObserveAuthenticationFlowUseCase @Inject constructor(
    private val authRepository: AuthRepository,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : FlowUseCase<Unit,PhoneAuthState>(ioDispatcher){

    override fun execute(parameters: Unit): Flow<Result<PhoneAuthState>> =
        authRepository.observeAuthenticationFlow()

}