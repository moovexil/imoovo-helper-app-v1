package com.imoovo.imoovohelper.domain.model.user

enum class UserStatus(val displayName : String) {
    PENDING("Pending"),
    APPROVED("Approved"),
    INCORRECT("Incorrect data"),
    NO_ANSWER("Not answer")
}