package com.imoovo.imoovohelper.domain.interactor.carrier

import com.imoovo.imoovohelper.data.repository.CarrierRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.mapper.CarrierMapper
import com.imoovo.imoovohelper.domain.model.carrier.Carrier
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UpdateCarrierUserUseCase @Inject constructor(
    private val repository: CarrierRepository,
    private val carrierMapper: CarrierMapper,
    @IoDispatcher private val dispatcherIo: CoroutineDispatcher
) : SuspendUseCase<Carrier, Unit>(dispatcherIo) {

    override suspend fun execute(parameters: Carrier) =
        carrierMapper.toEntity(parameters).run {
            repository.updateCarrierUser(this)
        }
}