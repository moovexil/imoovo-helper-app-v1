package com.imoovo.imoovohelper.domain.model.filters

import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.user.UserType

data class UserFilter (
    var userType: UserType? = null,
    var userStatus: UserStatus? = null,
    var phoneNumber: String? = null,
    var city: String? = null,
    var region: String? = null,
    var country: String? = null
    )