package com.imoovo.imoovohelper.domain.interactor.user

import com.imoovo.imoovohelper.data.repository.AppUserRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.mapper.UserMapper
import com.imoovo.imoovohelper.domain.model.user.User
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class CreateNewUserUseCase @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val userRepository: AppUserRepository,
    private val userMapper: UserMapper
): SuspendUseCase<User, User>(ioDispatcher) {

    override suspend fun execute(parameters: User): User =
        userMapper.toEntity(parameters).run {
            userRepository.createServerUser(this).run {
                userMapper.toDomain(this)
            }
        }

}