package com.imoovo.imoovohelper.domain.model

enum class SessionType{
    ADMIN,
    EX_ADMIN,
    BROKER,
    AGENT
}