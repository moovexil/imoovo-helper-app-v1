package com.imoovo.imoovohelper.domain.interactor

import com.imoovo.imoovohelper.data.repository.DashboardRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.model.dashboard.DashboardStatistics
import com.imoovo.imoovohelper.domain.model.user.UserType
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetDashboardStatisticsUseCase @Inject constructor(
    private val dashboardRepository: DashboardRepository,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
): SuspendUseCase<UserType,DashboardStatistics>(ioDispatcher) {

    override suspend fun execute(parameters: UserType): DashboardStatistics =
        dashboardRepository.getDashboardStatistics(parameters)
}