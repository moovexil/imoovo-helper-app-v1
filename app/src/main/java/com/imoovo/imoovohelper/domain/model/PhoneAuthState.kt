package com.imoovo.imoovohelper.domain.model

import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException

enum class PhoneAuthState {
    ON_CODE_SENT,
    AUTH_FAILED,
    BAD_CREDENTIALS,
    SMS_QUOTA_EXCEEDED,
    SUCCESS
}

fun Exception.getPhoneAuthErrorState(): PhoneAuthState = when (this) {
    is FirebaseAuthInvalidCredentialsException -> PhoneAuthState.BAD_CREDENTIALS
    is FirebaseTooManyRequestsException -> PhoneAuthState.SMS_QUOTA_EXCEEDED
    else -> PhoneAuthState.AUTH_FAILED
}