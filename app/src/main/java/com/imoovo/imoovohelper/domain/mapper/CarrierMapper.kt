package com.imoovo.imoovohelper.domain.mapper

import com.imoovo.imoovohelper.data.entity.CarrierEntity
import com.imoovo.imoovohelper.domain.mapper.base.BaseListMapper
import com.imoovo.imoovohelper.domain.mapper.base.BaseMapper
import com.imoovo.imoovohelper.domain.model.carrier.Carrier
import javax.inject.Inject

class CarrierMapper @Inject constructor(
    private val truckMapper: TruckMapper
) : BaseMapper<CarrierEntity, Carrier>,
    BaseListMapper<CarrierEntity, Carrier> {

    override fun toEntity(domain: Carrier): CarrierEntity =
        CarrierEntity(
            id = domain.id,
            adminComment = domain.adminComment,
            userType = domain.userType,
            userStatus = domain.userStatus,
            contactName = domain.contactName,
            phoneNumber = domain.phoneNumber,
            companyName = domain.companyName,
            email = domain.email,
            country = domain.country,
            region = domain.region,
            city = domain.city,
            trucks = domain.trucks?.let { truckMapper.toEntityList(it).toSet()},
            cbtAvailable = domain.cbtAvailable,
            gitAvailable = domain.gitAvailable,
            metaData = domain.metaData
        )

    override fun toDomain(entity: CarrierEntity): Carrier =
        Carrier(
            id = entity.id,
            adminComment = entity.adminComment,
            metaData = entity.metaData,
            userType = entity.userType,
            userStatus = entity.userStatus,
            contactName = entity.contactName,
            phoneNumber = entity.phoneNumber,
            companyName = entity.companyName,
            email = entity.email,
            country = entity.country,
            region = entity.region,
            city = entity.city,
            trucks = entity.trucks?.let { truckMapper.toDomainList(it).toSet()},
            cbtAvailable = entity.cbtAvailable,
            gitAvailable = entity.gitAvailable
        )

    override fun toDomainList(entities: Iterable<CarrierEntity>): Collection<Carrier> =
        entities.map(::toDomain)

    override fun toEntityList(domains: Iterable<Carrier>): Collection<CarrierEntity> =
        domains.map(::toEntity)
}