package com.imoovo.imoovohelper.domain.interactor.user

import com.imoovo.imoovohelper.data.repository.AppUserRepository
import com.imoovo.imoovohelper.di.qualifier.DefaultDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.mapper.UserMapper
import com.imoovo.imoovohelper.domain.model.user.User
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetUserByPhoneNumber @Inject constructor(
    private val repository: AppUserRepository,
    private val userMapper: UserMapper,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) : SuspendUseCase<String, User>(defaultDispatcher) {

    override suspend fun execute(parameters: String): User =
        repository.getServerUserByPhoneNumber(parameters).run {
            userMapper.toDomain(this)
        }


}