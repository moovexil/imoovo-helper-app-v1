package com.imoovo.imoovohelper.domain.interactor.authentication

import com.imoovo.imoovohelper.data.repository.AuthRepository
import com.imoovo.imoovohelper.di.qualifier.DefaultDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SetPhoneAuthSmsVerificationCodeUseCase @Inject constructor(
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher,
    private val authRepository: AuthRepository
) : SuspendUseCase<String, Unit>(defaultDispatcher) {

    override suspend fun execute(parameters: String) =
        authRepository.setSmsVerificationCode(parameters)
}