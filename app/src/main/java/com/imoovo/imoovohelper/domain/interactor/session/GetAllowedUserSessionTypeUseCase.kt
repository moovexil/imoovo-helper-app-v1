package com.imoovo.imoovohelper.domain.interactor.session

import com.imoovo.imoovohelper.data.remote_config.DEVICE_ID_USER_ADMIN
import com.imoovo.imoovohelper.data.remote_config.DEVICE_ID_USER_BROKER
import com.imoovo.imoovohelper.data.remote_config.DEVICE_ID_USER_EX_ADMIN
import com.imoovo.imoovohelper.data.repository.AppConfigRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.model.SessionType
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetAllowedUserSessionTypeUseCase @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val configRepository: AppConfigRepository
) : SuspendUseCase<Unit, SessionType>(ioDispatcher) {

    override suspend fun execute(parameters: Unit): SessionType {
        val userDeviceId = configRepository.getDeviceId()
        val idsMap = configRepository.getUsersIdsGroups()
        var sessionType = SessionType.AGENT

        for ((key, value) in idsMap) {
            when (key) {
                DEVICE_ID_USER_ADMIN -> value.forEach { id ->
                    if (id == userDeviceId) {
                        sessionType = SessionType.ADMIN
                    }
                }

                DEVICE_ID_USER_EX_ADMIN -> value.forEach { id ->
                    if (id == userDeviceId) {
                        sessionType = SessionType.EX_ADMIN
                    }
                }

                DEVICE_ID_USER_BROKER -> value.forEach { id ->
                    if (id == userDeviceId) {
                        sessionType = SessionType.BROKER
                    }
                }

            }
        }

        return sessionType
    }
}