package com.imoovo.imoovohelper.domain.interactor.carrier

import com.imoovo.imoovohelper.data.repository.CarrierRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.mapper.CarrierMapper
import com.imoovo.imoovohelper.domain.model.carrier.Carrier
import com.imoovo.imoovohelper.domain.model.filters.CarrierFilter
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetCarriersByFilersUseCase @Inject constructor(
    private val repository: CarrierRepository,
    private val carrierMapper: CarrierMapper,
    @IoDispatcher private val dispatcherIo: CoroutineDispatcher
) : SuspendUseCase<CarrierFilter, List<Carrier>>(dispatcherIo) {

    override suspend fun execute(parameters: CarrierFilter): List<Carrier> =
        repository.getCarriersByFilters(parameters).run {
            carrierMapper.toDomainList(this).toList()
        }

}