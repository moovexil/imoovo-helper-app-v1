package com.imoovo.imoovohelper.domain.model

import androidx.lifecycle.MutableLiveData
import com.imoovo.imoovohelper.domain.model.Result.Success

/**
 * Represents the data state as per loaded data state
 */
enum class State {
    /**
     * Identifies that data loading process started.
     */
    LOADING,
    /**
     * Identifies that data loading process succeed and result is not empty.
     */
    SUCCESS,
    /**
     * Identifies that data loading process fas failure
     */
    ERROR,
    /**
     * Identifies that data loading process succeed but result data is empty.
     */
    NO_DATA,
    /**
     * Default state of the object -> use it to instantiate the object.
     */
    DEFAULT
}

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class Result<out R> {


    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()
    object Loading : Result<Nothing>()
    object Empty : Result<Nothing>()
    object Default : Result<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
            Loading -> "Loading"
            Empty -> "No data"
            Default -> "Default state"
        }
    }
}

//val <T> Result<T>.data: T?
//    get() = (this as? Success)?.data
//
//val <T> Result<T>.exception: Exception?
//    get() = (this as? Result.Error)?.exception

val <T> Result<T>.state: State
    get() = when (this) {
        is Result.Loading -> State.LOADING
        is Result.Error -> State.ERROR
        is Success -> State.SUCCESS
        is Result.Empty -> State.NO_DATA
        is Result.Default -> State.DEFAULT
    }

/**
 * Invokes the lambda block with income value if [Result] is [State.SUCCESS].
 */
inline fun <T> Result<T>.ifSuccess(block: (T) -> Unit) {
    if (this is Success) block.invoke(this.data)
}

inline fun <T> Result<T>.ifError(block: (Exception) -> Unit) {
    if (this is Result.Error) block.invoke(this.exception)
}

inline fun <T> Result<T>.ifEmpty(block: () -> Unit) {
    if (this is Result.Empty) block.invoke()
}

///**
// * [Success.data] if [Result] is of type [Success]
// */
//fun <T> Result<T>.successOr(fallback: T): T {
//    return (this as? Success<T>)?.data ?: fallback
//}

