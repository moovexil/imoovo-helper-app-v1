package com.imoovo.imoovohelper.domain.interactor.carrier

import com.imoovo.imoovohelper.data.repository.CarrierRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.mapper.CarrierMapper
import com.imoovo.imoovohelper.domain.model.user.UserStatus
import com.imoovo.imoovohelper.domain.model.carrier.Carrier
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetCarriersByUserStatus @Inject constructor(
    private val carrierRepository: CarrierRepository,
    private val carrierMapper: CarrierMapper,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : SuspendUseCase<UserStatus, Set<Carrier>>(ioDispatcher) {

    override suspend fun execute(parameters: UserStatus): Set<Carrier>  =
        carrierRepository.getCarriersByUserStatus(parameters).run {
            carrierMapper.toDomainList(this).toSet()
        }

}