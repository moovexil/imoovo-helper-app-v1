package com.imoovo.imoovohelper.domain.interactor.session

import com.imoovo.imoovohelper.data.repository.AppUserRepository
import com.imoovo.imoovohelper.data.repository.AuthRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.model.user.User
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChangeSessionTypeUseCase @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val authRepository: AuthRepository,
    private val appUserRepository: AppUserRepository
) : SuspendUseCase<User, Unit>(ioDispatcher) {

    override suspend fun execute(parameters: User) {
        authRepository.signOut()
        appUserRepository.saveAppUserData(parameters)
    }

}