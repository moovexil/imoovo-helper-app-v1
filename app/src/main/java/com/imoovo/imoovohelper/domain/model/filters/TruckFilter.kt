package com.imoovo.imoovohelper.domain.model.filters

import com.imoovo.imoovohelper.domain.model.truck.TruckType

data class TruckFilter(
    var type: TruckType? = null,
    var tonnage: Double? = null
)