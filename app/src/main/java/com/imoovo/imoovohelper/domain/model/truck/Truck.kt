package com.imoovo.imoovohelper.domain.model.truck

import java.io.Serializable
import java.util.*

data class Truck(
    var id: UUID?,
    var type: TruckType?,
    var tonnage: Double?,
    var trucksNumber: Int?
) : Serializable



