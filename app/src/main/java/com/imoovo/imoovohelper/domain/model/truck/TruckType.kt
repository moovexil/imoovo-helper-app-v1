package com.imoovo.imoovohelper.domain.model.truck

enum class TruckType {
    UNKNOWN,
    FLATBED,
    ENCLOSED,
    REFRIGERATED,
    LOWBOY,
    STEP_DECK,
    EXTENDABLE_FLATBED,
    STRETCH_SINGLE_DROP_DECK,
    REMOVABLE_GOOSENECK,
    SPECIALITY,
    SIDE_KIT,
    EXPANDABLE_DOUBLE_DROP,
    STRETCH_RGN,
    CONESTOGA,
    POWER_ONLY,
    MULTI_CAR
}
