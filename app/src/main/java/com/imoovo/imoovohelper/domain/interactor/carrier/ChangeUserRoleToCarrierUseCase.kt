package com.imoovo.imoovohelper.domain.interactor.carrier

import com.imoovo.imoovohelper.data.repository.CarrierRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.mapper.CarrierMapper
import com.imoovo.imoovohelper.domain.model.carrier.Carrier
import com.imoovo.imoovohelper.domain.model.user.ChangeUserRoleRequest
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChangeUserRoleToCarrierUseCase @Inject constructor(
    private val brokerRepository: CarrierRepository,
    private val userMapper: CarrierMapper,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : SuspendUseCase<ChangeUserRoleRequest<Carrier>, Unit>(ioDispatcher) {

    override suspend fun execute(parameters: ChangeUserRoleRequest<Carrier>) {
        val user = userMapper.toEntity(parameters.user)
        brokerRepository.changeUserRoleToCarrier(user, parameters.oldUserId, parameters.oldUserType)
    }

}