package com.imoovo.imoovohelper.domain.interactor.broker

import com.imoovo.imoovohelper.data.repository.BrokerRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.mapper.UserMapper
import com.imoovo.imoovohelper.domain.model.user.User
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UpdateBrokerUserUseCase @Inject constructor(
    private val brokerRepository: BrokerRepository,
    private val userMapper: UserMapper,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : SuspendUseCase<User, Unit>(ioDispatcher) {

    override suspend fun execute(parameters: User) =
        userMapper.toEntity(parameters).run {
            brokerRepository.updateBrokerUser(this)
        }
}