package com.imoovo.imoovohelper.domain.model.user

import com.imoovo.imoovohelper.domain.model.Metadata
import com.imoovo.imoovohelper.domain.model.SessionType
import java.io.Serializable
import java.util.*

open class User(
    val id: UUID? = null,
    val adminComment: String? = null,
    val contactName: String,
    val phoneNumber: String,
    val userType: UserType,
    val userStatus: UserStatus,
    val companyName: String? = null,
    val sessionType: SessionType? = null,
    val email: String? = null,
    val country: String?,
    val region: String?,
    val city: String?,
    val metaData: Metadata? = null
) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is User) return false

        if (id != other.id) return false
        if (contactName != other.contactName) return false
        if (phoneNumber != other.phoneNumber) return false
        if (companyName != other.companyName) return false
        if (userType != other.userType) return false
        if (userStatus != other.userStatus) return false
        if (sessionType != other.sessionType) return false
        if (email != other.email) return false
        if (country != other.country) return false
        if (region != other.region) return false
        if (city != other.city) return false
        if (metaData != other.metaData) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + contactName.hashCode()
        result = 31 * result + phoneNumber.hashCode()
        result = 31 * result + (companyName?.hashCode() ?: 0)
        result = 31 * result + userType.hashCode()
        result = 31 * result + userStatus.hashCode()
        result = 31 * result + (sessionType?.hashCode() ?: 0)
        result = 31 * result + (email?.hashCode() ?: 0)
        result = 31 * result + country.hashCode()
        result = 31 * result + region.hashCode()
        result = 31 * result + city.hashCode()
        result = 31 * result + (metaData?.hashCode() ?: 0)
        return result
    }
}