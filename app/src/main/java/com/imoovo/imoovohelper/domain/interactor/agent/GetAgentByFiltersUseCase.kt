package com.imoovo.imoovohelper.domain.interactor.agent

import com.imoovo.imoovohelper.data.repository.AgentRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.mapper.UserMapper
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.filters.UserFilter
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetAgentByFiltersUseCase @Inject constructor(
    private val agentRepository: AgentRepository,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val userMapper: UserMapper
) : SuspendUseCase<UserFilter, List<User>>(ioDispatcher) {

    override suspend fun execute(parameters: UserFilter): List<User> =
        agentRepository.getAgentsByFilters(parameters).run {
            userMapper.toDomainList(this).toList()
        }

}