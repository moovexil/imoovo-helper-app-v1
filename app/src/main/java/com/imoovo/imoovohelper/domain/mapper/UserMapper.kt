package com.imoovo.imoovohelper.domain.mapper

import com.imoovo.imoovohelper.data.entity.user.UserEntity
import com.imoovo.imoovohelper.domain.mapper.base.BaseListMapper
import com.imoovo.imoovohelper.domain.mapper.base.BaseMapper
import com.imoovo.imoovohelper.domain.model.user.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserMapper @Inject constructor(
) : BaseMapper<UserEntity,User>, BaseListMapper<UserEntity,User> {

    override fun toEntity(domain: User): UserEntity = UserEntity(
        id = domain.id,
        userStatus = domain.userStatus,
        userType = domain.userType,
        contactName = domain.contactName,
        companyName = domain.companyName,
        phoneNumber = domain.phoneNumber,
        email = domain.email,
        city = domain.city,
        region = domain.region,
        country = domain.country,
        metaData = domain.metaData
    )

    override fun toDomain(entity: UserEntity): User = User(
        id = entity.id,
        userStatus = entity.userStatus,
        userType = entity.userType,
        contactName = entity.contactName,
        phoneNumber = entity.phoneNumber,
        companyName = entity.companyName,
        email = entity.email,
        city = entity.city,
        region = entity.region,
        country = entity.country,
        metaData = entity.metaData
    )

    override fun toDomainList(entities: Iterable<UserEntity>): Collection<User> =
        entities.map(::toDomain)

    override fun toEntityList(domains: Iterable<User>): Collection<UserEntity> =
        domains.map(::toEntity)
}