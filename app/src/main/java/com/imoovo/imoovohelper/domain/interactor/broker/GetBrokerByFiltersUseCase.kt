package com.imoovo.imoovohelper.domain.interactor.broker

import com.imoovo.imoovohelper.data.repository.BrokerRepository
import com.imoovo.imoovohelper.di.qualifier.IoDispatcher
import com.imoovo.imoovohelper.domain.SuspendUseCase
import com.imoovo.imoovohelper.domain.mapper.UserMapper
import com.imoovo.imoovohelper.domain.model.user.User
import com.imoovo.imoovohelper.domain.model.filters.UserFilter
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetBrokerByFiltersUseCase @Inject constructor(
    private val brokerRepository: BrokerRepository,
    private val userMapper: UserMapper,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : SuspendUseCase<UserFilter,List<User>>(ioDispatcher) {

    override suspend fun execute(parameters: UserFilter): List<User> =
        brokerRepository.getBrokersByFilters(parameters).run {
            userMapper.toDomainList(this).toList()
        }

}