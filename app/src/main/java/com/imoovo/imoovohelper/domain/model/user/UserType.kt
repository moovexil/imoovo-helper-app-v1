package com.imoovo.imoovohelper.domain.model.user

enum class UserType(val displayType: String) {
    CARRIER("Carrier"),
    AGENT("Agent"),
    ADMIN("Administrator"),
    EX_ADMIN ("Ex admin"),
    BROKER ("Broker")
}