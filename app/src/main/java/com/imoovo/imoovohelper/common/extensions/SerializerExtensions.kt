package com.imoovo.imoovohelper.common.extensions

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Converts the JSON string object into the requested type object.
 */
inline fun <reified T> String.deserializeJson(converter: Gson): T =
    converter.fromJson(this, T::class.java)

/**
 * Coverts the JSON string object into the collection with requested type objects.
 */
fun <T> String.deserializeCollection(converter: Gson): Collection<T> {
    val groupListType = object : TypeToken<Collection<T>>() {}.type
    return converter.fromJson<Collection<T>>(this, groupListType)
}

/**
 * Converts an object into the JSON string object
 */
fun Any.serializeJson(converter: Gson): String = converter.toJson(this)

/**
 * Checks if the given [String] either null or blank or empty.
 *
 * @return null if the given string is null blank or empty
 *          otherwise given string if it passed the predicate
 */
fun String.nullIfBlankOrEmpty(): String? = if (isBlank()) null else this

/**
 * Checks if the given [String] either null or blank or empty.
 *
 * @return false if the given string is null blank or empty otherwise true
 */
fun String?.notNullAndNotBlank () : Boolean = this != null && isNotBlank()

/**
 * Checks if the given [String] is null.
 *
 * @return the current [String] or empty string if value is null
 */
fun String?.emptyStringIfNull(): String = this ?: ""

/**
 * Checks if the given [Boolean] is null.
 *
 * @return the current [Boolean] or false if value is null
 */
fun Boolean?.falseIfNull(): Boolean = this ?: false

