package com.imoovo.imoovohelper.common.util

import android.os.Build

/**
 *  Checks if current device version Q(Android 10 / API 29) and higher
 */
fun isVersionQ(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q

/**
 *  Checks if current device version O(Android oreo / API 26) and higher
 */
fun isVersionO (): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O