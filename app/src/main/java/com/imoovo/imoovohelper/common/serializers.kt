//@file:JvmName("JsonSerializers")
//
package com.imoovo.imoovohelper.common
//
//import android.os.ParcelFormatException
//import kotlinx.serialization.*
//import kotlinx.serialization.internal.StringDescriptor
//import java.text.SimpleDateFormat
//import java.util.*
//
//@Serializer(forClass = Date::class)
//object DateSerializer : KSerializer<Date> {
//    private val df: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
//
//    override val descriptor: SerialDescriptor =
//        StringDescriptor.withName("WithCustomDefault")
//
//    override fun serialize(encoder: Encoder, obj: Date) {
//        encoder.encodeString(df.format(obj))
//    }
//
//    override fun deserialize(decoder: Decoder): Date {
//        return df.parse(decoder.decodeString()) ?: throw ParcelFormatException("Can't parse string")
//    }
//}
//
//@Serializer(forClass = UUID::class)
//object UuidSerializer : KSerializer<UUID> {
//
//    override fun serialize(encoder: Encoder, obj: UUID) =
//        encoder.encodeString(obj.toString())
//
//    override fun deserialize(decoder: Decoder): UUID =
//        UUID.fromString(decoder.decodeString())
//
//}
