package com.imoovo.imoovohelper.common.factories.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.ListenableWorker
import androidx.work.WorkerParameters

/**
 * Util interface for [CustomWorkerFactory]. Use it to delegate creation for the [Worker].
 */
interface ChildWorkerFactory {
    fun create(appContext: Context, params: WorkerParameters): ListenableWorker
}