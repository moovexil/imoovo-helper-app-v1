package com.imoovo.imoovohelper.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.lifecycle.LiveData

/**
 * Provides the Internet connection state notification service which uses
 * [androidx.lifecycle.Lifecycle] of the caller to starting updates and
 * removing this service if is no longer needed.
 *
 * When the Internet connection exists -> true, otherwise -> false
 */
class LiveNetwork(private val context: Context) : LiveData<Boolean>() {

    override fun onActive() {
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        context.registerReceiver(receiver, filter)
    }

    override fun onInactive() {
        context.unregisterReceiver(receiver)
    }

    private val receiver: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            if (intent.extras != null) {
                val isLost = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)
                postValue(!isLost)
            }
        }
    }

}